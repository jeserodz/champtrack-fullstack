'use strict';
const path = require('path');
const app = require(path.resolve(__dirname, '../../server/server'));
const _ = require('lodash');
const Promise = require('bluebird');

module.exports = function (Post) {

  // Add createdDate
  Post.observe('before save', function createdDate(ctx, next) {
    if (ctx.instance && !ctx.instance.createdDate) {
      ctx.instance.createdDate = new Date();
    }
    next();
  });

  // Increase users' notifications counter
  Post.observe('after save', async function incrementNotificationCounter(ctx, next) {
    const { AppUser } = app.models;
    if (ctx.isNewInstance) {
      const users = await AppUser.find();
      users.forEach(async user => {
        user.unreadPostCount = (user.unreadPostCount + 1) || 1;
        await user.save();
      });
    }
  });

  /**
 * Toggle like
 * @param {string} postId
 * @param {string} userId
 * @param {Function(Error, object)} callback
 */
  Post.toggleLike = function (postId, userId, callback) {
    const { PostLike } = app.models;
    let post;
    new Promise(async (resolve) => resolve()).then(async () => {
      post = await Post.findById(postId, { include: ['likes'] });
      post = post.toJSON();
      const userLike = _.find(post.likes, p => p.userId.toString() === userId);
      if (userLike) {
        await PostLike.destroyById(userLike.id);
      } else {
        await PostLike.create({ postId, userId, createdDate: new Date() });
      }
      post = await Post.findById(postId, { include: ['likes'] });
      return post;
    })
    .then(async (post) => {
      callback(null, post);
    });
  };
};
