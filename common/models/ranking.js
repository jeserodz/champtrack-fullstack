'use strict';

const path = require('path');
const fs = require('fs');
const app = require(path.resolve(__dirname, '../../server/server'));
const _ = require('lodash');
const json2xls = require('json2xls');
const Promise = require('bluebird');
const moment = require('moment');

module.exports = function (Ranking) {

  Ranking.observe('access', function logQuery(ctx, next) {
    console.log('Accessing %s matching %s', ctx.Model.modelName, ctx.query.where);
    next();
  });

/**
 *
 * @param {string} rankingId
 * @param {array} users
 * @param {Function(Error, object)} callback
 */
  Ranking.bulkAssignParticipants = function (rankingId, users, callback) {
    new Promise((resolve, reject) => {
      try {
        users.forEach(async u => {
          let user = await AppUser.findOne({ where: { id: u.id } });
          if (!user) return;
          user.rankingId = rankingId;
          user = await user.save();
          console.log('Participant assigned = ', JSON.stringify(user, null, 2));
        });
        resolve()
      } catch(err) {
        reject(err);
      }
    })
      .then(async () => {
        const updatedRanking = await Ranking.findOne({ where: { id: rankingId } });
        callback(null, updatedRanking);
      })
      .catch(err => {
        callback(err, null);
      });
  }

  /**
 * Get accumulated records per user for specific Ranking
 * @param {string} rankingId Ranking ID
 * @param {number} year Allows to get accumulated records per year
 * @param {Function(Error, array)} callback
 */
  Ranking.accumulatedRecords = function (rankingId, year, callback) {
    new Promise(async (resolve, reject) => {

      const { Record } = app.models;
      let accumulatedRecords = [];

      const ranking = await Ranking.findOne({
        where: { id: rankingId },
        include: [{ users: ['country', 'line'] }, 'variables', 'periods']
      });

      const rankingJSON = ranking.toJSON();

      // Get ranking variables
      const variables = rankingJSON.variables;
      let periods = rankingJSON.periods;

      // If year was provided, filter periods per year
      if (year) {
        periods = periods.filter(p => moment(p.endDate).year() === year);
      }

      // Get all ranking users
      const users = rankingJSON.users;
      const userPromises = users.map(async user => {
        return new Promise(async (resolve) => {
          const rankingPeriodIds = periods.map(p => p.id.toString());
          // Get all records of the user
          const records = await Record.find({
            where: { userId: user.id.toString(), periodId: {inq: rankingPeriodIds} },
            include: [{data: 'variable'}, 'likes']
          });
          // accumulate record values per variable and calculate average;
          const accumulatedRecord = getAccumulatedRecord(records);
          if (accumulatedRecord) {
            accumulatedRecord.user = user;
            accumulatedRecords.push(accumulatedRecord);
          }
          resolve();
        });
      });
      Promise.all(userPromises).then(() => {
        console.log('Calculated accumulated records for Ranking: ' + ranking.name);
        accumulatedRecords = _.sortBy(accumulatedRecords, record => record.score);
        accumulatedRecords = accumulatedRecords.reverse();
        resolve(accumulatedRecords);
      });
    })
      .then(accumulatedRecords => {
        callback(null, accumulatedRecords);
      })
      .catch(err => {
        callback(err);
      });
  };

  /**
 *
 * @param {array} recordsList
 * @param {Function(Error, )} callback
 */
  Ranking.exportToExcel = function(recordsList, req, res, callback) {
    const fileName = `export-${(new Date()).getTime()}.xlsx`;
    const filePath = path.resolve(`${__dirname}/../../server/uploads/exports/${fileName}`);
    var excelResponse = json2xls(recordsList);
    fs.writeFileSync(filePath, excelResponse, 'binary');
    callback(null, `/api/uploads/exports/download/${fileName}`);

    // delete file from server after 1 minute
    setTimeout(() => {
      fs.unlinkSync(filePath);
    }, 60000);
  };

};

function getAccumulatedRecord(records) {
  if (!records.length) return null;

  const recordsJSON = records.map(record => record.toJSON());
  const allRecordsData = _.flatten(recordsJSON.map(record => record.data));
  const allRecordsDataGroupedByVariable = _.groupBy(allRecordsData, (recordData) => recordData.variableId.toString());

  const accumulatedRecord = recordsJSON.reduce((previousRecord, currentRecord) => {
    if (previousRecord) {
      currentRecord.likes.concat(previousRecord.likes);
      currentRecord.data.forEach(recordData => {
        const previousRecordData = _.find(previousRecord.data, (rd) => rd.variableId.toString() == recordData.variableId.toString());
        if (previousRecordData) recordData.value += previousRecordData.value;
      });
    }
    return currentRecord;
  });

  accumulatedRecord.score = 0;
  accumulatedRecord.data.forEach(recordData => {
    recordData.value /= allRecordsDataGroupedByVariable[recordData.variableId.toString()].length;
    accumulatedRecord.score += recordData.value * (recordData.variable.weight/100);
  });

  return accumulatedRecord;
}
