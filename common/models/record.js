'use strict';

const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const app = require(path.resolve(__dirname, '../../server/server'));
const Promise = require('bluebird');
const json2csv = require('json2csv');
const parser = require('csv-parse');
const json2xls = require('json2xls');
const xlsx = require('xlsx');

const sanitizeRegex = /\W+/igm;

module.exports = function (Record) {

  /**
   * Creates or updates a record and variables data
   * @param {object} record Record object with variables data
   * @param {Function(Error, object)} callback
   */
  Record.upsertWithVariables = function (recordRequest, callback) {
     new Promise(async (resolve, reject) => {
      const { Ranking, Period, Variable, AppUser, RecordData } = app.models;
      const { userId, periodId } = recordRequest;

      let record = await Record.findOne({ where: { userId, periodId }, include: ['data', 'user']});
      
      // If record found, update all its variables data
      if (record) { 
        let recordJSON = record.toJSON();
        recordRequest.data.forEach(async (dataObj) => {
          let recordJSONData = _.find(recordJSON.data, dat => dat.variableId.toString() === dataObj.variableId);
          if (recordJSONData) {
            recordJSONData.value = dataObj.value;
            let result = await RecordData.upsertWithWhere({ id: recordJSONData.id }, recordJSONData);
            console.log('Updated record: ' + JSON.stringify(result, null, 2));
          } else {
            dataObj.recordId = record.id.toString();
            let result = await RecordData.upsert(dataObj);
            console.log('Created record: ' + JSON.stringify(result, null, 2));
          }
        });
      } 
      // If record not found, create new record and all its variables data
      else { 
        console.log('Record not found... Creating record');
        let newRecord = await Record.upsert(recordRequest);
        console.log('Record created ' + JSON.stringify(newRecord, null, 2));
        recordRequest.data.forEach(async (dataObj) => {
          dataObj.recordId = newRecord.id;
          let newData = await RecordData.upsert(dataObj);
          console.log('RecordData created ' + JSON.stringify(newData, null, 2));
        });
      }

      // Respond request with new or updated record
      record = await Record.findOne({ where: { userId, periodId }, include: ['data', 'user']});
      let recordJSON = record.toJSON();
      console.log(recordJSON);
      resolve(recordJSON);
    })
      .then(record => {
        callback(null, record);
      })
      .catch(err => {
        callback(err);
      });
  };

  /**
   * Generates CSV file template for bulk creation of records
   * @param {string} rankingId periodId
   * @param {string} periodId
   * @param {Function(Error, string)} callback
   */
  Record.generateCsvTemplate = (rankingId, periodId, callback) => {
    new Promise(async (resolve, reject) => {
      const { Ranking, Period, Variable, AppUser, RecordData } = app.models;
      console.log(`Generating CSV template for Ranking ID: ${rankingId} - Period ID: ${periodId}...`);
      const ranking = await Ranking.findById(rankingId, { include: ['periods', 'variables', 'users'] });
      const rankingJSON = ranking.toJSON();
      const period = _.find(rankingJSON.periods, (period) => period.id.toString() === periodId);
      const variables = rankingJSON.variables;
      const users = rankingJSON.users;
      resolve({ ranking, period, variables, users });
    })
      .then(({ ranking, period, variables, users }) => {
        const fields = getColumns(ranking, period, variables);
        const data = [getMetadataRow(ranking, period, variables)].concat(getUsersRows(users));
        const csvTemplate = json2csv({ data, fields });
        return csvTemplate;
      })
      .then(csvTemplate => {
        callback(null, csvTemplate);
      });
  };

  /**
   * Create records from CSV template
   * @param {string} csv CSV file with data
   * @param {Function(Error, string)} callback
   */
  Record.createFromCsv = function (csv, callback) {
    new Promise(async (resolve, reject) => {
      const { Ranking, Period, Variable, AppUser, RecordData } = app.models;
      var result;
      parser(csv, { columns: true }, function (err, data) {
        const metadataRow = data[0];
        const metadataKeys = Object.keys(metadataRow);
        const rankingId = metadataRow[metadataKeys[0]];
        const periodId = metadataRow[metadataKeys[1]];
        if (metadataKeys.length >= 6) { // CSV has variables fields
          let variableIds = [];
          for(var i = 5; i < metadataKeys.length; i++) {
            let id = metadataRow[metadataKeys[i]];
            variableIds.push(id);
          }
          const records = parseRecordsFromCSV(rankingId, periodId, variableIds, data.slice(1));
          let totalCreated = 0;
          records.forEach(async (record, index) => {
            const newRecord = await new Promise((resolve, reject) => {
              Record.upsertWithVariables(record, function(err, newRecord) {
                if (!err) totalCreated++;
                resolve(newRecord);
              });
            });
            if (index === records.length-1) 
              resolve(`Create ${totalCreated} records.`);
          });
        }
      });
    })
      .then(result => {
        console.log(result);
        callback(null, result);
      })
      .catch(err => {
        callback(err);
      });
  };
  
  /**
   * Generates XLS file template for bulk creation of records
   * @param {string} rankingId periodId
   * @param {string} periodId
   * @param {Function(Error, string)} callback
   */
  Record.generateXlsTemplate = (rankingId, periodId, callback) => {
    new Promise(async (resolve, reject) => {
      const { Ranking, Period, Variable, AppUser, RecordData } = app.models;
      console.log(`Generating XLS template for Ranking ID: ${rankingId} - Period ID: ${periodId}...`);
      const ranking = await Ranking.findById(rankingId, { include: ['periods', 'variables', 'users'] });
      const rankingJSON = ranking.toJSON();
      const period = _.find(rankingJSON.periods, (period) => period.id.toString() === periodId);
      const variables = rankingJSON.variables;
      const users = _.sortBy(rankingJSON.users, (u) => u.email.toLowerCase());
      resolve({ ranking, period, variables, users });
    })
      .then(({ ranking, period, variables, users }) => {
        const rankingTitle = `Ranking ${ranking.name}`.replace(sanitizeRegex, '_');
        const periodTitle = `Periodo ${period.name}`.replace(sanitizeRegex, '_');
        
        const header = {};
        header['Ranking'] = ranking.id;
        header['Periodo'] = period.id;
        header['Usuario'] = 'N/A';
        header['Correo'] = 'N/A';
        header['ID'] = 'N/A';
        variables.forEach(v => header[v.name.replace(sanitizeRegex, '_')] = v.id);

        const body = users.map(u => {
          const entry = {};
          entry['Ranking'] = 'N/A';
          entry['Periodo'] = 'N/A';
          entry['Usuario'] = u.displayName;
          entry['Correo'] = u.email;
          entry['ID'] = u.id;
          variables.forEach(v => entry[v.name.replace(sanitizeRegex, '_')] = 0);
          return entry;
        });

        const xlsTemplate = [ header, ...body ];
        return { xlsTemplate, rankingTitle, periodTitle };
      })
      .then(({ xlsTemplate, rankingTitle, periodTitle }) => {
        const fileName = `plantilla-${rankingTitle}-${periodTitle}-${(new Date()).getTime()}.xlsx`;
        const filePath = path.resolve(`${__dirname}/../../server/uploads/exports/${fileName}`);
        var excelResponse = json2xls(xlsTemplate);
        fs.writeFileSync(filePath, excelResponse, 'binary');
        callback(null, `/api/uploads/exports/download/${fileName}`);
        
        // delete file from server after 1 minute
        setTimeout(() => fs.unlinkSync(filePath), 60000);
      });
  };
  
  /**
   * Create records from XLS template
   * @param {string} xls XLS file with data
   * @param {Function(Error, string)} callback
   */
  Record.createFromXls = function (xls, callback) {
    const { Ranking, Period, Variable, AppUser, RecordData } = app.models;
    
    new Promise(async (resolve, reject) => {
      // store XLS data in a temp file
      const fileName = `uploaded-${(new Date()).getTime()}.xlsx`;
      const filePath = path.resolve(`${__dirname}/../../server/uploads/exports/${fileName}`);
      fs.writeFileSync(filePath, xls, 'binary');
      setTimeout(() => fs.unlinkSync(filePath), 60000); // delete file from server after 1 minute

      // parse XLS data
      const wb = xlsx.readFile(filePath);
      const xlsData = xlsx.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]]);

      // fetch ranking, period, variables and users used in the XLS
      const ranking = (await Ranking.findById(xlsData[0]['Ranking'], { include: ['periods', 'variables', 'users'] })).toJSON();
      const period = (await Period.findById(xlsData[0]['Periodo'], { include: {'records': 'data'} })).toJSON();
      const variables = ranking.variables;
      console.log(ranking, period, variables);

      // create or update records and records variables data
      const upsertWithVariablesAsync = Promise.promisify(Record.upsertWithVariables);
      xlsData.slice(1, xlsData.length).forEach(async record => {
        const periodRecord = {
          periodId: period.id.toString(),
          userId: record['ID'],
          data: []
        };
        variables.forEach(variable => {
          const variableColumnHeader = _.findKey(xlsData[0], column => column === variable.id.toString());
          periodRecord.data.push({
            variableId: variable.id.toString(),
            value: sanitizeValue(parseFloat(record[variableColumnHeader]) || "0.00")
          });
        });
        try { await upsertWithVariablesAsync(periodRecord) }
        catch (err) { 
          console.log(err);
          reject(err);
        }
      });
      
      resolve('Data uploaded successfully');
    })
    .then(result => callback(null, result))
    .catch(err => callback(err));
  };

};

/**
 * Helpers
 */

/**
 * 
 * @param {*} ranking 
 * @param {*} period 
 * @param {*} variables 
 */
function getColumns(ranking, period, variables) {
  const fields = [
    { label: `Ranking: ${ranking.name.replace(' ', '_')}`, value: 'ranking', default: 'N/A' },
    { label: `Periodo: ${period.name.replace(' ', '_')}`, value: 'period', default: 'N/A' },
    { label: 'Usuario', value: 'user', default: 'N/A' },
    { label: 'Correo', value: 'email', default: 'N/A' },
    { label: 'ID', value: 'id', default: 'N/A' }
  ];

  variables.forEach(variable => {
    fields.push({ label: variable.name, value: variable.id.toString(), default: 0 })
  });

  return fields;
}

/**
 * 
 * @param {*} ranking 
 * @param {*} period 
 * @param {*} variables 
 */
function getMetadataRow(ranking, period, variables) {
  let metadata = {
    ranking: ranking.id.toString(),
    period: period.id.toString()
  };

  variables.forEach(variable => {
    const variableId = variable.id.toString();
    metadata[variableId] = variableId;
  });

  return metadata;
}

/**
 * 
 * @param {*} users 
 */
function getUsersRows(users) {
  const usersRows = users.map(user => {
    return { user: user.displayName, email: user.email, id: user.id.toString() };
  });
  return usersRows;
}

function parseRecordsFromCSV(rankingId, periodId, variableIds, userRecords) {
  const records = userRecords.map(userRecord => {
    const userRecordKeys = Object.keys(userRecord);
    return {
      periodId,
      userId: userRecord[userRecordKeys[4]],
      data: variableIds.map((variableId, index) => {
        return {
          variableId,
          value: sanitizeValue(userRecord[userRecordKeys[5 + index]])
        }
      })
    };
  });
  return records;
}

function sanitizeValue(value) {
  value = parseFloat(value);

  // Convert fractions to percentages
  if (value > 0 && value < 5) {
    value *= 100;
  }

  value = value.toFixed(2);

  // Limit values to 150
  if (value > 150) {
    value = 150;
  }

  return value;
}
