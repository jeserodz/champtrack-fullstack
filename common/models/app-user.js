"use strict";
const path = require("path");
const app = require(path.resolve(__dirname, "../../server/server"));
const _ = require("lodash");
const Promise = require("bluebird");

module.exports = function(AppUser) {
  /**
 * Assign roles to user
 * @param {object} user
 * @param {array} roles
 * @param {Function(Error, object)} callback
 */
  AppUser.assignRoles = function(user, roles, callback) {
    const { AppUser, AppRole, AppRoleMapping } = app.models;
    Promise.resolve()
      // first remove all roles from user
      .then(() => AppRoleMapping.destroyAll({
        principalType: AppRoleMapping.USER,
        principalId: user.id
      }))
      // then add the roles in the payload
      .then(() => Promise.all(roles.map(r => {
        return AppRole
          .findOne({ where: { id: r.id } })
          .then(role => role.principals.create({
            principalType: AppRoleMapping.USER,
            principalId: user.id
          }));
      })))
      // get the updated user with roles
      .then(() => AppUser.findOne({
        where: { id: user.id },
        include: ["country", "businessUnit", "ranking", "roles"]
      }))
      .then(updatedUser => {
        callback(null, updatedUser);
      })
      .catch(err => {
        callback(err, null);
      });
  };

  /**
 * Clear unread notifications
 * @param {string} userId User ID
 * @param {string} notificationType Notification type. Example: 'Post', 'Message', 'Survey'
 * @param {Function(Error, object)} callback
 */
  AppUser.prototype.clearUnreadNotifications = async function(notificationType, callback) {
    const user = this;
    const supportedNotificationTypes = 'Message Post Survey';
    if (supportedNotificationTypes.includes(notificationType) === false) {
      return callback(
        `The notificationType "${notificationType}" is not supported.` +
        `Supported ones are: ${supportedNotificationTypes}`,
        null
      );
    }
    user[`unread${notificationType}Count`] = 0;
    const updatedUser = await user.save();
    callback(null, updatedUser);
  };
};
