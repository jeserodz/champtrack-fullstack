'use strict';
const path = require('path');
const app = require(path.resolve(__dirname, '../../server/server'));
const _ = require('lodash');
const Promise = require('bluebird');

module.exports = function (Message) {
  
  // Increase users' notifications counter
  Message.observe('after save', async function incrementNotificationCounter(ctx, next) {
    const { AppUser } = app.models;
    if (ctx.isNewInstance) {
      const user = await AppUser.findById(ctx.instance.toId);
      user.unreadMessageCount = (user.unreadMessageCount + 1) || 1;
      await user.save();
    }
  });

  /**
   *
   * @param {array} userIds
   * @param {string} text
   * @param {Function(Error, string)} callback
   */
  Message.sendMessage = function (fromId, userIds, text, callback) {
    const { Email, AppUser } = app.models;
    var result;
    new Promise((resolve) => resolve())
      // create message instance for each user 
      .then(() => {
        const msgPromises = userIds.map(userId => {
          return Message.create({
            fromId,
            toId: userId,
            text,
            createdDate: new Date()
          });
        });
        return Promise.all(msgPromises);
      })
      // send a single email including all users
      .then(async responses => {
        const users = await AppUser.find({ where: { id: { inq: userIds } } });
        let userEmails = users.map(user => user.email);
        userEmails = userEmails.join(', ');
        Email.send({
          to: userEmails,
          from: {
            name: "ChampTrack",
            address: 'champtracksystem@gmail.com'
          },
          subject: 'Mensaje ChampTrack',
          text: text
        }, function (err, mail) {
          console.log('Email sent');
          callback(null, 'Email sent');
        });
      });
  };


  /**
   *
   * @param {string} userId
   * @param {Function(Error, array)} callback
   */
  Message.userMessages = function (userId, callback) {
    new Promise(res => res()).then(async () => {
      let messages = await Message.find({
        where: { 'active': true },
        include: 'fromUser',
        order: 'createdDate DESC'
      });
      messages = messages.filter(msg => msg.toId.toString() === userId);
      return messages;
    }).then(messages => {
      callback(null, messages);
    });
  };

/**
 *
 * @param {string} messageId
 * @param {Function(Error, object)} callback
 */
  Message.hideMessage = function (messageId, callback) {
    new Promise(res => res()).then(async () => {
      let message = await Message.upsertWithWhere({ id: messageId }, { active: false });
      return message;
    }).then(message => {
      callback(null, message);
    });
  };

};
