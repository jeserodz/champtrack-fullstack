'use strict';
const path = require('path');
const app = require(path.resolve(__dirname, '../../server/server'));

module.exports = function (Survey) {

  // Increase users' notifications counter
  Survey.observe('after save', async function incrementNotificationCounter(ctx, next) {
    const { AppUser } = app.models;
    if (ctx.isNewInstance) {
      const users = await AppUser.find();
      users.forEach(async user => {
        user.unreadSurveyCount = (user.unreadSurveyCount + 1) || 1;
        await user.save();
      });
    }
  });

  /**
   *
   * @param {string} surveyId
   * @param {Function(Error, object)} callback
   */
  Survey.hideSurvey = function (surveyId, callback) {
    Survey.upsertWithWhere({ id: surveyId }, { active: false }, function(err, survey) {
      if (err) callback(err, null);
      else callback(null, survey);
    });
  };

};
