'use strict';
const path = require('path');
const app = require(path.resolve(__dirname, '../../server/server'));
const _ = require('lodash');
const Promise = require('bluebird');

module.exports = function(BusinessUnit) {

/**
 * @param {string} id
 * @param {Function(Error, number)} callback
 */
  BusinessUnit.safeDelete = function(id, callback) {
    const { Line, AppUser } = app.models;
    new Promise(async(resolve, reject) => {
      try {
        await Line.destroyAll({ businessUnitId: id });
        await BusinessUnit.destroyById(id);
        const users = await AppUser.find({ businessUnitId: id });
        users.forEach(async user => {
          user.businessUnitId = null;
          user.lineId = null;
          await user.save();
        });
        resolve();
      } catch (err) {
        reject(err);
      }
    })
    .then(() => {
      const count = 1;
      callback(null, count);
    })
    .catch(err => {
      callback(err, null);
    });
  };
};
