const _ = require('lodash');
const fs = require('fs');

const users = require('./users-list');

const lines = [
  {
    "name": "Cardio",
    "businessUnitId": "5979c30bad86fd69f1b9be5b",
    "active": true,
    "id": "5979c30cad86fd69f1b9be61"
  },
  {
    "name": "Diabetes",
    "businessUnitId": "5979c30bad86fd69f1b9be5b",
    "active": true,
    "id": "5979c30cad86fd69f1b9be62"
  },
  {
    "name": "Músculo-Articular",
    "businessUnitId": "5979c30bad86fd69f1b9be5e",
    "active": true,
    "id": "5979c30cad86fd69f1b9be63"
  },
  {
    "name": "Pediatría",
    "businessUnitId": "5979c30bad86fd69f1b9be5e",
    "active": true,
    "id": "5979c30cad86fd69f1b9be64"
  },
  {
    "name": "Respi-Gastro",
    "businessUnitId": "5979c30bad86fd69f1b9be5e",
    "active": true,
    "id": "5979c30cad86fd69f1b9be65"
  },
  {
    "name": "Gastro-Pediatría",
    "businessUnitId": "5979c30bad86fd69f1b9be5e",
    "active": true,
    "id": "5979c30cad86fd69f1b9be66"
  },
  {
    "name": "KIM",
    "businessUnitId": "5979c30bad86fd69f1b9be5f",
    "active": true,
    "id": "597cf06ada52ce6e959d9c15"
  },
  {
    "name": "KIM SAN-GEN",
    "businessUnitId": "5979c30bad86fd69f1b9be5f",
    "active": true,
    "id": "597cf07dda52ce6e959d9c16"
  },
  {
    "name": "KAM ONCO",
    "businessUnitId": "5979c30bad86fd69f1b9be5f",
    "active": true,
    "id": "597cf088da52ce6e959d9c17"
  },
  {
    "name": "MOVILIDAD",
    "businessUnitId": "5979c30bad86fd69f1b9be5e",
    "active": true,
    "id": "597cf0b3da52ce6e959d9c18"
  },
  {
    "name": "MULTILINEA",
    "businessUnitId": "5979c30bad86fd69f1b9be5e",
    "active": true,
    "id": "597cf0bdda52ce6e959d9c19"
  },
  {
    "name": "MSA",
    "businessUnitId": "5979c30bad86fd69f1b9be5b",
    "active": true,
    "id": "597cf0e9da52ce6e959d9c1a"
  },
  {
    "name": "INTERNAL MEDICINE",
    "businessUnitId": "5979c30bad86fd69f1b9be5b",
    "active": true,
    "id": "597cf0f7da52ce6e959d9c1b"
  },
  {
    "name": "GENERAL PHYSICIAN",
    "businessUnitId": "5979c30bad86fd69f1b9be5b",
    "active": true,
    "id": "597cf104da52ce6e959d9c1c"
  },
  {
    "name": "VENDEDORES",
    "businessUnitId": "597cf165da52ce6e959d9c1d",
    "active": true,
    "id": "597cf175da52ce6e959d9c1e"
  },
  {
    "name": "CENTRALES",
    "businessUnitId": "597cf165da52ce6e959d9c1d",
    "active": true,
    "id": "597cf17bda52ce6e959d9c1f"
  },
  {
    "name": "FARMACIAS",
    "businessUnitId": "597cf18cda52ce6e959d9c20",
    "active": true,
    "id": "597cf1a3da52ce6e959d9c21"
  },
  {
    "name": "GENERICOS",
    "businessUnitId": "5979c30bad86fd69f1b9be60",
    "active": true,
    "id": "597cf3b788ee3e6f7e697dc6"
  },
  {
    "name": "INSTITUCIONAL",
    "businessUnitId": "5979c30bad86fd69f1b9be5f",
    "active": true,
    "id": "597cf41588ee3e6f7e697dc7"
  }
];

users.forEach(u => {
  switch (u.businessUnit.name) {
    case 'CARDIO': u.businessUnit.name = 'Cardio'; break;
    case 'DIABETES': u.businessUnit.name = 'Diabetes'; break;
    case 'GASTROPEDIATRIA': u.businessUnit.name = 'Gastro-Pediatría'; break;
    case 'RESPI GASTRO': u.businessUnit.name = 'Respi-Gastro'; break;
    case 'PEDIATRIA': u.businessUnit.name = 'Pediatría'; break;
    case 'KAM SAN-GEN': u.businessUnit.name = 'KIM SAN-GEN'; break;
    case 'CENTRALES': u.businessUnit.name = 'KIM SAN-GEN'; break;
  }

  const line = _.find(lines, { name: u.businessUnit.name });
  u.lineId = line.id;
  delete u.businessUnit;
  delete u.ranking;
  delete u.country;
});

let noLineUsers = [];

users.forEach(u => {
  u.lineId ? null : noLineUsers.push(u);
});

// countries = _.uniq(countries);
// bus = _.uniq(bus);

console.log(JSON.stringify(users, null, 2));
console.log('============================================');
console.log(JSON.stringify(noLineUsers, null, 2));

fs.writeFile('users-list.json', JSON.stringify(users, null, 2), (err, success) => {
  if (!err) {
    return console.error(err);
  }
  console.log('File written');
})