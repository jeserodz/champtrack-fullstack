const _ = require('lodash');

module.exports = async function(app) {
  const { AppUser, ACL, AppRole, AppRoleMapping } = app.models;

  const roles = await AppRole.find({});

  // Find or create roles
  let adminRole = _.find(roles, { name: 'Administrator' });
  if (!adminRole) {
    adminRole = await AppRole.create({ name: 'Administrator' });
    console.log('Admin role created (OK)');
  } else {
    console.log('Admin role found (OK)');
  }

  // Verify superAdmin account is created
  const superAdminEmail = app.get('superAdminEmail');
  const superAdminPassword = app.get('superAdminPassword');
  let superAdmin = await AppUser.findOne({ where: { email: superAdminEmail } });
  if (!superAdmin) {
    console.log('Super admin account not found. Creating...');
    let superAdmin = await AppUser.create({ 
      displayName: 'Super Admin',
      username: 'superadmin', 
      email: superAdminEmail, 
      password: superAdminPassword 
    });
    console.log('Super admin created. (OK)');
  } else {
    console.log('Super admin account found. (OK)');
    await superAdmin.updateAttributes({ password: superAdminPassword });
    console.log('Super admin password reset. (OK)');
  }

  // Assign admin role to superAdmin
  let adminRoleAssignment = await AppRoleMapping.findOne({
    principalType: AppRoleMapping.USER,
    principalId: superAdmin.id
  });

  if (!adminRoleAssignment) {
    console.log('Super Admin role not found. Creating...');
    await adminRole.principals.create({
      principalType: AppRoleMapping.USER,
      principalId: superAdmin.id
    });
    console.log('Super Admin role created. (OK)');
  } else {
    console.log('Super Admin role found. (OK)');
  }
}