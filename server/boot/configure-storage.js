'use strict';

module.exports = function(app) {
  var Ranking = app.models.Ranking;
  var AppUser = app.models.AppUser;
  var Country = app.models.Country;
  var Post = app.models.Post;

  //Function for checking the file type..
  app.dataSources.storage.connector.getFilename = function(file, req, res) {
    //First checking the file type..
    var pattern = /^image\/.+$/;
    var value = pattern.test(file.type);
    if (value) {
      var fileExtension = file.name.split('.').pop();
      var container = file.container;
      var id = req.query.id;
      var time = (new Date()).getTime();
      
      //Now preparing the file name..
      var newFileName = '' + id + '-' + time + '.' + fileExtension;

      // update the model
      switch (container) {
        case 'rankings': {
          Ranking.findById(id, function(err, ranking) {
            ranking.photoURL = '/uploads/rankings/download/' + newFileName;
            ranking.save();
          });
          break;
        }
        case 'profiles': {
          AppUser.findById(id, function(err, user) {
            user.photoURL = '/uploads/profiles/download/' + newFileName;
            user.save();
          });
          break;
        }
        case 'countries': {
          Country.findById(id, function(err, country) {
            country.photoURL = '/uploads/countries/download/' + newFileName;
            country.save();
          });
          break;
        }
        case 'posts': {
          Post.findById(id, function(err, post) {
            post.photoURL = '/uploads/posts/download/' + newFileName;
            post.save();
          });
          break;
        }
      }

      //And the file name will be saved as defined..
      return newFileName;
    } else {
      throw 'FileTypeError: Only File of Image type is accepted.';
    }
  };
};
