'use strict';
var loopback = require('loopback');
var boot = require('loopback-boot');

var http = require('http');
var https = require('https');

var app = module.exports = loopback();
var redirectServer = require('./redirectServer');

var sslConfig = null;

// boot scripts mount components like REST API
boot(app, __dirname);

app.start = function(httpOnly) {
  if (httpOnly === undefined) {
    httpOnly = process.env.HTTP || app.get('httpOnly');
  }

  var server = null;

  if (!httpOnly) {
    sslConfig = require('./ssl-config');
    var options = {
      key: sslConfig.privateKey,
      cert: sslConfig.certificate,
    };
    server = https.createServer(options, app);
  } else {
    server = http.createServer(app);
  }

  server.listen(app.get('port'), function() {
    var baseUrl = (httpOnly ? 'http://' : 'https://') + app.get('host') + ':' + app.get('port');
    app.emit('started', baseUrl);
    console.log('LoopBack server listening at %s%s', baseUrl, '/');
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });

  if (!httpOnly) {
    redirectServer.listen(80);
  }

  return server;
};

// start the server if `$ node server.js`
if (require.main === module) {
  app.start();
}
