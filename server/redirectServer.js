var express = require('express');
var redirectServer = express();

redirectServer.use(function (req, res) {
  res.redirect(301, 'https://' + req.hostname + req.originalUrl);
});

module.exports = redirectServer;