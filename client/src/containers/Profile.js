import _ from 'lodash';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import ChangePasswordDialog from '../components/ChangePasswordDialog';
import { Avatar, FlatButton, List, ListItem, Divider, CircularProgress } from 'material-ui';
import { colors, spacing } from 'material-ui/styles';
import { StyleSheet, css } from 'aphrodite';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);
import fallbackImage from '../images/avatar-placeholder.png';
 
class Profile extends Component {
  INITIAL_STATE = { selectedFile: null }
  state = this.INITIAL_STATE

  componentDidMount() {
    const { user } = this.props.auth;
    this.props.fetchUser(user.id);
  }

  componentDidUpdate() {
    if (this.props.user.photoUploadProgress && this.state.selectedFile) 
      this.setState({ ...this.state, selectedFile: null });
  }

  onFileSelected() {
    const selectedFile = document.getElementById('fileInput').files[0];
    this.setState({ selectedFile });
  }

  onFileButtonPress() {
    const { profile } = this.props.user;
    if (!this.state.selectedFile) return null;
    const validFormat = String('image/jpeg image/gif image/png').indexOf(this.state.selectedFile.type) > -1;
    validFormat ? this.props.uploadUserImage(profile.id, this.state.selectedFile) : null;
  }

  onChangePasswordButtonPress() {
    this.props.openChangePasswordDialog();
  }
  
  renderSelectFileButton() {
    const { photoUploadProgress } = this.props.user;
    if (photoUploadProgress) return null;
    return (
      <FlatButton 
        containerElement="label"
        hoverColor="rgba(255,255,255,0.3)" 
        rippleColor="white" 
        style={prefixer({ color: 'white' })} 
        label={this.renderFileButtonLabel()}
        disabled={photoUploadProgress ? true : false}>
        <input 
          style={prefixer({ display: 'none' })} 
          type="file" 
          id="fileInput" 
          onChange={() => this.onFileSelected()}
        />
      </FlatButton>
    )
  }

  renderUploadButton() {
    const { user } = this.props;
    if (user.photoUploadProgress) return null;
    if (!this.state.selectedFile) return null;
    return (
      <FlatButton 
        hoverColor="rgba(255,255,255,0.3)" 
        rippleColor="white" 
        style={prefixer({ color: 'white' })} 
        label={"Subir Foto"}
        disabled={user.photoUploadProgress ? true : false}
        onTouchTap={() => this.onFileButtonPress()}
      />
    )
  }

  renderChangePasswordButton() {
    return (
      <FlatButton 
        hoverColor="rgba(255,255,255,0.3)" 
        rippleColor="white" 
        style={prefixer({ color: 'white' })} 
        label={"Cambiar Password"}
        disabled={false}
        onTouchTap={() => this.onChangePasswordButtonPress()}
      />
    )
  }

  renderSpinner() {
    const { user } = this.props;
    if (!user.photoUploadProgress) return null;
    return <CircularProgress color="white"/>
  }

  renderFileButtonLabel() {
    if (!this.state.selectedFile) return 'Cambiar Foto';
    const validFormat = String('image/jpeg image/gif image/png').indexOf(this.state.selectedFile.type) > -1;
    return validFormat ? this.state.selectedFile.name : 'Tipo de archivo inválido';
  }

  render() {
    const { profile } = this.props.user;
    if (!profile) return null;
    return (
      <div 
        style={prefixer({ 
          flex: 1, 
          backgroundColor: 'white', 
          marginTop: 64 
        })} 
        className="animated fadeIn"
      >
        <div className={css(styles.jumbotronContainer)}>
          <Avatar size={164} src={profile.photoURL ? baseURL + profile.photoURL : fallbackImage}/>
        </div>
        <div className={css(styles.uploadContainer)}>
          { this.renderSelectFileButton() }
          { this.renderUploadButton() }
          { this.renderChangePasswordButton() }
          { this.renderSpinner() }
        </div>
        <List style={{ backgroundColor: 'white' }}>
          <ListItem primaryText={"Nombre: " + profile.displayName} />
          <Divider/>
          <ListItem primaryText={"Email: " + profile.email} />
          <Divider/>
          <ListItem primaryText={"País: " + (profile.country ? profile.country.name : 'País no asignado') } />
          <Divider/>
          <ListItem primaryText={"Business Unit: " + (profile.businessUnit ? profile.businessUnit.name : 'BU no asignado') } />
          <Divider/>
          <ListItem primaryText={"Ranking Asignado: " + (profile.ranking ? profile.ranking.name : 'Ranking no asignado') } />
        </List>
        <ChangePasswordDialog />
      </div>
    );
  }
}

const styles = StyleSheet.create({
  jumbotronContainer: prefixer({ 
    width: '100%', 
    height: 200, 
    backgroundColor: 'rgb(113,30,30)', 
    display: 'flex', 
    justifyContent: 'center', 
    alignItems: 'center' 
  }),
  uploadContainer: prefixer({ 
    width: '100%', 
    minHeight: 50, 
    paddingTop: spacing.desktopGutterMini,
    paddingBottom: spacing.desktopGutterMini,
    backgroundColor: 'rgb(105, 15, 15)', 
    display: 'flex', 
    flexDirection: 'column',
    justifyContent: 'center', 
    alignItems: 'center' 
  })
})

function mapStateToProps({ auth, user }) {
  return { auth, user };
}
 
export default connect(mapStateToProps, Actions)(Profile);