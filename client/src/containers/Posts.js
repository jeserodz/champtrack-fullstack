import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import PostCard from '../components/PostCard';
import { StyleSheet, css } from 'aphrodite';

class Posts extends Component {
  componentDidMount() {
    const { auth: { user } } = this.props;
    this.props.fetchPosts();
    this.props.clearUnreadNotifications(user.id, 'Post');
  }

  renderPosts() {
    const { list } = this.props.posts;
    const { user } = this.props.auth;
    const { likePost } = this.props;

    return list.map(post => (
      <PostCard 
        key={post.id} 
        post={post} 
        user={user}
        toggleLike={likePost}
      />
    ));
  }
  
  render() {
    return (
      <div className={css(styles.container)}>
        { this.renderPosts() }
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    marginTop: 64
  }
});

function mapStateToProps({ posts, auth }) {
  return { posts, auth };
}

export default connect(mapStateToProps, Actions)(Posts);