import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import { baseURL } from '../api';
import { Card, CardHeader, CardTitle, CardText, CardActions, FlatButton, List, Subheader, ListItem, Divider, Checkbox, RaisedButton, Chip, LinearProgress } from 'material-ui';
import { ContentSend, ActionCheckCircle, NavigationCancel, ImageAdjust } from 'material-ui/svg-icons';
import ConfirmDialog from '../components/ConfirmDialog';
import { spacing, colors } from 'material-ui/styles';
import background from '../images/survey-bg.jpg';
import profileImageFallback from '../images/avatar-placeholder.png';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);

class Messages extends Component {

  state = {
    confirmModal: false,
    confirmMessageId: null
  }

  componentDidMount() {
    const { auth: { user } } = this.props;
    this.props.fetchMessages(user.id);
    this.props.clearUnreadNotifications(user.id, 'Message');
  }

  showConfirm(messageId) {
    this.setState({ confirmModal: true, confirmMessageId: messageId });
  }

  onConfirm() {
    this.props.hideMessage(this.state.confirmMessageId);
    this.setState({ confirmModal: false, confirmMessageId: null });
  }

  onCancel() {
    this.setState({ confirmModal: false, confirmMessageId: null });
  }

  renderMessagesList() {
    const { list } = this.props.messages;
    return list.map(msg => {
      return (
        <Card key={msg.id} className="animated fadeIn" style={{ margin: spacing.desktopGutterLess }}>
          <CardHeader
            title={msg.fromUser.displayName}
            subtitle={msg.fromUser.email}
            avatar={msg.fromUser.photoURL ? baseURL + msg.fromUser.photoURL : profileImageFallback}
          />
          <CardText className="animated fadeIn">
            {msg.text}
          </CardText>
          <CardActions>
            <FlatButton label="Ocultar" onTouchTap={() => this.showConfirm(msg.id) } />
          </CardActions>
        </Card>
      )
    });
  }

  render() {
    return (
      <div style={prefixer({ flex: 1, marginTop: 64 })} className="animated fadeIn">
        <div 
          className="animated fadeIn"
          style={prefixer({ 
            width: '100%', 
            height: 250, 
            backgroundColor: 'rgb(113, 30, 30)', 
            backgroundImage: `url("${background}")`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            position: 'relative',
            boxShadow: '0 1px 5px #000'
          })}>
          <span style={prefixer({ 
            color: 'white', 
            position: 'absolute', 
            fontSize: 48, 
            top: 15, 
            left: 15, 
            fontWeight: 100,
            textShadow: '1px 1px 1px #000'
        })}>
            Mensajes
          </span>
        </div>
        <div>
          { this.renderMessagesList() }
        </div>
        <ConfirmDialog 
          open={this.state.confirmModal}
          onConfirm={this.onConfirm.bind(this)}
          onCancel={this.onCancel.bind(this)}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ messages, auth }) => ({ messages, auth });

export default connect(mapStateToProps, Actions)(Messages);