import _ from 'lodash';
import api, { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import * as Utils from '../utils';
import { ContentDrafts, ImageBlurOn, ActionThumbUp, NavigationArrowBack } from 'material-ui/svg-icons';
import { colors, spacing } from 'material-ui/styles';
import backgroundFallback from '../images/rankings-background.jpg';
import profileImageFallback from '../images/avatar-placeholder.png';
import xlsIcon from '../images/xls.png';
import backgroungLevelVeryHigh from '../images/ranking-level-very-high.png';
import backgroungLevelHigh from '../images/ranking-level-high.png';
import backgroungLevelBase from '../images/ranking-level-base.png';
import SelectRankingDialog from '../components/SelectRankingDialog';
import SelectBUDialog from '../components/SelectBUDialog';
import SelectCountryDialog from '../components/SelectCountryDialog';
import SelectRankingPeriodDialog from '../components/SelectRankingPeriodDialog';
import DownloadExcelDialog from '../components/DownloadExcelDialog';
import { StyleSheet, css } from 'aphrodite';
import {
  List,
  ListItem,
  Card,
  CardHeader,
  CardActions,
  CardText,
  Divider,
  Avatar,
  RaisedButton,
  FlatButton,
  SelectField,
  FloatingActionButton,
  CircularProgress
} from 'material-ui';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column'
  },
  heroImage: {
    width: '100%',
    // minHeight: 250,
    backgroundColor: 'rgb(113, 30, 30)',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    position: 'relative',
    marginTop: 64
  },
  heroImageTitle: {
    color: 'white',
    position: 'absolute',
    fontSize: 48,
    top: 15,
    left: 15,
    fontWeight: 100,
    textShadow: '0px 1px 1px rgba(0,0,0,0.65)',
    marginRight: '1.6em'
  },
  exportButtonStyle: {
    position: 'absolute',
    top: '1em',
    right: '1em',
    height: '3em',
    padding: '0.2em',
    borderRadius: '0.3em',
    border: '1px solid rgba(0, 0, 0, 0.29)',
    cursor: 'pointer',
    backgroundColor: 'rgba(255, 255, 255, 0.75)',
    transition: 'all .2s ease',
    ':hover': {
      backgroundColor: 'rgba(255, 255, 255, 1)',
      boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.10)'
    }
  },
  filtersContainer: {
    backgroundColor: 'white'
  },
  filterStyle: {
    margin: spacing.desktopGutterMini
  },
  selectionFeedback: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white' ,
    marginTop: 64
  },
  cardStyle: {},
  cardContainerStyle: {},
  cardHeaderStyle: {
    display: 'flex',
    paddingBottom: 0
  },
  rankingPosition: {
    position: 'relative',
    left: -32,
    top: 42,
    float: 'left'
  },
  top10Container: {
    minHeight: 120,
    backgroundImage: `url('${require('../images/top10.png')}')`,
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    marginTop: spacing.desktopGutterLess,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  top10SeeMoreContainer: {
    flex: 1,
    paddingTop: spacing.desktopGutterMore,
    paddingBottom: spacing.desktopGutterMore,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  backToTop10FloatingButton: {
    position: 'fixed',
    bottom: spacing.desktopGutter,
    right: spacing.desktopGutter,
    zIndex: 3000
  },
  loadingContainer: {
    flex: 1,
    paddingTop: spacing.desktopGutterMore,
    paddingBottom: spacing.desktopGutterMore,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

class Rankings extends Component {

  componentWillMount() {
    // select user ranking automatically
    const { auth: { user }, select } = this.props;
    if (user.rankingId) {
      select('ranking', user.rankingId);
    }
  }

  componentDidMount() {
    this.props.fetchRankings();
    this.props.fetchRoles();
    window.scrollTo(0,0);
  }

  onExportToExcelClicked() {
    const {
      rankings,
      records,
      accumulatedRecords,
      accumulatedRecordsYear,
      selectedRanking,
      selectedPeriod,
      selectedBUs,
      selectedCountries,
    } = this.props.rankings;

    const ranking = _.find(rankings, { id: selectedRanking });
    const period = _.find(ranking.periods, { id: selectedPeriod });

    let filteredRecords = _.filter(selectedPeriod ? records : accumulatedRecords, record => {
      let include = true;
      if (!_.isEmpty(selectedBUs) && !_.includes(selectedBUs, record.user.businessUnitId))
        include = false;
      if (!_.isEmpty(selectedCountries) && !_.includes(selectedCountries, record.user.countryId))
        include = false;
      return include;
    });

    const recordsList = _.map(filteredRecords, record => {
      const excelRecord = {
        'Ranking': ranking.name,
        'Período': (period) ? period.name : (accumulatedRecordsYear || 'Acumulado'),
        'Inicio de Período': (period) ? period.startDate : ranking.periods[0].startDate,
        'Fin de Período': (period) ? period.endDate : ranking.periods[ranking.periods.length-1].endDate,
        'Participante': record.user.displayName,
        'Email': record.user.email,
        'Puntuación': record.score
      };

      _.forEach(record.data, data => {
        excelRecord[data.variable.name] = data.value;
      });

      return excelRecord;
    });

    const access_token = window.localStorage.getItem('access_token');

    api.post('rankings/exportToExcel', { recordsList }, { params: { access_token }})
      .then(({ data }) => {
        this.props.openDownloadExcelDialog(data);
      });
  }

  renderRankingTitle() {
    const { rankings, selectedRanking } = this.props.rankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking || !ranking.showTitle) return null;
    return <span className={css(styles.heroImageTitle)} children={ranking.name} />
  }

  renderExportToExcelButton() {
    const { auth } = this.props;
    const { rankings, selectedRanking, loading, accumulatedLoading } = this.props.rankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!Utils.isAdmin(auth.user) || !ranking || loading || accumulatedLoading) return null;
    return (
      <img
        title="Descargar ranking actual en formato Excel"
        className={'animated fadeInRight ' + css(styles.exportButtonStyle)}
        src={xlsIcon}
        onTouchTap={() => this.onExportToExcelClicked()}
      />
    )
  }

  renderSelectRankingButton() {
    const { rankings: { selectedRanking }, auth } = this.props;
    if (!Utils.hasRoles(auth.user) || !selectedRanking) return null;
    return (
      <RaisedButton
        className="animated fadeInLeft"
        style={{ position: 'absolute', bottom: 16, left: 16 }}
        label="Rankings"
        onTouchTap={this.props.openSelectRankingDialog}
      />
    )
  }

  renderSelectRankingPeriodButton() {
    const { rankings, selectedRanking, selectedPeriod, accumulatedRecordsYear } = this.props.rankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return null;
    const period = _.find(ranking.periods, { id: selectedPeriod });
    return (
      <RaisedButton
        className="animated fadeInRight"
        style={{ position: 'absolute', bottom: 16, right: 16 }}
        primary
        labelColor="#FFF"
        label={ (period) ? period.name : (accumulatedRecordsYear || 'Acumulado') }
        onTouchTap={this.props.openSelectRankingPeriodDialog}
      />
    )
  }

  getBackgroundImage() {
    const { rankings, selectedRanking } = this.props.rankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return null;
    return (ranking.photoURL) ? baseURL + ranking.photoURL : backgroundFallback;
  }

  renderFilters() {
    const { auth } = this.props;
    const { rankings, selectedRanking } = this.props.rankings;
    if (_.isEmpty(rankings) || !selectedRanking) return null;
    const displayBUFilter = Utils.isAdmin(auth.user);
    return <div className={css(styles.filtersContainer)}>
        <FlatButton
          label="Filtrar BU"
          style={{ display: displayBUFilter ? null : 'none', margin: spacing.desktopGutterMini }}
          onTouchTap={(e) => {
            e.preventDefault();
            e.stopPropagation();
            this.props.openSelectBUDialog();
          }}
        />
        <FlatButton
          label="Filtrar Países"
          style={{ margin: spacing.desktopGutterMini }}
          onTouchTap={(e) => {
            e.preventDefault();
            e.stopPropagation();
            this.props.openSelectCountryDialog();
          }}
        />
    </div>
  }

  renderTop10() {
    const { rankings, selectedRanking, viewTop10 } = this.props.rankings;
    if (!viewTop10) return null;
    if (_.isEmpty(rankings) || !selectedRanking) return null;
    return <div className={css(styles.top10Container) + ' animated bounceIn'} />
  }

  renderSeeMoreButton() {
    const { loading, accumulatedLoading, viewTop10 } = this.props.rankings;
    if (loading || accumulatedLoading || !viewTop10) return null;
    return (
      <div className={css(styles.top10SeeMoreContainer)}>
        <RaisedButton label="VER MÁS" onTouchTap={() => {
          this.props.viewRest();
          window.scrollTo(0,0);
        }}/>
      </div>
    )
  }

  renderBackToTop10() {
    if (this.props.rankings.viewTop10) return null;
    return (
      <FloatingActionButton
        className={css(styles.backToTop10FloatingButton) + ' animated bounceInRight'}
        onTouchTap={() => this.props.viewTop10() }
      >
        <NavigationArrowBack/>
      </FloatingActionButton>

    )
  }

  getLikeButtonColor(record) {
    const myLike = _.find(record.likes, { userId: this.props.auth.user.id });
    return myLike ? colors.red400 : '#757575';
  }

  onLikePress(record) {
    const { unlikeRecord, likeRecord, auth } = this.props;
    const myLike = _.find(record.likes, { userId: this.props.auth.user.id });
    myLike ? unlikeRecord(record, auth.user) : likeRecord(record, auth.user);
  }

  renderRecords() {
    const {
      viewTop10,
      selectedPeriod,
      records,
      accumulatedRecords,
      selectedBUs,
      selectedCountries,
    } = this.props.rankings;

    let filteredRecords = _.filter(selectedPeriod ? records : accumulatedRecords, record => {
      let include = true;
      if (!_.isEmpty(selectedBUs) && !_.includes(selectedBUs, record.user.businessUnitId))
        include = false;
      if (!_.isEmpty(selectedCountries) && !_.includes(selectedCountries, record.user.countryId))
        include = false;
      return include;
    });

    filteredRecords = viewTop10 ? _.slice(filteredRecords, 0, 10) : _.slice(filteredRecords, 10, filteredRecords.length);

    return _.map(filteredRecords, (record, index) => {
      return (
        <Card
          key={index}
          style={{
            paddingBottom: 16,
            margin: spacing.desktopGutterLess,
            transition: 'all 2s ease'
          }}
          containerStyle={{ position: 'relative' }}
          className="animated fadeIn"
        >
          { viewTop10 ? this.renderPositionImage(index + 1) : null }
          { this.renderPositionNumber(index + ( viewTop10 ? 1 : 11)) }
          <CardHeader
            avatar={record.user.photoURL ? baseURL + record.user.photoURL : profileImageFallback}
            title={record.user.displayName}
            subtitle={`Línea: ${_.get(record, 'user.line.name', '—')} - Resultado: ${parseFloat(record.score).toFixed(2)}%`}
            actAsExpander={true}
            showExpandableButton={true}
            className={css(styles.cardHeaderStyle)}
          />
          <CardActions style={{ paddingTop: 0, paddingBottom: 0, display: 'flex' }}>
            <Avatar
              size={32}
              src={_.get(record.user, 'country.photoURL', false) ? baseURL + record.user.country.photoURL : null }
              backgroundColor="white"
              style={{
                marginTop: 3,
                marginLeft: 13,
                borderRadius: 0
              }}
            />
            <FlatButton
              style={{ textAlign: 'left', color: '#757575' }}
              icon={<ActionThumbUp color={this.getLikeButtonColor(record)}/>}
              label={record.likes.length.toString()}
              onTouchTap={() => this.onLikePress(record)}
            />
          </CardActions>
          <CardText expandable={true} style={{ paddingLeft: 0 }}  className="animated fadeIn">
            {this.renderVariableValues(record)}
          </CardText>
        </Card>
      )
    });
  }

  renderVariableValues(record) {
    return _.map(record.data, (recordData, index) => {
      return (
        <ListItem key={index}
          primaryText={`${recordData.variable.name}: ${recordData.value.toFixed(2)}%`}
          leftAvatar={<Avatar icon={<ImageBlurOn />} backgroundColor={colors.blue700} />}
          leftIcon={<ContentDrafts />}
        />
      )
    })
  }

  renderSelectionFeedback() {
    const { rankings, selectedRanking, selectedPeriod, loading } = this.props.rankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (_.isEmpty(rankings)) return (
      <div className={css(styles.selectionFeedback)}>
        { loading ? 'Cargando datos...' : 'No hay rankings registrados' }
      </div>
    )
    if (!_.isEmpty(rankings) && !selectedRanking) return (
      <div className={css(styles.selectionFeedback)}>
        <RaisedButton
          className="animated bounceIn"
          style={{ margin: spacing.desktopGutterMini }}
          label={"Seleccione un ranking"}
          onTouchTap={this.props.openSelectRankingDialog}
        />
      </div>
    )
  }

  renderPositionNumber(position) {
    let style = {
      position: 'absolute',
      right: 18,
      top: 60,
      height: 42,
      lineHeight: '42px',
      textShadow: '1px 1px 1px rgba(0, 0, 0, 0.65)'
    };
    if (position == 1) {
      style.fontSize = 38;
      style.fontWeight = '800';
      style.color = '#ee5350';
    } else if (position < 4) {
      style.fontSize = 34;
      style.fontWeight = '600';
      style.color = '#ee9b50';
    } else if (position < 100) {
      style.fontSize = 24;
    } else {
      style.fontSize = 15;
    }
    return (
      <div style={style}> { position } </div>
    )
  }

  renderPositionImage(position) {
    let style = {
      position: 'absolute',
      top: 46,
      right: 6,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      height: 20,
      width: 40,
    }

    if (position == 1) {
      style.backgroundImage = `url(${backgroungLevelVeryHigh})`;
    } else if (position < 4) {
      style.backgroundImage = `url(${backgroungLevelHigh})`;
    } else if (position < 11) {
      style.backgroundImage = `url(${backgroungLevelBase})`;
    }
    return <div style={style} />
  }

  renderLoading() {
    const { loading, accumulatedLoading } = this.props.rankings;
    if (!loading && !accumulatedLoading) return null;
    return (
      <div className={css(styles.loadingContainer)}>
        <CircularProgress
          thickness={5}
          color="white"
        />
      </div>
    );
  }

  render() {
    const { rankings, selectedRanking } = this.props.rankings;
    return (
      <div className={"animated fadeIn " + css(styles.container)}>
        <div
          className={"animated fadeIn " + css(styles.heroImage)}
          style={{
            backgroundImage: `url(${this.getBackgroundImage()})`,
            minHeight: !selectedRanking ? 0 : 250
          }}
        >
          { this.renderRankingTitle() }
          { this.renderExportToExcelButton() }
          { this.renderSelectRankingButton() }
          { this.renderSelectRankingPeriodButton() }
        </div>
        { this.renderFilters() }
        { this.renderTop10() }
        { this.renderBackToTop10() }
        <List>
          { this.renderRecords() }
        </List>
        { this.renderSelectionFeedback() }
        { this.renderSeeMoreButton() }
        { this.renderLoading() }
        <SelectRankingDialog/>
        <SelectBUDialog/>
        <SelectCountryDialog/>
        <SelectRankingPeriodDialog/>
        <DownloadExcelDialog/>
      </div>
    );
  }
}

const mapStateToProps = ({ rankings, auth }) => ({ rankings, auth })

export default connect(mapStateToProps, Actions)(Rankings);
