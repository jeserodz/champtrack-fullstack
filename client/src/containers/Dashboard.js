import api from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { HashRouter, Router, Route } from 'react-router-dom';
import { setupRankings,fetchCountries, userLogout, openDrawer, closeDrawer } from '../actions';
import { AppBar } from 'material-ui';
import AppMenuIcon from '../components/AppMenuIcon';
import AppMenu from '../components/AppMenu';
import Rankings from './Rankings';
import Messages from './Messages';
import Surveys from './Surveys';
import Posts from './Posts';
import Profile from './Profile';
import AdminRankings from './AdminRankings';
import AdminDatos from './AdminDatos';
import AdminUsuarios from './AdminUsuarios';
import AdminSurveys from './AdminSurveys';
import AdminPaises from './AdminPaises';
import AdminRoles from './AdminRoles';
import AdminBusinessUnits from './AdminBusinessUnits';
import AdminLines from './AdminLines';
import AdminPosts from './AdminPosts';
import AdminMessages from './AdminMessages';
import headerLogo from '../images/header-logo.png';
import { StyleSheet, css } from 'aphrodite';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);

class Dashboard extends Component {

  state = { filterText: '' }

  logout() {
    window.localStorage.clear();
    this.props.userLogout();
  }

  toggleDrawer() {
    const { ux, openDrawer, closeDrawer } = this.props;
    ux.drawer ? closeDrawer() : openDrawer();
  }

  render() {
    return (
          <div id="dashboard" className={css(styles.container)}>
            <AppBar 
              style={{ position: 'fixed' }} 
              iconElementLeft={<AppMenuIcon />}
              onLeftIconButtonTouchTap={this.toggleDrawer.bind(this)}
              children={<div className={css(styles.appBarLogo)}/>}
            />
            <div className={css(styles.contentWrapperStyle)}>
              <AppMenu history={this.props.history} onLogoutPress={this.logout.bind(this)} />
              <div className={css(styles.contentWrapperStyle)}>
                <Route exact path="/" component={Rankings}/>
                <Route exact path="/mensajes" component={Messages}/>
                <Route path="/surveys" component={Surveys}/>
                <Route path="/mktnews" component={Posts}/>
                <Route path="/profile" component={Profile}/>
                <Route path="/admin/rankings" component={AdminRankings}/>
                <Route path="/admin/datos" component={AdminDatos}/>
                <Route path="/admin/usuarios" component={AdminUsuarios}/>
                <Route path="/admin/surveys" component={AdminSurveys}/>
                <Route path="/admin/paises" component={AdminPaises}/>
                <Route path="/admin/roles" component={AdminRoles}/>
                <Route path="/admin/businessunits" component={AdminBusinessUnits}/>
                <Route path="/admin/lines" component={AdminLines}/>
                <Route path="/admin/mktnews" component={AdminPosts}/>
                <Route path="/admin/mensajes" component={AdminMessages}/>
              </div>
            </div>
          </div>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
      flex: 1,
      display: 'flex', 
      flexDirection: 'column', 
      height: '100%',
      backgroundColor: 'rgb(113,30,30)',
  },
  appBarLogo: { 
    width: '100%',
    height: '100%',
    backgroundImage: `url(${headerLogo})`,
    backgroundPosition: 'center',
    backgroundSize: '80px 40px',
    backgroundRepeat: 'no-repeat',
    position: 'absolute',
    left: 0,
    top: 0
  },
  contentWrapperStyle: { 
    flex: 1, 
    display: 'flex',
  },
  routeWrapperStyle: { 
    flex: 1
  }
})

function mapStateToProps({ ux, router }) {
  return { ux, router }
}

export default connect(mapStateToProps, { setupRankings, fetchCountries, userLogout, openDrawer, closeDrawer })(Dashboard);