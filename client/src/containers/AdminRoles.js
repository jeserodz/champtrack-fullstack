import _ from 'lodash';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, css } from 'aphrodite';
import styled from 'styled-components';
import { colors, spacing } from 'material-ui/styles';
import { EditorModeEdit } from 'material-ui/svg-icons';
import {
  Paper,
  List,
  ListItem,
  Subheader,
  Avatar,
  RaisedButton,
  TextField,
  Checkbox,
} from "material-ui";
import {
  fetchRoles,
  selectRole,
  createRole,
  updateRole,
  fetchRankings,
  addRankingToRole,
  removeRankingFromRole,
} from "../actions/AdminRolesActions";

class AdminRoles extends Component {
  
  INITAL_STATE = { 
    id: null,
    name: ''
  }

  state = this.INITAL_STATE

  componentDidMount() {
    this.props.fetchRoles();
    this.props.fetchRankings();
  }

  componentWillReceiveProps(nextProps) {
    const { roles, selectedRole } = nextProps.adminRoles;
    const role = _.find(roles, { id: selectedRole })
    if (role) {
      this.setState({ ...role });
    } else {
      this.setState({ ...this.INITAL_STATE });
    }
  }

  onInputChange(field, value) {
    this.setState({ [field]: value });
  }

  onSavePress() {
    const { roles, selectedRole } = this.props.adminRoles;
    const role = _.find(roles, { id: selectedRole })
    if (role) {
      this.props.updateRole({ ...this.state });
    } else {
      this.props.createRole({ ...this.state });
    }
  }

  onCancelPress() {
    this.props.selectRole(null);
    this.setState({ ...this.INITAL_STATE })
  }

  onRankingCheckboxPress(roleId, rankingId, checked) {
    return checked 
      ? this.props.addRankingToRole(roleId, rankingId)
      : this.props.removeRankingFromRole(roleId, rankingId);
  }

  renderRoles() {
    const { roles, selectedRole } = this.props.adminRoles;
    if (!roles.length) return <ListItem primaryText="Aún no tiene Roles para mostrar"/>
    return _.map(roles, (role, id) => {
      return (
        <ListItem 
          key={id}
          rightIcon={role.name !== 'Administrator' ? <EditorModeEdit/> : null}
          primaryText={role.name}
          style={{ color: selectedRole === role.id ? colors.red400 : null }}
          onTouchTap={() => role.name !== 'Administrator' ? this.props.selectRole(role.id) : null}
        />
      );
    });
  }

  renderRankings() {
    const { rankings, roles, selectedRole } = this.props.adminRoles;
    const role = _.find(roles, { id: selectedRole });
    return rankings.map(ranking => (
      <ListItem
        key={ranking.id}
        primaryText={ranking.name}
        leftCheckbox={(
          <Checkbox
            disabled={!selectedRole}
            checked={(role && _.find(role.rankings, { id: ranking.id})) ? true : false}
            onCheck={(evt, checked) => this.onRankingCheckboxPress(selectedRole, ranking.id, checked)}
          />
        )}
      />
    ));
  }

  render() {
    const { roles, selectedRole } = this.props.adminRoles;
    return (
      <div className={css(styles.container)}>
        <Paper className={css(styles.leftPane)}>
          <List>
            <Subheader>Lista de Roles</Subheader>
            { this.renderRoles() }
          </List>
        </Paper>
        <Paper className={css(styles.rightPane)}>
          <TextField
            floatingLabelText="Nombre del Rol"
            value={this.state.name}
            onChange={(e) => this.onInputChange('name', e.target.value)}
          />
          <br />
          <RaisedButton 
            primary 
            style={{ marginBottom: spacing.desktopGutterLess }}
            label={ selectedRole ? "Actualizar Rol" : "Agregar Rol" }
            disabled={ _.isEmpty(this.state.name) }
            onTouchTap={() => this.onSavePress()}
          />
          <br/>
          <RaisedButton 
            label="Cancelar"
            onTouchTap={() => this.onCancelPress()}
          />
          <br/>
          <RakingsList>
            <Subheader>RANKINGS SUPERVISADOS POR ESTE ROL</Subheader>
            {this.renderRankings()}
          </RakingsList>
        </Paper>
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    display: 'flex',
    marginTop: 72
  },
  leftPane: { 
    flex: 1, 
    margin: spacing.desktopGutterMini,
    overflow: 'auto'
  },
  rightPane: { 
    flex: 1, 
    margin: spacing.desktopGutterMini, 
    padding: spacing.desktopGutterLess,
    overflow: 'auto'
  },
  uploadButton: { 
    marginBottom: spacing.desktopGutterLess,
    marginRight: spacing.desktopGutterLess 
  }
});

const RakingsList = styled(List)`
  margin-top: 2rem;
`;

function mapStateToProps({ adminRoles }) {
  return { adminRoles };
}
const actions = {
  fetchRoles,
  selectRole,
  createRole,
  updateRole,
  fetchRankings,
  addRankingToRole,
  removeRankingFromRole,
};

export default connect(mapStateToProps, actions)(AdminRoles);