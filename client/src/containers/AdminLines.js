import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchBusinessUnits, selectBusinessUnit } from '../actions/AdminBusinessUnitsActions';
import { fetchLines, selectLine, updateLine, createLine } from '../actions/AdminLinesActions';
import { StyleSheet, css } from 'aphrodite';
import { Paper, List, Subheader, TextField, RaisedButton, Toolbar, ToolbarGroup, SelectField, MenuItem, ListItem } from 'material-ui';
import { colors, spacing } from 'material-ui/styles';
import { EditorModeEdit } from 'material-ui/svg-icons';

class AdminLines extends Component {
  
  INITAL_STATE = { 
    name: ''
  }

  state = this.INITAL_STATE

  componentDidMount() {
    this.props.fetchBusinessUnits();
    this.props.fetchLines();
  }

  componentWillReceiveProps(nextProps) {
    const { lines, selectedLine } = nextProps.adminLines;
    const line = _.find(lines, { id: selectedLine })
    if (line) {
      this.setState({ ...line });
    } else {
      this.setState({ ...this.INITAL_STATE });
    }
  }

  onInputChange(field, value) {
    this.setState({ [field]: value });
  }

  onSavePress() {
    const { lines, selectedLine } = this.props.adminLines;
    const { selectedBusinessUnit } = this.props.adminBusinessUnits;
    const line = _.find(lines, { id: selectedLine })
    if (line) {
      this.props.updateLine({ ...this.state });
    } else {
      this.props.createLine({ ...this.state, businessUnitId: selectedBusinessUnit });
    }
  }

  onCancelPress() {
    this.props.selectLine(null);
  }

  renderBUItems() {
    const { businessUnits } = this.props.adminBusinessUnits;
    return _.map(businessUnits, (bu, index) => {
      return (
        <MenuItem
          key={index}
          value={bu.id}
          primaryText={bu.name}
        />
      );
    });
  }

  renderLines() {
    const { selectedBusinessUnit } = this.props.adminBusinessUnits; 
    const { lines, selectedLine } = this.props.adminLines;

    if (!lines.length) return <ListItem primaryText="Aún no tiene líneas para mostrar"/>
    return _.map(lines, (line, id) => {
      if (line.businessUnitId !== selectedBusinessUnit) return null;
      return (
        <ListItem 
          key={id}
          rightIcon={<EditorModeEdit/>}
          primaryText={line.name}
          style={{ color: selectedLine === line.id ? colors.red400 : null }}
          onTouchTap={() => this.props.selectLine(line.id)}
        />
      );
    });
  }

  render() {
    const { 
      adminBusinessUnits: { selectedBusinessUnit },
      adminLines: { selectedLine }
    } = this.props;

    return (
      <div className={css(styles.container)}>
        <Toolbar>
          <ToolbarGroup>
            <SelectField
              floatingLabelText="Business Unit"
              floatingLabelFixed={true}
              value={selectedBusinessUnit}
              onChange={(e, i, id) => this.props.selectBusinessUnit(id)}
              children={this.renderBUItems()}
            />
          </ToolbarGroup>
        </Toolbar>
        <div className={css(styles.panelsContainer)}>
          <Paper className={css(styles.leftPane)}>
            <List>
              <Subheader>Lista de Líneas</Subheader>
              { this.renderLines() }
            </List>
          </Paper>
          <Paper className={css(styles.rightPane)}>
            <TextField
              floatingLabelText="Nombre de Línea"
              value={this.state.name}
              onChange={(e) => this.onInputChange('name', e.target.value)}
            /><br />
            <RaisedButton
              primary
              style={{ marginBottom: spacing.desktopGutterLess }}
              label={ selectedLine ? "Actualizar Línea" : "Agregar Línea"}
              disabled={_.isEmpty(this.state.name)}
              onTouchTap={() => this.onSavePress()}
            /><br />
            <RaisedButton
              label="Cancelar"
              onTouchTap={() => this.onCancelPress()}
            />
          </Paper>
        </div>
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    marginTop: 72,
  },
  panelsContainer: {
    flex: 1,
    display: 'flex',
  },
  leftPane: {
    flex: 1,
    margin: spacing.desktopGutterMini,
    overflow: 'auto',
  },
  rightPane: {
    flex: 1,
    margin: spacing.desktopGutterMini,
    padding: spacing.desktopGutterLess,
    overflow: 'auto',
  },
});

function mapStateToProps({ adminBusinessUnits, adminLines }) {
  return { adminBusinessUnits, adminLines };
}

export default connect(
  mapStateToProps, 
  { 
    fetchBusinessUnits, 
    selectBusinessUnit, 
    fetchLines, 
    selectLine, 
    updateLine,
    createLine
  }
)(AdminLines);