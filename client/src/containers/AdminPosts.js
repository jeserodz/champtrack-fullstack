import _ from 'lodash';
import { baseURL } from '../api';
import React, { Component } from 'react';
import * as actions from '../actions/AdminPostsActions';
import { connect } from 'react-redux';
import { StyleSheet, css } from 'aphrodite';
import { Paper, TextField, ListItem, Avatar, FlatButton, Card, CardHeader, CardMedia, CardActions } from 'material-ui';
import { colors, spacing } from 'material-ui/styles';
import { ActionAnnouncement, EditorModeEdit } from 'material-ui/svg-icons';
import Pagination from '../components/Pagination';
import CrudToolbar from '../components/CrudToolbar';
import placeholderImg from '../images/placeholder.png';

class AdminPosts extends Component {
  
  INITAL_STATE = { 
    id: null,
    title: '',
    description: '',
    photoURL: null,
    createdDate: null,
  }

  selectedFile = null

  state = this.INITAL_STATE

  componentDidMount() {
    this.props.fetchPosts();
  }
  
  componentWillReceiveProps(nextProps) {
    const { posts, selectedPost, uploadProgress } = nextProps.adminPosts;
    const post = _.find(posts, { id: selectedPost });
    
    if (uploadProgress >= 100) {
      this.selectedFile = null;
    }

    this.setState({ ...(post ? post : this.INITAL_STATE) });
  }

  onInputChange(field, value) {
    this.setState({ [field]: value });
  }

  onSavePress() {
    const { posts, selectedPost } = this.props.adminPosts;
    const { user } = this.props.auth;
    const post = _.find(posts, { id: selectedPost })
    if (post) {
      this.props.updatePost({ ...this.state });
    } else {
      this.props.createPost({ ...this.state, userId: user.id });
    }
  }

  onCancelPress() {
    this.props.selectPost(null);
  }

  onFileSelected() {
    this.selectedFile = document.getElementById('fileInput').files[0];
    this.setState({ ...this.state });
  }
  
  onUploadButtonPress() {
    if (!this.selectedFile) return null;
    const validFormat = String('image/jpeg image/gif image/png').indexOf(this.selectedFile.type) > -1;
    validFormat ? this.props.uploadPostImage(this.state.id, this.selectedFile) : null;
  }

  validate() {
    const { title, description } = this.state;
    if (title) return true;
    else return false;
  }
  
  renderFileButtonLabel() {
    if (!this.selectedFile) return 'Seleccionar Imagen';
    const validFormat = String('image/jpeg image/gif image/png').indexOf(this.selectedFile.type) > -1;
    return validFormat ? this.selectedFile.name : 'Tipo de archivo inválido';
  }

  renderImageEditor() {
    if (!this.state.id) return null;
    const { uploadProgress } = this.props.adminPosts;
    return (
      <Card style={{ maxWidth: 520, marginTop: spacing.desktopGutter }}>
        <CardHeader
          title="Imagen de Portada"
          subtitle="Previsualize o carge la imagen de portada para esta publicación"
        />
        <CardMedia>
          <img src={this.state.photoURL ? baseURL + this.state.photoURL : placeholderImg} />
        </CardMedia>
        <CardActions>
          <FlatButton 
            label={this.renderFileButtonLabel()} 
            containerElement="label"
            disabled={uploadProgress !== null}
          >
            <input 
              id="fileInput" 
              type="file" 
              style={{ display: 'none' }} 
              onChange={() => this.onFileSelected()}
            />
          </FlatButton>
          <FlatButton 
            label={uploadProgress === null ? 'Subir Imagen' : `Cargando ${uploadProgress}%...`} 
            containerElement="label"
            disabled={!this.selectedFile || uploadProgress !== null}
            onTouchTap={this.onUploadButtonPress.bind(this)}
          />
        </CardActions>
      </Card>
    );
  }

  renderItem(item, index) {
    return (
      <ListItem
        key={index}
        primaryText={item.title}
        leftAvatar={<Avatar icon={<ActionAnnouncement />} />}
        rightIcon={<EditorModeEdit />}
        onTouchTap={() => this.props.selectPost(item.id)}
        style={{ color: this.props.adminPosts.selectedPost === item.id ? 'red' : null }}
      />
    );
  }

  render() {
    const { posts } = this.props.adminPosts;
    return (
      <div className={css(styles.container)}>
        <Paper className={css(styles.leftPane)}>
          <Pagination 
            collection={posts} 
            pageSize={10} 
            render={this.renderItem.bind(this)}
          />
        </Paper>
        <Paper className={css(styles.rightPane)}>
          <CrudToolbar 
            saveButtonLabel={this.state.id ? 'Salvar' : 'Crear'}
            saveButtonDisabled={!this.validate()}
            onSavePress={this.onSavePress.bind(this)}
            onCancelPress={this.onCancelPress.bind(this)}
          />
          <TextField
            floatingLabelText="Título"
            value={this.state.title}
            onChange={e => this.onInputChange('title', e.target.value)}
          /><br />
          <TextField
            floatingLabelText="Descripción"
            value={this.state.description}
            onChange={e => this.onInputChange('description', e.target.value)}
          /><br />
          { this.renderImageEditor() }
        </Paper>
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    display: 'flex',
    marginTop: 72
  },
  leftPane: { 
    flex: 1, 
    margin: spacing.desktopGutterMini,
    overflow: 'auto'
  },
  rightPane: { 
    flex: 1, 
    margin: spacing.desktopGutterMini, 
    padding: spacing.desktopGutterLess,
    overflow: 'auto'
  },
});

function mapStateToProps({ adminPosts, auth }) {
  return { adminPosts, auth }
}

export default connect(mapStateToProps, actions)(AdminPosts);