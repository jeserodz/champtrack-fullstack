import _ from "lodash";
import validator from "validator";
import { baseURL } from "../api";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from 'react-router-dom';
import { StyleSheet, css } from "aphrodite";
import {
  fetchData,
  select,
  createUser,
  updateUser,
  uploadUserImage
} from "../actions/AdminUsersActions";
import { openAssignRolesDialog } from "../actions/UXActions";
import {
  Paper,
  List,
  ListItem,
  Subheader,
  Avatar,
  RaisedButton,
  TextField,
  SelectField,
  MenuItem
} from "material-ui";
import { colors, spacing } from "material-ui/styles";
import { EditorModeEdit } from "material-ui/svg-icons";
import Pagination from "../components/Pagination";
import AssignRolesDialog from "../components/AssignRolesDialog";
import tempFlag from "../images/flag-CR.png";
import fallbackImage from "../images/avatar-placeholder.png";

class AdminUsuarios extends Component {
  INITAL_STATE = {
    displayName: "",
    email: "",
    password: "",
    businessUnitId: null,
    countryId: null,
    rankingId: null,
    roles: [],
    active: true,
    filterText: ""
  };

  state = this.INITAL_STATE;

  selectedFile = null;

  componentDidMount() {
    this.props.fetchData();
  }

  componentWillReceiveProps(nextProps) {
    const { users, selectedUser } = nextProps.adminUsers;
    const user = _.find(users, { id: selectedUser });
    if (user) {
      this.setState({ ...this.INITAL_STATE, ...user });
    } else {
      this.setState({ ...this.INITAL_STATE });
    }
  }

  onInputChange(field, value) {
    this.setState({ [field]: value });
  }

  onSavePress() {
    const { users, selectedUser } = this.props.adminUsers;
    const user = _.find(users, { id: selectedUser });
    if (user) {
      if (!this.state.password) delete this.state.password;
      this.props.updateUser({ ...this.state });
    } else {
      this.props.createUser({ ...this.state });
    }
  }

  onCancelPress() {
    this.selectedFile = null;
    this.props.select("user", null);
  }

  onFileSelected(e) {
    this.selectedFile = e.target.files[0];
    this.setState(this.state);
  }

  onUploadPress() {
    const { users, selectedUser } = this.props.adminUsers;
    this.props.uploadUserImage(selectedUser, this.selectedFile);
  }

  userFilter(user, filterText) {
    let match = false;
    if (!filterText) return true;
    if (validator.contains(user.displayName.toLowerCase(), filterText.toLowerCase()))
      match = true;
    if (validator.contains(user.email.toLowerCase(), filterText.toLowerCase())) match = true;
    if (
      validator.contains(
        _.get(user, "country.name", "").toLowerCase(),
        filterText.toLowerCase()
      )
    )
      match = true;
    if (
      validator.contains(
        _.get(user, "businessUnit.name", "").toLowerCase(),
        filterText.toLowerCase()
      )
    )
      match = true;
      if (
        validator.contains(
          _.get(user, "line.name", "").toLowerCase(),
          filterText.toLowerCase()
        )
      )
        match = true;
    if (
      validator.contains(
        _.get(user, "ranking.name", "").toLowerCase(),
        filterText.toLowerCase()
      )
    )
      match = true;
    return match;
  }

  renderBusinessUnitItems() {
    const { businessUnits } = this.props.adminUsers;
    return _.map(businessUnits, (businessUnit, index) => {
      return (
        <MenuItem
          key={index}
          value={businessUnit.id}
          primaryText={businessUnit.name}
        />
      );
    });
  }

  renderLineItems() {
    const { lines } = this.props.adminUsers;
    return _.map(lines, (line, index) => {
      if (line.businessUnitId !== this.state.businessUnitId) return null;
      return <MenuItem key={index} value={line.id} primaryText={line.name} />;
    });
  }

  renderCountryItems() {
    const { countries } = this.props.adminUsers;
    return _.map(countries, (country, index) => {
      return (
        <MenuItem key={index} value={country.id} primaryText={country.name} />
      );
    });
  }

  renderRankingItems() {
    const { rankings } = this.props.adminUsers;
    return _.map(rankings, (ranking, index) => {
      return (
        <MenuItem key={index} value={ranking.id} primaryText={ranking.name} />
      );
    });
  }

  renderUploadBtn() {
    const { users, selectedUser, uploadProgress } = this.props.adminUsers;
    const user = _.find(users, { id: selectedUser });

    if (uploadProgress > 99) {
      this.selectedFile = null;
    }

    let label;
    let valid = false;

    if (!this.selectedFile) label = "Agregar Imagen";
    else {
      if (
        String("image/jpg image/jpeg image/gif image/png").match(
          this.selectedFile.type
        )
      ) {
        label = this.selectedFile.name;
        valid = true;
      } else {
        label = "Archivo seleccionado inválido.";
      }
    }

    return (
      <div>
        <RaisedButton
          secondary
          className={css(styles.uploadButton)}
          label={label}
          containerElement="label"
          disabled={!selectedUser || uploadProgress ? true : false}
        >
          <input
            id="fileInput"
            style={{ display: "none" }}
            type="file"
            disabled={selectedUser == null || uploadProgress}
            onChange={e => this.onFileSelected(e)}
          />
        </RaisedButton>
        <RaisedButton
          secondary
          style={{
            display: !valid ? "none" : "inline-block",
            marginBottom: spacing.desktopGutterLess
          }}
          label="Subir Imagen"
          onTouchTap={() => this.onUploadPress()}
        />
      </div>
    );
  }

  validate() {
    let isValid = true;
    if (!this.state.displayName) isValid = false;
    if (!validator.isEmail(this.state.email)) isValid = false;
    return isValid;
  }

  renderUser(user) {
    const { selectedUser } = this.props.adminUsers;

    return (
      <ListItem
        key={user.id}
        leftAvatar={
          <Avatar
            src={user.photoURL ? baseURL + user.photoURL : fallbackImage}
          />
        }
        rightIcon={<EditorModeEdit />}
        primaryText={user.displayName}
        secondaryTextLines={2}
        secondaryText={
          <p>
            <span>
              <strong>{"BU: "}</strong>{" "}
              {_.get(user, "businessUnit.name", "Sin BU")}
              <strong>{" | País: "}</strong>{" "}
              {_.get(user, "country.name", "Sin País")}
              <strong>{" | Ranking: "}</strong>{" "}
              {_.get(user, "ranking.name", "Sin Ranking")}
            </span>
            <br />
            <span>
              <strong>{"Email: "}</strong> {user.email}
            </span>
          </p>
        }
        style={{ color: selectedUser === user.id ? colors.red400 : null }}
        onTouchTap={() => this.props.select("user", user.id)}
      />
    );
  }

  render() {
    const { users, selectedUser } = this.props.adminUsers;
    return (
      <div className={css(styles.container)}>
        <Paper className={css(styles.leftPane)}>
          <Pagination
            collection={users}
            pageSize={10}
            render={this.renderUser.bind(this)}
            filterHintText={"Filtrar usuarios..."}
            filter={this.userFilter}
          />
        </Paper>
        <Paper className={css(styles.rightPane)}>
          <TextField
            floatingLabelText="Nombre del usuario"
            value={this.state.displayName}
            onChange={e => this.onInputChange("displayName", e.target.value)}
          />
          <br />
          <TextField
            floatingLabelText="Email"
            value={this.state.email}
            type="email"
            onChange={e => this.onInputChange("email", e.target.value)}
          />
          <br />
          <TextField
            floatingLabelText="Contraseña"
            value={this.state.password}
            type="password"
            autoComplete="new-password"
            onChange={e => this.onInputChange("password", e.target.value)}
          />
          <br />
          <SelectField
            floatingLabelText="Business Unit"
            floatingLabelFixed={true}
            value={this.state.businessUnitId || ""}
            onChange={(e, i, id) => this.onInputChange("businessUnitId", id)}
          >
            <MenuItem value={""} primaryText={"Ninguno"} />
            {this.renderBusinessUnitItems()}
          </SelectField>
          <br />
          <SelectField
            floatingLabelText="Línea"
            floatingLabelFixed={true}
            value={this.state.lineId || ""}
            onChange={(e, i, id) => this.onInputChange("lineId", id)}
            children={this.renderLineItems()}
          />
          <br />
          <SelectField
            floatingLabelText="País"
            floatingLabelFixed={true}
            value={this.state.countryId || ""}
            onChange={(e, i, id) => this.onInputChange("countryId", id)}
          >
            <MenuItem value={""} primaryText={"Ninguno"} />
            {this.renderCountryItems()}
          </SelectField>
          <br />
          <SelectField
            floatingLabelText="Ranking"
            floatingLabelFixed={true}
            value={this.state.rankingId}
            onChange={(e, i, id) => this.onInputChange("rankingId", id)}
          >
            <MenuItem value={null} primaryText={"Ninguno"} />
            {this.renderRankingItems()}
          </SelectField>
          <br />
          <List>
            <Subheader>Roles Asignados</Subheader>
            {this.state.roles.length ? (
              this.state.roles.map(role => <ListItem primaryText={role.name} key={role.id} />)
            ) : (
              <ListItem primaryText={"Ninguno"} />
            )}
          </List>
          <RaisedButton
            style={{
              display: selectedUser ? null : "none"
            }}
            primary
            label={"Asignar Roles"}
            onTouchTap={() => this.props.openAssignRolesDialog()}
          />
          <br />
          <RaisedButton
            primary
            style={{ marginBottom: spacing.desktopGutterLess }}
            label={selectedUser ? "Actualizar Usuario" : "Crear Usuario"}
            disabled={this.validate() ? false : true}
            onTouchTap={() => this.onSavePress()}
          />
          <br />
          {this.renderUploadBtn()}
          <RaisedButton
            label="Cancelar"
            onTouchTap={() => this.onCancelPress()}
          />
        </Paper>
        <AssignRolesDialog />
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    marginTop: 72
  },
  leftPane: {
    flex: 1,
    margin: spacing.desktopGutterMini,
    overflow: "auto"
  },
  rightPane: {
    flex: 1,
    margin: spacing.desktopGutterMini,
    padding: spacing.desktopGutterLess,
    overflow: "auto"
  },
  uploadButton: {
    marginBottom: spacing.desktopGutterLess,
    marginRight: spacing.desktopGutterLess
  }
});

function mapStateToProps({ adminUsers }) {
  return { adminUsers };
}

export default connect(mapStateToProps, {
  fetchData,
  select,
  createUser,
  updateUser,
  uploadUserImage,
  openAssignRolesDialog
})(AdminUsuarios);
