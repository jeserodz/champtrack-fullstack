import _ from 'lodash';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchRankings, select, create } from '../actions/AdminRankingsActions';
import { spacing, colors } from 'material-ui/styles';
import { ContentAdd, ContentSort } from 'material-ui/svg-icons';
import {
  Paper,
  List,
  ListItem,
  Divider,
  Subheader,
  Tabs,
  Tab,
  FloatingActionButton,
  Avatar
} from 'material-ui';
import RankingParticipants from '../components/RankingParticipants';
import RankingVariables from '../components/RankingVariables';
import RankingPeriods from '../components/RankingPeriods';
import RankingSettings from '../components/RankingSettings';
import EditRankingDialog from '../components/EditRankingDialog';
import { StyleSheet, css } from 'aphrodite';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);


class AdminRankings extends Component {

  state = { addRankingDialogOpen: false, editRankingDialogOpen: false }

  componentDidMount() {
    this.props.fetchRankings();
  }

  renderRankingsListItems() {
    const { rankings, selectedRanking } = this.props.adminRankings;
    if (_.isEmpty(rankings)) return null;
    return _.map(rankings, (ranking, index) => {
      return (
        <span key={index}>
          <Divider />
          <ListItem 
            leftAvatar={<Avatar src={ ranking.photoURL ? baseURL + ranking.photoURL : null } backgroundColor={colors.cyan800} />}
            style={prefixer({ color: selectedRanking === ranking.id ? colors.red600 : null })}
            primaryText={ranking.name} 
            onTouchTap={() => this.props.select('ranking', ranking.id)}
          />
          <Divider />
        </span>
      );
    });
  }

  render() {
    return (
      <div className={css(styles.c1)}>
        <Paper zDepth={1} style={prefixer({ flex: 1, overflowY: 'auto' })}>
          <List>
            <Subheader>Rankings</Subheader>
            {this.renderRankingsListItems()}
          </List>
        </Paper>
        <Paper zDepth={1} style={prefixer({ flex: 2, marginLeft: spacing.desktopGutter, overflowY: 'auto' })}>
          <Tabs>
            <Tab label="Participantes" style={prefixer({ flex: 1 })}>
              <RankingParticipants />
            </Tab>
            <Tab label="Variables">
              <RankingVariables />
            </Tab>
            <Tab label="Períodos">
              <RankingPeriods />
            </Tab>
            <Tab label="Ajustes">
              <RankingSettings />
            </Tab>
          </Tabs>
        </Paper>
        <FloatingActionButton 
          style={prefixer({ position: 'fixed', bottom: spacing.desktopGutter, right: spacing.desktopGutter })}
          onTouchTap={() => this.setState({ addRankingDialogOpen: true })}
        >
          <ContentAdd />
        </FloatingActionButton>

        <EditRankingDialog 
          open={this.state.addRankingDialogOpen}
          title="Crear Ranking"
          onConfirm={(rankingName) => this.props.create('ranking', { name: rankingName })}
          onClose={() => this.setState({ ...this.state, addRankingDialogOpen: false })}
        />
      </div>
    );
  }
}

const styles = StyleSheet.create({
  c1: { 
    flex: 1, 
    display: 'flex', 
    margin: spacing.desktopGutter,
    marginTop: 72
  }
})

function mapStateToProps({ adminRankings }) {
  return { adminRankings }
}

export default connect(mapStateToProps, { fetchRankings, select, create })(AdminRankings);