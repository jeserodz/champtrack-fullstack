import _ from 'lodash';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, css } from 'aphrodite';
import { fetchCountries, selectCountry, createCountry, updateCountry, uploadCountryImage } from '../actions/AdminCountriesActions';
import { Paper, List, ListItem, Subheader, Avatar, RaisedButton, TextField } from 'material-ui';
import { colors, spacing } from 'material-ui/styles';
import { EditorModeEdit } from 'material-ui/svg-icons';
import tempFlag from '../images/flag-CR.png';

class AdminPaises extends Component {
  
  INITAL_STATE = { 
    name: ''
  }

  state = this.INITAL_STATE

  selectedFile = null

  componentDidMount() {
    this.props.fetchCountries();
  }

  componentWillReceiveProps(nextProps) {
    const { countries, selectedCountry } = nextProps.adminCountries;
    const country = _.find(countries, { id: selectedCountry })
    if (country) {
      this.setState({ ...country });
    } else {
      this.setState({ ...this.INITAL_STATE });
    }
  }

  onInputChange(field, value) {
    this.setState({ [field]: value });
  }

  onSavePress() {
    const { countries, selectedCountry } = this.props.adminCountries;
    const country = _.find(countries, { id: selectedCountry })
    if (country) {
      this.props.updateCountry({ ...this.state });
    } else {
      this.props.createCountry({ ...this.state });
    }
  }

  onCancelPress() {
    this.selectedFile = null;
    this.props.selectCountry(null);
  }

  onFileSelected(e) {
    this.selectedFile = e.target.files[0];
    this.setState(this.state);
  }

  onUploadPress() {
    const { countries, selectedCountry } = this.props.adminCountries;
    this.props.uploadCountryImage(selectedCountry, this.selectedFile);
  }

  renderCountries() {
    const { countries, selectedCountry } = this.props.adminCountries;
    if (!countries.length) return <ListItem primaryText="Aún no tiene países para mostrar"/>
    return _.map(countries, (country, id) => {
      return (
        <ListItem 
          key={id}
          leftAvatar={<Avatar src={country.photoURL ? baseURL + country.photoURL : null }/>} 
          rightIcon={<EditorModeEdit/>}
          primaryText={country.name}
          style={{ color: selectedCountry === country.id ? colors.red400 : null }}
          onTouchTap={() => this.props.selectCountry(country.id)}
        />
      );
    });
  }

  renderUploadBtn() {
    const { countries, selectedCountry, uploadProgress } = this.props.adminCountries;
    const country = _.find(countries, { id: selectedCountry });

    if (uploadProgress > 99) {
      this.selectedFile = null;
    } 

    let label;
    let valid = false;

    if (!this.selectedFile) label = "Agregar Imagen";
    else {
      if (String("image/jpg image/jpeg image/gif image/png").match(this.selectedFile.type)) {
        label = this.selectedFile.name;
        valid = true;
      } else {
        label = "Archivo seleccionado inválido."
      }
    }

    return (
      <div>
        <RaisedButton 
          secondary
          className={css(styles.uploadButton)}
          label={label}
          containerElement="label"
          disabled={ (!selectedCountry || uploadProgress) ? true : false }
        >
          <input 
            id="fileInput" 
            style={{ display: 'none'}}
            type="file" 
            disabled={ selectedCountry == null || uploadProgress } 
            onChange={(e) => this.onFileSelected(e)}
          />
        </RaisedButton>
        <RaisedButton 
          secondary
          style={{ display: !valid ? 'none': 'inline-block', marginBottom: spacing.desktopGutterLess }}
          label='Subir Imagen'
          onTouchTap={() => this.onUploadPress()}
        />
      </div>
    )
  }

  render() {
    const { countries, selectedCountry } = this.props.adminCountries;
    return (
      <div className={css(styles.container)}>
        <Paper className={css(styles.leftPane)}>
          <List>
            <Subheader>Lista de Países</Subheader>
            { this.renderCountries() }
          </List>
        </Paper>
        <Paper className={css(styles.rightPane)}>
          <TextField
            floatingLabelText="Nombre del país"
            value={this.state.name}
            onChange={(e) => this.onInputChange('name', e.target.value)}
          /><br />
          <RaisedButton 
            primary 
            style={{ marginBottom: spacing.desktopGutterLess }}
            label={ selectedCountry ? "Actualizar País" : "Agregar País" }
            disabled={ _.isEmpty(this.state.name) }
            onTouchTap={() => this.onSavePress()}
          />
          <br/>
          { this.renderUploadBtn() }
          <RaisedButton 
            label="Cancelar"
            onTouchTap={() => this.onCancelPress()}
          />
        </Paper>
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    display: 'flex',
    marginTop: 72
  },
  leftPane: { 
    flex: 1, 
    margin: spacing.desktopGutterMini,
    overflow: 'auto'
  },
  rightPane: { 
    flex: 1, 
    margin: spacing.desktopGutterMini, 
    padding: spacing.desktopGutterLess,
    overflow: 'auto'
  },
  uploadButton: { 
    marginBottom: spacing.desktopGutterLess,
    marginRight: spacing.desktopGutterLess 
  }
});

function mapStateToProps({ adminCountries }) {
  return { adminCountries };
}
 
export default connect(mapStateToProps, { fetchCountries, selectCountry, createCountry, updateCountry, uploadCountryImage })(AdminPaises);