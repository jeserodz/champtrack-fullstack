import _ from 'lodash';
import validator from 'validator';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as UXActions from '../actions/UXActions';
import * as AdminMessagesActions from '../actions/AdminMessagesActions';
import { Paper, Toolbar, ToolbarGroup, FlatButton, Subheader, ListItem, Avatar, Chip, TextField, Checkbox, SelectField, MenuItem } from 'material-ui';
import { ContentSend } from 'material-ui/svg-icons';
import { StyleSheet, css } from 'aphrodite';
import { colors, spacing } from 'material-ui/styles';
import Pagination from '../components/Pagination';
import placeholder from '../images/avatar-placeholder.png';

class AdminMessages extends Component {

  componentDidMount() {
    this.props.fetchMessagesUsers();
  }

  handleChange(text) {
    this.props.onTextChange(text);
  }

  sendMessage() {
    this.props.sendMessage(
      this.props.auth.user,
      this.props.adminMessages.users,
      this.props.adminMessages.text
    );
  }

  userFilter(user, filterText) {
    let match = false;
    if (!filterText) return true;
    if (validator.contains(user.displayName.toLowerCase(), filterText)) match = true;
    if (validator.contains(user.email.toLowerCase(), filterText)) match = true;
    if (validator.contains(_.get(user, 'country.name', '').toLowerCase(), filterText)) match = true;
    if (validator.contains(_.get(user, 'businessUnit.name', '').toLowerCase(), filterText)) match = true;
    if (validator.contains(_.get(user, 'ranking.name', '').toLowerCase(), filterText)) match = true;
    return match;
  }

  renderUser(user) {
    return (
      <ListItem
        key={user.id}
        leftCheckbox={<Checkbox checked={user.selected} onCheck={() => this.props.toggleUserSelection(user)} />}
        className={user.selected ? css(styles.selected) : null}
        primaryText={user.displayName}
        secondaryText={user.email}
        rightAvatar={<Avatar src={user.photoURL ? baseURL + user.photoURL : placeholder} />}
      />
    )
  }

  renderSelectedUsers() {
    return this.props.adminMessages.users.map(user => {
      if (!user.selected) return null;
      return (
        <Chip key={user.id} className={css(styles.selectedUserChip)}>
          <Avatar src={user.photoURL ? baseURL + user.photoURL : placeholder} />
          {user.displayName}
        </Chip>
      );
    });
  }

  renderOptions(entityName) {
    return this.props.adminMessages[entityName].map(entity => {
      return <MenuItem
        key={entity.id}
        value={entity.id}
        primaryText={entity.name || entity.title}
      />
    });
  }

  render() {
    return (
      <div className={css(styles.container)}>
        <Paper className={css(styles.panelLeft)}>
          <Subheader>Seleccione Destinatarios</Subheader>
          <Pagination
            collection={this.props.adminMessages.users}
            pageSize={5}
            render={this.renderUser.bind(this)}
            filterHintText={'Filtrar usuarios...'}
            filter={this.userFilter}
          />
          <hr />
          <Subheader>Seleccionar Por Grupo</Subheader>
          <div className={css(styles.operationsBlockSelectors)}>
            <SelectField
              floatingLabelText="Seleccionar Ranking"
              hintText=" "
              value={this.props.adminMessages.rankingSelected}
              onChange={(e, x, id) => this.props.selectEntity('ranking', id)}>
              <MenuItem value={false} primaryText="Todos" />
              {this.renderOptions('rankings')}
            </SelectField>
            <SelectField
              floatingLabelText="Seleccionar País"
              hintText=" "
              value={this.props.adminMessages.countrySelected}
              onChange={(e, x, id) => this.props.selectEntity('country', id)}>
              <MenuItem value={false} primaryText="Todos" />
              {this.renderOptions('countries')}
            </SelectField>
            <SelectField
              floatingLabelText="Seleccionar BU"
              hintText=" "
              value={this.props.adminMessages.businessUnitSelected}
              onChange={(e, x, id) => this.props.selectEntity('businessUnit', id)}>
              <MenuItem value={false} primaryText="Todos" />
              {this.renderOptions('businessUnits')}
            </SelectField>
          </div>
          <div className={css(styles.operationsBlock)}>
            <FlatButton
              label="Seleccionar Todos"
              onTouchTap={() => this.props.selectAll(
                this.props.adminMessages.rankingSelected,
                this.props.adminMessages.countrySelected,
                this.props.adminMessages.businessUnitSelected,
              )}
            />
            <FlatButton
              label="Deseleccionar Todos"
              onTouchTap={() => this.props.unselectAll(
                this.props.adminMessages.rankingSelected,
                this.props.adminMessages.countrySelected,
                this.props.adminMessages.businessUnitSelected,
              )}
            />
          </div>
        </Paper>
        <Paper className={css(styles.panelRight)}>
          <Toolbar>
            <ToolbarGroup>
              <FlatButton
                icon={<ContentSend />}
                label="Enviar Mensaje"
                onTouchTap={() => this.sendMessage()}
                disabled={this.props.adminMessages.loading || !this.props.adminMessages.text.length}
              />
            </ToolbarGroup>
          </Toolbar>
          <div className={css(styles.messageEditor)}>
            <TextField
              hintText="Introduzca su mensaje aquí"
              value={this.props.adminMessages.text}
              onChange={e => this.handleChange(e.target.value)}
              disabled={this.props.adminMessages.loading}
              multiLine
              fullWidth
            />
            <br />
            {this.renderSelectedUsers()}
          </div>
        </Paper>
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    marginTop: 64
  },
  panelLeft: {
    flex: 1,
    margin: spacing.desktopGutterLess
  },
  panelRight: {
    flex: 1,
    margin: spacing.desktopGutterLess
  },
  selected: {
    color: 'red',
    backgroundColor: '#ddd'
  },
  messageEditor: {
    flex: 1,
    padding: spacing.desktopGutter
  },
  selectedUserChip: {
    float: 'left'
  },
  operationsBlockSelectors: {
    display: 'flex',
    flexDirection: 'column',
    margin: spacing.desktopGutterMini,
    marginLeft: spacing.desktopGutter,
    marginTop: 0
  },
  operationsBlock: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: spacing.desktopGutterMini,
    marginTop: 0
  }
});

function mapStateToProps({ auth, adminMessages }) {
  return { auth, adminMessages };
}

export default connect(mapStateToProps, { ...UXActions, ...AdminMessagesActions })(AdminMessages);