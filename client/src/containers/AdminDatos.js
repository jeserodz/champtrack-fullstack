import _ from 'lodash';
import validator from 'validator';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, css } from 'aphrodite';
import { 
  fetchRankings, 
  select, 
  generateCsvTemplate, 
  selectCsvFile,
  deselectCsvFile,
  uploadCsvFile
} from '../actions/AdminDatosActions';
import { openRecordDialog } from '../actions/UXActions';
import { Paper, Toolbar, ToolbarGroup, RaisedButton, SelectField, List, ListItem, MenuItem, Avatar, LinearProgress } from 'material-ui';
import { spacing } from 'material-ui/styles';
import { ContentAdd, ContentCreate, FileFileUpload } from 'material-ui/svg-icons';
import Pagination from '../components/Pagination';
import profileFallBack from '../images/avatar-placeholder.png';
import EditRecordDialog from '../components/EditRecordDialog';

class AdminDatos extends Component {

  componentDidMount() {
    this.props.fetchRankings();
  }

  onSelectRecord(id) {
    this.props.select('record', id);
    this.props.openRecordDialog();
  }

  onCsvFileSelected(e) {
    const file = e.target.files[0];
    this.props.selectCsvFile(file);
  }

  onDeselectCsvFile() {
    
  }

  onUploadPress() {
    this.props.uploadCsvFile(this.props.adminDatos.selectedFile);
  }

  recordFilter(record, filterText) {
    const { user } = record;
    let match = false;
    if (!filterText) return true;
    if (validator.contains(user.displayName.toLowerCase(), filterText)) match = true;
    if (validator.contains(user.email.toLowerCase(), filterText)) match = true;
    if (validator.contains(_.get(user, 'country.name', '').toLowerCase(), filterText)) match = true;
    if (validator.contains(_.get(user, 'businessUnit.name', '').toLowerCase(), filterText)) match = true;
    if (validator.contains(_.get(user, 'ranking.name', '').toLowerCase(), filterText)) match = true;
    return match;
  }

  renderRecord(record) {
    return (
      <ListItem
        key={record.id}
        primaryText={record.user.displayName}
        secondaryText={record.user.email + ' - Puntuación Total: ' + parseFloat(record.score).toFixed(2) }
        leftAvatar={<Avatar src={record.user.photoURL ? baseURL + record.user.photoURL : profileFallBack} />}
        onTouchTap={() => this.onSelectRecord(record.id)}
      />
    );
  }

  renderRankingItems() {
    const { rankings } = this.props.adminDatos;
    return _.map(rankings, (ranking, index) => {
      return (
        <MenuItem
          key={index}
          value={ranking.id}
          primaryText={ranking.name}
        />
      );
    });
  }

  renderPeriodItems() {
    const { rankings, selectedRanking } = this.props.adminDatos;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return null;
    return _.map(ranking.periods, (period, index) => {
      return (
        <MenuItem
          key={index}
          value={period.id}
          primaryText={period.name}
        />
      );
    });
  }

  render() {
    const { 
      loading,
      rankings, 
      selectedRanking, 
      selectedPeriod, 
      selectedRecord, 
      records,
      selectedFile, 
      csvButtonLabel,
    } = this.props.adminDatos;
    const ranking = _.find(rankings, { id: selectedRanking });
    const period = ranking ? _.find(ranking.periods, { id: selectedPeriod }) : null;
    const record = period ? _.find(period.records, { id: selectedRecord }) : null;
    return (
      <Paper className={css(styles.containerStyle)} zDepth={1}>
        <Toolbar className={css(styles.toolbarStyle)}>
          <ToolbarGroup>
            <SelectField
              floatingLabelText="Ranking"
              floatingLabelFixed={true}
              value={selectedRanking}
              onChange={(e, i, id) => this.props.select('ranking', id)}
              children={this.renderRankingItems()}
            />
            <SelectField
              floatingLabelText="Período"
              floatingLabelFixed={true}
              value={selectedPeriod}
              onChange={(e, i, id) => this.props.select('period', id)}
              children={this.renderPeriodItems()}
            />
          </ToolbarGroup>
          <ToolbarGroup style={{ minWidth: 750 }}>
            <RaisedButton
              primary
              label="Crear Dato"
              icon={<ContentAdd />}
              disabled={ (selectedRanking && selectedPeriod && !loading) ? false : true }
              onTouchTap={() => this.props.openRecordDialog()}
            />
            <RaisedButton
              secondary
              label="Generar Plantilla CSV"
              icon={<ContentCreate />}
              style={{ display: selectedFile ? 'none' : null }}
              disabled={ (selectedRanking && selectedPeriod && !loading) ? false : true }
              onTouchTap={() => this.props.generateCsvTemplate(selectedRanking, selectedPeriod)}
            />
            <RaisedButton 
              containerElement="label"  
              secondary
              label={csvButtonLabel}
              icon={<FileFileUpload />}
              style={{ maxHeight: 36 }}
              labelStyle={{ overflow: 'hidden' }}
              disabled={ (selectedRanking && selectedPeriod && !loading) ? false : true }
            >
              <input 
                id="fileInput" 
                style={{ display: 'none'}}
                type="file" 
                disabled={ (selectedRanking && selectedPeriod && !loading) ? false : true } 
                onChange={(e) => this.onCsvFileSelected(e)}
              />
            </RaisedButton>
            <RaisedButton
              secondary
              label="Iniciar Cargar"
              icon={<FileFileUpload />}
              style={{ display: !selectedFile ? 'none' : null }}
              disabled={ (selectedRanking && selectedPeriod && !loading) ? false : true }
              onTouchTap={() => this.onUploadPress()}
            />
          </ToolbarGroup>
        </Toolbar>
        <LinearProgress mode="indeterminate" style={{ display: loading ? null : 'none' }} />
        <List>
          <Pagination 
            collection={records}
            pageSize={20}
            render={this.renderRecord.bind(this)}
            filterHintText={'Filtrar datos...'}
            filter={this.recordFilter}
          />
        </List>
        <EditRecordDialog />
      </Paper>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    margin: spacing.desktopGutter,
    overflowY: 'auto',
    marginTop: 72
  },
  toolbarStyle: { flex: 1 }
})

function mapStateToProps({ adminDatos }) {
  return { adminDatos }
}
 
export default connect(mapStateToProps, { fetchRankings, select, generateCsvTemplate, selectCsvFile, deselectCsvFile, uploadCsvFile, openRecordDialog })(AdminDatos);