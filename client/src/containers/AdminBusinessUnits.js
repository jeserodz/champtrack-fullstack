import _ from 'lodash';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, css } from 'aphrodite';
import { fetchBusinessUnits, selectBusinessUnit, createBusinessUnit, updateBusinessUnit } from '../actions/AdminBusinessUnitsActions';
import { Paper, List, ListItem, Subheader, Avatar, RaisedButton, TextField } from 'material-ui';
import { colors, spacing } from 'material-ui/styles';
import { EditorModeEdit } from 'material-ui/svg-icons';
import tempFlag from '../images/flag-CR.png';

class AdminBusinessUnits extends Component {
  
  INITAL_STATE = { 
    id: null,
    name: ''
  }

  state = this.INITAL_STATE

  componentDidMount() {
    this.props.fetchBusinessUnits();
  }

  componentWillReceiveProps(nextProps) {
    const { businessUnits, selectedBusinessUnit } = nextProps.adminBusinessUnits;
    const businessUnit = _.find(businessUnits, { id: selectedBusinessUnit })
    if (businessUnit) {
      this.setState({ ...businessUnit });
    } else {
      this.setState({ ...this.INITAL_STATE });
    }
  }

  onInputChange(field, value) {
    this.setState({ [field]: value });
  }

  onSavePress() {
    const { businessUnits, selectedBusinessUnit } = this.props.adminBusinessUnits;
    const businessUnit = _.find(businessUnits, { id: selectedBusinessUnit })
    if (businessUnit) {
      this.props.updateBusinessUnit({ ...this.state });
    } else {
      this.props.createBusinessUnit({ ...this.state });
    }
  }

  onDeletePress() {
    const { selectedBusinessUnit } = this.props.adminBusinessUnits;
    this.props.deleteBusinessUnit(selectBusinessUnit);
  }

  onCancelPress() {
    this.props.selectBusinessUnit(null);
    this.setState({ ...this.INITAL_STATE })
  }

  renderBusinessUnits() {
    const { businessUnits, selectedBusinessUnit } = this.props.adminBusinessUnits;
    if (!businessUnits.length) return <ListItem primaryText="Aún no tiene Business Units para mostrar"/>
    return _.map(businessUnits, (businessUnit, id) => {
      return (
        <ListItem 
          key={id}
          rightIcon={<EditorModeEdit/>}
          primaryText={businessUnit.name}
          style={{ color: selectedBusinessUnit === businessUnit.id ? colors.red400 : null }}
          onTouchTap={() => this.props.selectBusinessUnit(businessUnit.id)}
        />
      );
    });
  }

  render() {
    const { businessUnits, selectedBusinessUnit } = this.props.adminBusinessUnits;
    return (
      <div className={css(styles.container)}>
        <Paper className={css(styles.leftPane)}>
          <List>
            <Subheader>Lista de Business Units</Subheader>
            { this.renderBusinessUnits() }
          </List>
        </Paper>
        <Paper className={css(styles.rightPane)}>
          <TextField
            floatingLabelText="Nombre del Business Unit"
            value={this.state.name}
            onChange={(e) => this.onInputChange('name', e.target.value)}
          /><br />
          <RaisedButton 
            primary 
            style={{ marginBottom: spacing.desktopGutterLess }}
            label={ selectedBusinessUnit ? "Actualizar Business Unit" : "Agregar Business Unit" }
            disabled={ _.isEmpty(this.state.name) }
            onTouchTap={() => this.onSavePress()}
          /><br />
          <RaisedButton 
            primary 
            style={{ marginBottom: spacing.desktopGutterLess }}
            label={ "Eliminar Business Unit" }
            /* disabled={ !selectedBusinessUnit } */
            disabled={ true }
            onTouchTap={() => this.onDeletePress()}
          />
          <br/>
          <RaisedButton 
            label="Cancelar"
            onTouchTap={() => this.onCancelPress()}
          />
        </Paper>
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    display: 'flex',
    marginTop: 72
  },
  leftPane: { 
    flex: 1, 
    margin: spacing.desktopGutterMini,
    overflow: 'auto'
  },
  rightPane: { 
    flex: 1, 
    margin: spacing.desktopGutterMini, 
    padding: spacing.desktopGutterLess,
    overflow: 'auto'
  },
  uploadButton: { 
    marginBottom: spacing.desktopGutterLess,
    marginRight: spacing.desktopGutterLess 
  }
});

function mapStateToProps({ adminBusinessUnits }) {
  return { adminBusinessUnits };
}
 
export default connect(mapStateToProps, { fetchBusinessUnits, selectBusinessUnit, createBusinessUnit, updateBusinessUnit })(AdminBusinessUnits);