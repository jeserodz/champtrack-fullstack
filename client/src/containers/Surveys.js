import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import * as SurveysActions from '../actions/SurveysActions';
import { Card, CardTitle, CardText, List, Subheader, ListItem, Divider, Checkbox, RaisedButton, Chip, LinearProgress } from 'material-ui';
import { ContentSend, ActionCheckCircle, NavigationCancel, ImageAdjust } from 'material-ui/svg-icons';
import { spacing, colors } from 'material-ui/styles';
import background from '../images/survey-bg.jpg';
var moment = require('moment');
var esLocale = require('moment/locale/es');
moment.locale('es', esLocale);

import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);

class Surveys extends Component {

  componentDidMount() {
    const { auth: { user } } = this.props;
    this.props.fetchSurveys()
    this.props.clearUnreadNotifications(user.id, 'Survey')
  }

  getTimeLeft(survey) {
    const now = moment();
    const endDate = moment(survey.endDate);
    return endDate.from(now);
  }

  isFinished(survey) {
    return moment().isAfter(survey.endDate); 
  }

  isAnswered(survey) {
    const { user } = this.props.auth;
    const myAnswer = _.find(survey.answers, { userId: user.id });
    return (myAnswer) ? true : false;
  }

  renderOptionsList(survey) {
    const { selectedOption } = this.props.surveys;
    return (
      <List style={prefixer({ padding: 0 })}>
        <Divider/>
        <Subheader style={prefixer({ backgroundColor: "rgb(249, 249, 249)" })}>Opciones</Subheader>
        { _.map(survey.options, (option, index) => {
          return (
            <ListItem 
              key={index}
              primaryText={option.title}
              leftCheckbox={<Checkbox 
                onCheck={() => this.props.select('option', option.id)}
                checked={selectedOption === option.id}
              />}
            />
          )
        }) }
      </List>
    )
  }

  renderConfirmButton(survey) {
    const { selectedOption } = this.props.surveys;
    const { user } = this.props.auth;
    const option = _.find(survey.options, { id: selectedOption })
    return (
      <div style={prefixer({ marginTop: spacing.desktopGutterMini, marginLeft: spacing.desktopGutterMini })}>
        <RaisedButton 
          primary 
          icon={<ContentSend/>} 
          label="Confirmar Respuesta" 
          labelPosition="before"
          disabled={selectedOption ? false : true}
          onTouchTap={() => this.props.confirmAnswer(option, user)}
        />
      </div>
    )
  }

  renderSelectedAnswer(survey) {
    const { user } = this.props.auth;
    const myAnswer = _.find(survey.answers, { userId: user.id }) || {};
    return (
      <List style={prefixer({ padding: 0 })}>
        <Divider/>
        <Subheader style={{ backgroundColor: "rgb(249, 249, 249)" }}>Opción Seleccionada</Subheader>
        <ListItem 
          primaryText={ myAnswer.id ? myAnswer.option.title : 'Ninguna opción seleccionada' }
          leftIcon={
            !myAnswer.id ? null :
            survey.hasCorrectOption ?
            myAnswer.option.isCorrect ?
              <ActionCheckCircle color={colors.green500}/>:
              <NavigationCancel color={colors.red500}/>:
            <ImageAdjust />
          }
        />
      </List>
    )
  }

  renderSurveyResults(survey) {
    const totalAnswers = survey.answers.length;
    const results = _.countBy(survey.answers, answer => {
      return answer.surveyOptionId;
    });
    return (
      <List style={{ padding: 0 }}>
        <Divider/>
        <Subheader style={{ backgroundColor: "rgb(249, 249, 249)" }}>Respuestas</Subheader>
        {
          _.map(survey.options, (option, key) => (
            <span key={key}>
              <Divider/>
              <ListItem>
                <Chip style={{ marginBottom: spacing.desktopGutterMini }}>
                  {option.title}: {results[option.id] ? (results[option.id] / totalAnswers * 100) : 0}%
                </Chip>
                <LinearProgress 
                  mode="determinate"
                  value={ results[option.id] ? (results[option.id] / totalAnswers * 100) : 0 }
                />
              </ListItem>
            </span>
          ))
        }
      </List>
    )
  }

  renderCorrectOption(trivia) {
    const correctOptions = _.filter(trivia.options, { isCorrect: true });
    return (
      <List style={{ padding: 0 }}>
        <Divider/>
        <Subheader style={{ backgroundColor: "rgb(249, 249, 249)" }}>Opción Correcta</Subheader>
        {
          _.map(correctOptions, (option, index) => (
              <ListItem key={index}
                primaryText={ option ? option.title : 'Sin opción correcta' }
                leftIcon={option ? <ActionCheckCircle color={colors.green500}/> : null }
              />
          ))
        }
    </List>
    )
  }

  renderSurveyList() {
    const { surveys } = this.props.surveys;
    if (_.isEmpty(surveys)) return null;
    return _.map(surveys, (survey, index) => {
      return (
        <Card key={index} className="animated fadeIn" style={{ margin: spacing.desktopGutterLess }}>
          <CardTitle 
            title={survey.question} 
            subtitle={`${this.isFinished(survey) ? 'Finalizada' : 'Finalización'} ${this.getTimeLeft(survey)}`} 
            actAsExpander={true} 
            showExpandableButton={true}
          />
          <CardText style={{ padding: 0 }} expandable={true} className="animated fadeIn">
            { 
              (this.isAnswered(survey) || this.isFinished(survey)) ? 
              this.renderSelectedAnswer(survey) : 
              this.renderOptionsList(survey) 
            }
            <Divider/>
            { 
              (this.isAnswered(survey) || this.isFinished(survey)) ? 
              survey.hasCorrectOption ? 
                this.renderCorrectOption(survey) :
                this.renderSurveyResults(survey) : 
              this.renderConfirmButton(survey) 
            }
          </CardText>
        </Card>
      )
    });
  }

  render() {
    return (
      <div style={prefixer({ flex: 1, marginTop: 64 })} className="animated fadeIn">
        <div 
          className="animated fadeIn"
          style={prefixer({ 
            width: '100%', 
            height: 250, 
            backgroundColor: 'rgb(113, 30, 30)', 
            backgroundImage: `url("${background}")`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            position: 'relative',
            boxShadow: '0 1px 5px #000'
          })}>
          <span style={prefixer({ 
            color: 'white', 
            position: 'absolute', 
            fontSize: 48, 
            top: 15, 
            left: 15, 
            fontWeight: 100,
            textShadow: '1px 1px 1px #000'
        })}>
            Encuestas
          </span>
        </div>
        <div>
          { this.renderSurveyList() }
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ surveys, auth }) => ({ surveys, auth });

export default connect(mapStateToProps, { ...Actions, ...SurveysActions })(Surveys);