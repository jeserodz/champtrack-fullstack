import _ from 'lodash';
import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions/AdminSurveysActions';
import * as UXActions from '../actions/UXActions';
import { openSurveyDialog, openSurveyResultsDialog } from '../actions/UXActions';
import { 
  Paper, 
  Toolbar, 
  ToolbarGroup, 
  FlatButton, 
  TextField, 
  List, 
  ListItem, 
  Subheader, 
  Avatar, 
  Divider, 
  IconButton, 
  IconMenu, 
  MenuItem  
} from 'material-ui';
import { ContentAdd, CommunicationLiveHelp, NavigationMoreVert } from 'material-ui/svg-icons';
import { spacing, colors } from 'material-ui/styles';
import EditSurveyDialog from '../components/EditSurveyDialog';
import SurveyResultsDialog from '../components/SurveyResultsDialog';

import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);

const styles = {
  containerStyle: prefixer({
    flex: 1,
    margin: spacing.desktopGutter,
    marginTop: 72,
    overflowY: 'auto',
  })
}

class AdminSurveys extends Component {

  state = { filterText: '' }

  componentDidMount() {
    this.props.fetchSurveys();
  }

  onSelectSurvey(id) {
    this.props.select('survey', id);
    this.props.openSurveyDialog();
  }

  onOpenResults(id) {
    this.props.select('survey', id);
    this.props.openSurveyResultsDialog();
  }

  onDeleteItem(id) {
    this.props.hideSurvey(id);
  }

  matchFilter(survey) {
    let match = false;
    if (String(survey.question).toLowerCase().match(this.state.filterText.toLowerCase())) {
      match = true;
    }
    return match;
  }

  renderRightIconMenu(id) { 
    return (
      <IconMenu iconButtonElement={this.renderIconButtonElement()}>
        <MenuItem onTouchTap={() => this.onOpenResults(id)}>Ver Resultados</MenuItem>
        <MenuItem onTouchTap={() => this.onDeleteItem(id)}>Eliminar</MenuItem>
      </IconMenu>
    )
  }

  renderIconButtonElement() {
    return (
      <IconButton
        touch={true}
        tooltip="Ver resultados o eliminar..."
        tooltipPosition="bottom-left"
      >
        <NavigationMoreVert color={colors.grey400} />
      </IconButton>
    )
  };

  renderSurveyList() {
    const { surveys } = this.props.adminSurveys;
    return _.map(surveys, (survey, key) => {
      if (!this.matchFilter(survey)) return null;
      return (
        <span key={key}>
          <ListItem
            primaryText={survey.question}
            secondaryTextLines={2}
            secondaryText={
              <p>
               {'Inicio: ' +  (new Date(survey.startDate)).toLocaleString()}
               {' - Fin: ' + (new Date(survey.endDate)).toLocaleString()} <br />
               {'Estado: ' + (moment(survey.startDate).isAfter(new Date()) ? 'Finalizada' : 'Activa') }
              </p>
            }
            leftAvatar={<Avatar icon={<CommunicationLiveHelp />} />}
            rightIconButton={this.renderRightIconMenu(survey.id)}
            onTouchTap={() => this.onSelectSurvey(survey.id)}
          />
          <Divider />
        </span>
      );
    });
  }

  render() {
    return (
      <Paper style={styles.containerStyle} zDepth={1}>
        <Toolbar style={prefixer({ flex: 1 })}>
          <ToolbarGroup>
            <FlatButton 
              label="Crear Encuesta o Trivia" 
              icon={<ContentAdd />} 
              primary 
              onTouchTap={() => this.props.openSurveyDialog()}
            />
            <TextField 
              name="filterInput" 
              placeholder="Filtrar lista"
              onChange={(e, filterText) => this.setState({ ...this.state, filterText })}
            />
          </ToolbarGroup>
        </Toolbar>
        <List>
          <Subheader>Lista de Encuestas</Subheader>
          <Divider />
          { this.renderSurveyList() }
        </List>
        <EditSurveyDialog />
        <SurveyResultsDialog mode="survey" />
      </Paper>
    );
  }
}

function mapStateToProps({ adminSurveys, ux }) {
  return { adminSurveys, ux };
}

export default connect(mapStateToProps, { ...Actions, ...UXActions })(AdminSurveys);