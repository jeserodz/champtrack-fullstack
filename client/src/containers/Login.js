import api from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userLoggedIn } from '../actions/AuthActions';
import { 
  Card, 
  CardTitle, 
  CardText, 
  CardActions, 
  TextField,
  Divider,
  RaisedButton
} from 'material-ui';
import { spacing } from 'material-ui/styles';
import backgroundImage from '../images/login-bg3.jpg';
import logoImage from '../images/logo.png';
import { StyleSheet, css } from 'aphrodite';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);

const styles = StyleSheet.create({
  loginContainerStyle: prefixer({ 
    flex: 1, 
    display: 'flex', 
    height: '100vh',
    flexDirection: 'column',
    justifyContent: 'center', 
    alignItems: 'center', 
    backgroundImage: `url(${backgroundImage})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center'
  }),
  logo: prefixer({
    height: 150,
    marginBottom: spacing.desktopGutter,
    marginLeft: -20
  }),
  loginCardStyle: prefixer({ 
    maxWidth: 400,
    opacity: 0.9
  }),
  error: prefixer({
    color: 'red',
    marginTop: spacing.desktopGutterLess,
    marginBottom: spacing.desktopGutterLess,
    textAlign: 'center'
  })
})

class Login extends Component {

  state = { email: '', password: '', error: '', loading: false }

  componentDidMount() {
    // auto-login user if session found
    const access_token = window.localStorage.getItem('access_token', access_token);
    const userId = window.localStorage.getItem('userId', userId);
    if (access_token && userId) {
      this.setState({ ...this.state, loading: true });
      api.get(`/users/${userId}`, { 
        params: { 
          access_token, 
          filter: { include: ['ranking', { relation: 'roles', scope: { include: [{ relation: 'rankings'}] } } ] } 
        }})
        .then(res => {
          const user = res.data;
          console.log(user);
          window.localStorage.setItem('user', JSON.stringify(user));
          this.setState({ ...this.state, loading: false, error: '' });
          this.props.userLoggedIn(user);
        })
        .catch(err => {
          console.log(err);
          this.setState({ ...this.state, loading: false, error: '' })
        });
    }
  }

  onInputChange(field, event) {
    this.setState({ ...this.state, [field]: event.target.value });
  }

  onButtonPressed() {
    this.setState({ ...this.state, loading: true });
    const { email, password } = this.state;
    api.post('users/login', { email, password })
      .then(res => {
        console.log(res);
        const access_token = res.data.id;
        const userId = res.data.userId;
        window.localStorage.setItem('access_token', access_token);
        window.localStorage.setItem('userId', userId);
        return { access_token, userId };
      })
      .then(({ access_token, userId }) => {
        return api.get(`/users/${userId}`, { 
          params: { 
            access_token, 
            filter: { include: ['ranking', { relation: 'roles', scope: { include: [{ relation: 'rankings'}] } }] }  
          }
        });
      })
      .then(res => {
        const user = res.data;
        console.log(user);
        window.localStorage.setItem('user', JSON.stringify(user));
        this.setState({ ...this.state, loading: false, error: '' });
        this.props.userLoggedIn(user);
      })
      .catch(err => {
        console.log(err);
        this.setState({ ...this.state, loading: false, error: 'Email o constraseña incorrectos' })
      });
  }

  onKeyPress(e) {
    if (e.charCode === 13) 
      this.onButtonPressed();
  }

  render() {
    return (
      <div id="login" className={css(styles.loginContainerStyle)} onKeyPress={this.onKeyPress.bind(this)}>
        <img className={css(styles.logo)} src={logoImage} />
        <Card className={css(styles.loginCardStyle)}>
          <CardTitle>
            Introduzca email y contraseña para continuar
          </CardTitle>
          <Divider />
          <CardText>
            <TextField
              hintText="ej: johnsmith@email.com"
              floatingLabelText="Email"
              type="email"
              autoComplete="off"
              value={this.state.email}
              fullWidth
              onChange={(event) => this.onInputChange('email', event)}
            /><br />
            <TextField
              floatingLabelText="Contraseña"
              type="password"
              autoComplete="new-password"
              value={this.state.password}
              fullWidth
              onChange={(event) => this.onInputChange('password', event)}
            />
          </CardText>
          <CardActions>
            <RaisedButton 
              primary={true} 
              label="Acceder" 
              onTouchTap={this.onButtonPressed.bind(this)}
              style={{ width: '100%' }}
              disabled={this.state.loading}
            />
            <div className={css(styles.error)}>
              { this.state.error }
            </div>
          </CardActions>
        </Card>
      </div>
    );
  }
}

export default connect(null, { userLoggedIn })(Login);