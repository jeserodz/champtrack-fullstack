import _ from 'lodash';
import React from 'react';
import { Redirect } from 'react-router-dom';

/**
 * Verifies if user is adminnistrator
 * @param {AppUser} user 
 */
export const isAdmin = (user) => {
  if (!user.roles) return false;
  const isAdmin = _.find(user.roles, { name: 'Administrator' });
  return isAdmin ? true : false;
};

export const hasRoles = (user) => {
  return (user.roles.length) ? true : false;
}

/**
 * If user is not administrator, 
 * redirects to main screen
 * @param {AppUser} user 
 */
export const authorizeScreen = (user, history) => {
  if (!isAdmin(user)) history.push("/");
}