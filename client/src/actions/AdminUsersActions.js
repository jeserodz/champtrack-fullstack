import api from '../api';
import axios from 'axios';
import * as UXActions from './UXActions';

export const ADMIN_USERS_FETCH_START = 'ADMIN_USERS_FETCH_START'
export const ADMIN_USERS_FETCH_FAIL = 'ADMIN_USERS_FETCH_FAIL'
export const ADMIN_USERS_FETCH_SUCCESS = 'ADMIN_USERS_FETCH_SUCCESS'
export const ADMIN_USERS_INITIAL_FETCH_START = 'ADMIN_USERS_INITIAL_FETCH_START'
export const ADMIN_USERS_INITIAL_FETCH_FAIL = 'ADMIN_USERS_INITIAL_FETCH_FAIL'
export const ADMIN_USERS_INITIAL_FETCH_SUCCESS = 'ADMIN_USERS_INITIAL_FETCH_SUCCESS'
export const ADMIN_USERS_CREATE_START = 'ADMIN_USERS_CREATE_START'
export const ADMIN_USERS_CREATE_FAIL = 'ADMIN_USERS_CREATE_FAIL'
export const ADMIN_USERS_CREATE_SUCCESS = 'ADMIN_USERS_CREATE_SUCCESS'
export const ADMIN_USERS_UPDATE_START = 'ADMIN_USERS_UPDATE_START'
export const ADMIN_USERS_UPDATE_FAIL = 'ADMIN_USERS_UPDATE_FAIL'
export const ADMIN_USERS_UPDATE_SUCCESS = 'ADMIN_USERS_UPDATE_SUCCESS'
export const ADMIN_USERS_ASSIGN_ROLE_START = 'ADMIN_USERS_ASSIGN_ROLE_START'
export const ADMIN_USERS_ASSIGN_ROLE_FAIL = 'ADMIN_USERS_ASSIGN_ROLE_FAIL'
export const ADMIN_USERS_ASSIGN_ROLE_SUCCESS = 'ADMIN_USERS_ASSIGN_ROLE_SUCCESS'
export const ADMIN_USERS_UPLOAD_IMAGE_PROGRESS = 'ADMIN_USERS_UPLOAD_IMAGE_PROGRESS'
export const ADMIN_USERS_UPLOAD_IMAGE_SUCCESS = 'ADMIN_USERS_UPLOAD_IMAGE_SUCCESS'
export const ADMIN_USERS_SELECT_USER = 'ADMIN_USERS_SELECT_USER'
export const ADMIN_USERS_SELECT_COUNTRY = 'ADMIN_USERS_SELECT_COUNTRY'
export const ADMIN_USERS_SELECT_BU = 'ADMIN_USERS_SELECT_BU'
export const ADMIN_USERS_SELECT_LINE = 'ADMIN_USERS_SELECT_LINE'
export const ADMIN_USERS_SELECT_RANKING = 'ADMIN_USERS_SELECT_RANKING'

const defaultUsersParams = {
  filter: {
    include: ['country', 'businessUnit', 'line', 'ranking', 'roles']
  }
}

export const fetchData = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_USERS_INITIAL_FETCH_START })
    const access_token = window.localStorage.getItem('access_token')
    axios.all([
      api.get('users', { params: { ...defaultUsersParams, access_token }}),
      api.get('countries', { params: { access_token }}),
      api.get('businessunits', { params: { access_token }}),
      api.get('lines', { params: { access_token }}),
      api.get('rankings', { params: { access_token }}),
      api.get('roles', { params: { access_token }})
    ])
      .then(res => {
        const users = res[0].data;
        const countries = res[1].data;
        const businessUnits = res[2].data;
        const lines = res[3].data;
        const rankings = res[4].data;
        const roles = res[5].data;
        dispatch({ type: ADMIN_USERS_INITIAL_FETCH_SUCCESS, payload: { users, countries, businessUnits, lines, rankings, roles }})
      })
      .catch(err => {
        dispatch({ type: ADMIN_USERS_INITIAL_FETCH_FAIL, payload: err })
      })
  }
}

export const select = (entityType, id) => {
  switch (entityType) {
    case 'user': {
      return { type: ADMIN_USERS_SELECT_USER, payload: id }
      break;
    }
    case 'country': {
      return { type: ADMIN_USERS_SELECT_COUNTRY, payload: id }
      break;
    }
    case 'businessUnit': {
      return { type: ADMIN_USERS_SELECT_BU, payload: id }
      break;
    }
    case 'line': {
      return { type: ADMIN_USERS_SELECT_LINE, payload: id }
      break;
    }
    case 'ranking': {
      return { type: ADMIN_USERS_SELECT_RANKING, payload: id }
      break;
    }
  }
  
}

export const createUser = (user) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_USERS_CREATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.post('users', user, { params: { access_token } })
      .then(res => {
        return api.get('users', { params: { access_token }})
      })
      .then(res => {
        const users = res.data;
        return dispatch({ type: ADMIN_USERS_FETCH_SUCCESS, payload: users })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_USERS_CREATE_FAIL, payload: err })
      })
  }
}

export const updateUser = (user) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_USERS_UPDATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.patch(`users/${user.id}`, user, { params: { access_token } })
      .then(res => {
        return api.get('users', { params: { ...defaultUsersParams, access_token }})
      })
      .then(res => {
        const users = res.data;
        dispatch({ type: ADMIN_USERS_FETCH_SUCCESS, payload: users });
        dispatch(UXActions.showSnackbarMessage('Usuario actualizado correctamente.'));
      })
      .catch(err => {
        return dispatch({ type: ADMIN_USERS_UPDATE_FAIL, payload: err })
      })
  }
}

export const resetPassword = (user, newPassword) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_USERS_UPDATE_START })
    user.password = newPassword
    const access_token = window.localStorage.getItem('access_token')
    api.patch(`users/${user.id}`, user, { params: { access_token } })
      .then(res => {
        return api.get('users', { params: { ...defaultUsersParams, access_token }})
      })
      .then(res => {
        const users = res.data;
        return dispatch({ type: ADMIN_USERS_FETCH_SUCCESS, payload: users })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_USERS_UPDATE_FAIL, payload: err })
      })
  }
}

export const uploadUserImage = (id, file) => {
  return function(dispatch) {
    let extension = file.name.split('.');
    extension = extension[extension.length-1];
    const data = new FormData();
    data.append('file', file);
    const access_token = window.localStorage.getItem('access_token')
    api.post(`uploads/profiles/upload`, data, {
      params: { id, access_token },
      onUploadProgress: function(progressEvent) {
        const percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
        console.log(percentCompleted + '% uploaded');
        dispatch({ type: ADMIN_USERS_UPLOAD_IMAGE_PROGRESS, payload: percentCompleted });
      }
    }).then(res => {
      if (!Promise) return res;
      return new Promise((resolve, reject) => {
        window.setTimeout(() => { resolve(res) }, 2000);
      })
    }).then(res => {
      console.log('100% complete');
      dispatch({ type: ADMIN_USERS_UPLOAD_IMAGE_SUCCESS });
      dispatch({ type: ADMIN_USERS_FETCH_START })
      return api.get('users', { params: { ...defaultUsersParams, access_token }})
    }).then(res => {
      return dispatch({ type: ADMIN_USERS_FETCH_SUCCESS, payload: res.data })
    }).catch(err => {
      dispatch({ type: ADMIN_USERS_UPDATE_FAIL, payload: err });
    });
  }
}

export const assignRolesToUser = (user, roles) => {
  return dispatch => {
    dispatch({ type: ADMIN_USERS_ASSIGN_ROLE_START });
    const access_token = window.localStorage.getItem('access_token');
    api.post('users/assignRoles', { user, roles }, { params: { access_token } })
      .then(res => {
        const updatedUser = res.data;
        dispatch({ type: ADMIN_USERS_ASSIGN_ROLE_SUCCESS, payload: updatedUser });
        dispatch(UXActions.closeAssignRolesDialog());
        dispatch(UXActions.showSnackbarMessage('Rol asignado correctamente.'));
      })
      .catch(err => {
        dispatch({ type: ADMIN_USERS_ASSIGN_ROLE_FAIL, payload: err });
      });
  }
}