import { CLOSE_DRAWER } from './UXActions';

export const USER_LOGIN_START = "USER_LOGIN_START";
export const USER_LOGIN_SUCCESS = "USER_LOGIN_SUCCESS";
export const USER_LOGOUT = "USER_LOGOUT";

export const userLoggedIn = (user) => {
  return function(dispatch) {
    dispatch({ type: user ? USER_LOGIN_SUCCESS : USER_LOGOUT, payload: user });
    dispatch({ type: CLOSE_DRAWER });
  }
}

export const userLogout = () => {
  return function(dispatch) {
    dispatch({ type: USER_LOGOUT });
    dispatch({ type: CLOSE_DRAWER });
  }
}