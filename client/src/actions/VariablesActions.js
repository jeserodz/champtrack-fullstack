export const VARIABLES_FETCHED = "VARIABLES_FETCHED"
export const VARIABLE_ADDED = "VARIABLE_ADDED"
export const VARIABLE_CHANGED = "VARIABLE_CHANGED"
export const VARIABLE_REMOVED = "VARIABLE_REMOVED"
export const VARIABLE_SELECTED = "VARIABLE_SELECTED"

let variablesRef = null

export const setupVariables = () => {
  // variablesRef = firebase.database().ref('/variables');
  
  return (dispatch) => {
    variablesRef.once('value', snapshot => {
      dispatch({ type: VARIABLES_FETCHED, payload: snapshot.val() })

      variablesRef.on('child_added', (data) => {
        dispatch({ type: VARIABLE_ADDED, payload: { key: data.key, val: data.val() } })
      });

      variablesRef.on('child_changed', (data) => {
        dispatch({ type: VARIABLE_CHANGED, payload: { key: data.key, val: data.val() } })
      });

      variablesRef.on('child_removed', (data) => {
        dispatch({ type: VARIABLE_REMOVED, payload: { key: data.key } })
      });
    });
  }
}

export const selectVariable = (key) => {
  return { type: VARIABLE_SELECTED, payload: key };
}

export const addVariable = (data) => {
  return function(dispatch) {
    variablesRef.push(data);
  }
}

export const changeVariable = (key, data) => {
  return function(dispatch) {
    variablesRef.child(key).update(data);
  }
}

export const removeVariable = (key) => {
  return function(dispatch) {
    variablesRef.child(key).remove();
  }
}