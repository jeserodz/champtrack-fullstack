import { CLOSE_DRAWER } from '../actions/UXActions';
import { RANKINGS_RESET_SELECTIONS } from './RankingsActions';

export const ROUTE_CHANGE = 'ROUTE_CHANGE';

export function navigateTo(scene) {
  return function(dispatch) {
    window.scrollTo(0,0);
    dispatch({ type: ROUTE_CHANGE, payload: scene });
    dispatch({ type: RANKINGS_RESET_SELECTIONS });
    dispatch({ type: CLOSE_DRAWER });
  }
}