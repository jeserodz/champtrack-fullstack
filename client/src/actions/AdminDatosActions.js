import api from '../api';
import axios from 'axios';
import _ from 'lodash';
import fileSaver from 'file-saver';
import * as UXActions from './UXActions';

export const ADMIN_DATOS_RANKINGS_START = 'ADMIN_DATOS_RANKINGS_START'
export const ADMIN_DATOS_RANKINGS_SUCCESS = 'ADMIN_DATOS_RANKINGS_SUCCESS'
export const ADMIN_DATOS_RANKINGS_FAIL = 'ADMIN_DATOS_RANKINGS_FAIL'
export const ADMIN_DATOS_SELECT_RANKING = 'ADMIN_DATOS_SELECT_RANKING'
export const ADMIN_DATOS_SELECT_PERIOD = 'ADMIN_DATOS_SELECT_PERIOD'
export const ADMIN_DATOS_SELECT_RECORD = 'ADMIN_DATOS_SELECT_RECORD'
export const ADMIN_DATOS_START = 'ADMIN_DATOS_START'
export const ADMIN_DATOS_SUCCESS = 'ADMIN_DATOS_SUCCESS'
export const ADMIN_DATOS_FAIL = 'ADMIN_DATOS_FAIL'
export const ADMIN_DATOS_GENERATE_CSV = 'ADMIN_DATOS_GENERATE_CSV'
export const ADMIN_DATOS_SELECT_CSV = 'ADMIN_DATOS_SELECT_CSV'
export const ADMIN_DATOS_DESELECT_CSV = 'ADMIN_DATOS_DESELECT_CSV'
export const ADMIN_DATOS_UPLOAD_CSV_START = 'ADMIN_DATOS_UPLOAD_CSV_START'
export const ADMIN_DATOS_UPLOAD_CSV_SUCCESS = 'ADMIN_DATOS_UPLOAD_CSV_SUCCESS'
export const ADMIN_DATOS_UPLOAD_CSV_FAIL = 'ADMIN_DATOS_UPLOAD_CSV_FAIL'

const defaultRankingParams = {
  filter: {
    include: ['users', 'periods', 'variables']
  }
}

const defaultRecordsParams = { 
  filter: { include: [
    { relation: 'user' }, 
    { relation: 'data', include: ['variable']}
  ]}
}

export const fetchRankings = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_DATOS_RANKINGS_START })
    const access_token = window.localStorage.getItem('access_token')
    api.get('rankings', { params: { ...defaultRankingParams, access_token }})
      .then(res => {
        const rankings = res.data;
        dispatch({ type: ADMIN_DATOS_RANKINGS_SUCCESS, payload: rankings })
      })
      .catch(err => {
        dispatch({ type: ADMIN_DATOS_RANKINGS_FAIL, payload: err })
      })
  }
}

export const select = (entityType, id) => {
  return (dispatch) => {
    switch (entityType) {
      case 'ranking': {
        dispatch({ type: ADMIN_DATOS_SELECT_RANKING, payload: id })
        break;
      }
      case 'period': {
        dispatch({ type: ADMIN_DATOS_SELECT_PERIOD, payload: id })
        dispatch({ type: ADMIN_DATOS_START })
        const access_token = window.localStorage.getItem('access_token')
        api.get(`periods/${id}/records`, { params: { ...defaultRecordsParams, access_token }})
          .then(res => {
            const records = res.data;
            dispatch({ type: ADMIN_DATOS_SUCCESS, payload: records })
          })
          .catch(err => {
            dispatch({ type: ADMIN_DATOS_FAIL, payload: err })
          })
        break;
      }
      case 'record': {
        dispatch({ type: ADMIN_DATOS_SELECT_RECORD, payload: id })
        break;
      }
    }
  }
}

export const createRecord = (record) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_DATOS_START })
    record.data = _.toArray(record.data)
    const access_token = window.localStorage.getItem('access_token')
    // create new record
    api.post(`records/upsertWithVariables`, { record }, { params: { access_token }})
      .then(res => { // refresh records list
        return api.get(`periods/${record.periodId}/records`, { params: { ...defaultRecordsParams, access_token } })
      })
      .then(res => {
        const records = res.data;
        dispatch({ type: ADMIN_DATOS_SUCCESS, payload: records })
      })
      .catch(err => {
        dispatch({ type: ADMIN_DATOS_FAIL, payload: err })
      })
  }
}

export const updateRecord = (record) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_DATOS_START })
    record.data = _.toArray(record.data)
    const access_token = window.localStorage.getItem('access_token')
    // update record
    api.patch(`records/${record.id}`, record, { params: { access_token }})
      .then(res => {
        // update variable data for the record
        return axios.all(_.forEach(record.data, data => {
          return api.patch(`recorddata/${data.id}`, data, { params: { access_token }})
        }))
      })
      .then(res => {
        // refresh records list
        return api.get(`periods/${record.periodId}/records`, { params: { ...defaultRecordsParams, access_token } })
      })
      .then(res => {
        const records = res.data;
        dispatch({ type: ADMIN_DATOS_SUCCESS, payload: records })
      })
      .catch(err => {
        dispatch({ type: ADMIN_DATOS_FAIL, payload: err })
      })
  }
}

export const generateCsvTemplate = (rankingId, periodId) => {
  if (!rankingId) throw new Error('Ranking ID was not provided');
  if (!periodId) throw new Error('Period ID was not provided');

  return (dispatch) => {
    // api.get('records/generateCsvTemplate', { params: { rankingId, periodId }})
    //   .then(({ data }) => {
    //     let file = new Blob([data], { type: 'text/csv;charset=utf-8' });
    //     fileSaver.saveAs(file, 'datos.csv');
    //     dispatch({ type: ADMIN_DATOS_GENERATE_CSV, payload: data });
    //   });
    const win = window.open();
    api.get('records/generateXlsTemplate', { params: { rankingId, periodId }})
      .then(({ data }) => {
        console.log(data)
        win.location.href = data;
      });
  };
}

export const selectCsvFile = (file) => {
  return {
    type: ADMIN_DATOS_SELECT_CSV,
    payload: file
  };
}

export const deselectCsvFile = () => {
  return {
    type: ADMIN_DATOS_DESELECT_CSV
  };
}

export const uploadCsvFile = (file) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_DATOS_UPLOAD_CSV_START });
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = function(event) {
      const xls = event.target.result;
      const access_token = window.localStorage.getItem('access_token');
      api.post('records/createFromXls', { xls }, { params: { access_token }})
        .then(res => {
          dispatch({ type: ADMIN_DATOS_UPLOAD_CSV_SUCCESS });
          dispatch(UXActions.showSnackbarMessage('Datos cargados correctamente.'));
        })
        .catch(err => {
          dispatch({ type: ADMIN_DATOS_UPLOAD_CSV_FAIL });
          dispatch(UXActions.showSnackbarMessage('Error cargando datos.'));
        });
    }
  }
};