import api from '../api';
import _ from 'lodash';

export const POSTS_FETCH_START = 'POSTS_FETCH_START'
export const POSTS_FETCH_FAIL = 'POSTS_FETCH_FAIL'
export const POSTS_FETCH_SUCCESS = 'POSTS_FETCH_SUCCESS'
export const POSTS_SELECT_POST = 'POSTS_SELECT_POST'
export const POSTS_LIKE_START = 'POSTS_LIKE_START'
export const POSTS_LIKE_FAIL = 'POSTS_LIKE_FAIL'
export const POSTS_LIKE_SUCCESS = 'POSTS_LIKE_SUCCESS'

export const fetchPosts = () => {
  return (dispatch) => {
    dispatch({ type: POSTS_FETCH_START });
    const access_token = window.localStorage.getItem('access_token')
    api.get('posts', { params: { access_token, filter: { include: ['likes'] } }})
      .then(res => {
        let posts = res.data;
        posts = _.sortBy(posts, p => (new Date(p.createdDate)).getTime()).reverse();
        dispatch({ type: POSTS_FETCH_SUCCESS, payload: posts })
      })
      .catch(err => dispatch({ type: POSTS_FETCH_FAIL, payload: err }))
  }
}

export const selectPost = (id) => {
  return { type: POSTS_SELECT_POST, payload: id }
}

export const likePost = (postId, userId) => {
  return (dispatch) => {
    dispatch({ type: POSTS_LIKE_START });
    const access_token = window.localStorage.getItem('access_token');
    api.post(
      'posts/toogleLike', 
      { postId, userId },
      { params: { access_token } }
    )
      .then(res => {
        dispatch({ type: POSTS_LIKE_SUCCESS, payload: res.data });
      })
      .catch(err => {
        dispatch({ type: POSTS_LIKE_FAIL, payload: err });
      });
  }
}