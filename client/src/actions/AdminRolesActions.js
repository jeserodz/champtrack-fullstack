import api from '../api';
import * as UXActions from './UXActions';

export const ADMIN_ROLE_FETCH_START = 'ADMIN_ROLE_FETCH_START'
export const ADMIN_ROLE_FETCH_FAIL = 'ADMIN_ROLE_FETCH_FAIL'
export const ADMIN_ROLE_FETCH_SUCCESS = 'ADMIN_ROLE_FETCH_SUCCESS'
export const ADMIN_ROLE_CREATE_START = 'ADMIN_ROLE_CREATE_START'
export const ADMIN_ROLE_CREATE_FAIL = 'ADMIN_ROLE_CREATE_FAIL'
export const ADMIN_ROLE_CREATE_SUCCESS = 'ADMIN_ROLE_CREATE_SUCCESS'
export const ADMIN_ROLE_UPDATE_START = 'ADMIN_ROLE_UPDATE_START'
export const ADMIN_ROLE_UPDATE_FAIL = 'ADMIN_ROLE_UPDATE_FAIL'
export const ADMIN_ROLE_UPDATE_SUCCESS = 'ADMIN_ROLE_UPDATE_SUCCESS'
export const ADMIN_ROLE_UPLOAD_IMAGE_PROGRESS = 'ADMIN_ROLE_UPLOAD_IMAGE_PROGRESS'
export const ADMIN_ROLE_UPLOAD_IMAGE_SUCCESS = 'ADMIN_ROLE_UPLOAD_IMAGE_SUCCESS'
export const ADMIN_ROLE_SELECT_BU = 'ADMIN_ROLE_SELECT_BU'
export const ADMIN_ROLE_FETCH_RANKINGS = 'ADMIN_ROLE_FETCH_RANKINGS'

export const fetchRoles = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_ROLE_FETCH_START })
    api.get('roles', { params: { filter: { include: ['rankings'] } } })
      .then(res => {
        const roles = res.data;
        dispatch({ type: ADMIN_ROLE_FETCH_SUCCESS, payload: roles })
      })
      .catch(err => {
        dispatch({ type: ADMIN_ROLE_FETCH_FAIL, payload: err })
      })
  }
}

export const selectRole = (id) => {
  return { type: ADMIN_ROLE_SELECT_BU, payload: id }
}

export const createRole = (role) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_ROLE_CREATE_START })
    api.post('roles', role)
      .then(res => api.get('roles'))
      .then(res => {
        const roles = res.data;
        return dispatch({ type: ADMIN_ROLE_FETCH_SUCCESS, payload: roles })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_ROLE_CREATE_FAIL, payload: err })
      })
  }
}

export const updateRole = (role) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_ROLE_UPDATE_START })
    api.patch(`roles/${role.id}`, role)
      .then(res => {
        return api.get('roles')
      })
      .then(res => {
        const roles = res.data;
        return dispatch({ type: ADMIN_ROLE_FETCH_SUCCESS, payload: roles })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_ROLE_UPDATE_FAIL, payload: err })
      })
  }
}

export const fetchRankings = () => {
  return async dispatch => {
    try {
      const response = await api.get(`rankings`);
      const rankings = response.data;
      return dispatch({ type: ADMIN_ROLE_FETCH_RANKINGS, payload: rankings });
    } catch (error) {
      throw (error);
    }
  }
}

export const addRankingToRole = (roleId, rankingId) => {
  return async dispatch => {
    try {
      await api.put(`/AppRoleRankings`, { roleId, rankingId });
      dispatch(fetchRoles());
      dispatch(UXActions.showSnackbarMessage('Ranking agregado al rol'));
    } catch (error) {
      throw(error);
    }
  }
}

export const removeRankingFromRole = (roleId, rankingId) => {
  return async dispatch => {
    try {
      const { data: roleRanking } = await api.get(`/AppRoleRankings/findOne`, { params: { filter: { where: { roleId, rankingId }}}});
      await api.delete(`/AppRoleRankings/${roleRanking.id}`);
      dispatch(fetchRoles());
      dispatch(UXActions.showSnackbarMessage('Ranking eliminado del rol'));
    } catch (error) {
      throw(error);
    }
  }
}
