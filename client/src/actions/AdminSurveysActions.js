import api from '../api';
import axios from 'axios';
import _ from 'lodash';
import * as UXActions from './UXActions';

export const ADMIN_SURVEYS_FETCH_START = 'ADMIN_SURVEYS_FETCH_START'
export const ADMIN_SURVEYS_FETCH_FAIL = 'ADMIN_SURVEYS_FETCH_FAIL'
export const ADMIN_SURVEYS_FETCH_SUCCESS = 'ADMIN_SURVEYS_FETCH_SUCCESS'
export const ADMIN_SURVEYS_CREATE_START = 'ADMIN_SURVEYS_CREATE_START'
export const ADMIN_SURVEYS_CREATE_FAIL = 'ADMIN_SURVEYS_CREATE_FAIL'
export const ADMIN_SURVEYS_CREATE_SUCCESS = 'ADMIN_SURVEYS_CREATE_SUCCESS'
export const ADMIN_SURVEYS_UPDATE_START = 'ADMIN_SURVEYS_UPDATE_START'
export const ADMIN_SURVEYS_UPDATE_FAIL = 'ADMIN_SURVEYS_UPDATE_FAIL'
export const ADMIN_SURVEYS_UPDATE_SUCCESS = 'ADMIN_SURVEYS_UPDATE_SUCCESS'
export const ADMIN_SURVEYS_HIDE_START = 'ADMIN_SURVEYS_HIDE_START'
export const ADMIN_SURVEYS_HIDE_FAIL = 'ADMIN_SURVEYS_HIDE_FAIL'
export const ADMIN_SURVEYS_HIDE_SUCCESS = 'ADMIN_SURVEYS_HIDE_SUCCESS'
export const ADMIN_SURVEYS_SELECT_SURVEY = 'ADMIN_SURVEYS_SELECT_SURVEY'

const defaultSurveysParams = {
  filter: {
    where: { active: true },
    include: [
      { relation: 'options' },
      { relation: 'answers', scope: { include: ['user', 'option']} }
    ]
  }
}

export const fetchSurveys = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_SURVEYS_FETCH_START })
    const access_token = window.localStorage.getItem('access_token');
    api.get('surveys', { params: { ...defaultSurveysParams, access_token }})
      .then(res => {
        const surveys = res.data;
        return dispatch({ type: ADMIN_SURVEYS_FETCH_SUCCESS, payload: surveys })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_SURVEYS_FETCH_FAIL, payload: err })
      })
  }
}

export const select = (entityType, id) => {
  switch (entityType) {
    case 'survey': {
      return { type: ADMIN_SURVEYS_SELECT_SURVEY, payload: id }
      break;
    }
  }
}

export const createSurvey = (survey) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_SURVEYS_CREATE_START })
    survey.options = _.toArray(survey.options)
    const access_token = window.localStorage.getItem('access_token')
    // create new survey
    api.post('surveys', survey, { params: { access_token }})
      .then(res => {
        // create options for the survey
        return api.post(`surveys/${res.data.id}/options`, survey.options, { params: { access_token }})
      })
      .then(res => {
        // refresh surveys list
        dispatch({ type: ADMIN_SURVEYS_FETCH_START })
        return api.get(`surveys`, { params: { ...defaultSurveysParams, access_token } })
      })
      .then(res => {
        const surveys = res.data;
        dispatch({ type: ADMIN_SURVEYS_FETCH_SUCCESS, payload: surveys })
      })
      .catch(err => {
        dispatch({ type: ADMIN_SURVEYS_CREATE_FAIL, payload: err })
      })
  }
}

export const updateSurvey = (survey) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_SURVEYS_CREATE_START })
    survey.options = _.toArray(survey.options)
    const access_token = window.localStorage.getItem('access_token')
    // update survey
    api.patch(`surveys/${survey.id}`, survey, { params: { access_token }})
      .then(res => {
        // update options for the survey
        return axios.all(_.forEach(survey.options, option => {
          return api.patch(`surveyoptions/${option.id}`, option, { params: { access_token }})
        }))
      })
      .then(res => {
        // refresh surveys list
        dispatch({ type: ADMIN_SURVEYS_FETCH_START })
        return api.get(`surveys`, { params: { ...defaultSurveysParams, access_token } })
      })
      .then(res => {
        const surveys = res.data;
        dispatch({ type: ADMIN_SURVEYS_FETCH_SUCCESS, payload: surveys })
      })
      .catch(err => {
        dispatch({ type: ADMIN_SURVEYS_CREATE_FAIL, payload: err })
      })
  }
}

export const hideSurvey = (surveyId) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_SURVEYS_HIDE_START });
    const access_token = window.sessionStorage.getItem('access_token');
    api.post('surveys/hideSurvey', { surveyId }, { params: { access_token } })
      .then(res => {
        dispatch({ type: ADMIN_SURVEYS_HIDE_SUCCESS, payload: surveyId });
        dispatch(UXActions.showSnackbarMessage('Operacion realizada correctamente.'));
      })
      .catch(err => {
        dispatch({ type: ADMIN_SURVEYS_HIDE_FAIL, payload: err });
        dispatch(UXActions.showSnackbarMessage('Ha ocurrido un error al realizar esta acción.'));
      });
  }
}