export const OPEN_DRAWER = "OPEN_DRAWER";
export const CLOSE_DRAWER = "CLOSE_DRAWER";
export const OPEN_VARIABLE_DIALOG = "OPEN_VARIABLE_DIALOG";
export const CLOSE_VARIABLE_DIALOG = "CLOSE_VARIABLE_DIALOG";
export const OPEN_PERIOD_DIALOG = "OPEN_PERIOD_DIALOG";
export const CLOSE_PERIOD_DIALOG = "CLOSE_PERIOD_DIALOG";
export const OPEN_RECORD_DIALOG = "OPEN_RECORD_DIALOG";
export const CLOSE_RECORD_DIALOG = "CLOSE_RECORD_DIALOG";
export const OPEN_TRIVIA_DIALOG = "OPEN_TRIVIA_DIALOG";
export const CLOSE_TRIVIA_DIALOG = "CLOSE_TRIVIA_DIALOG";
export const OPEN_SURVEY_DIALOG = "OPEN_SURVEY_DIALOG";
export const CLOSE_SURVEY_DIALOG = "CLOSE_SURVEY_DIALOG";
export const OPEN_TRIVIA_RESULTS_DIALOG = "OPEN_TRIVIA_RESULTS_DIALOG";
export const CLOSE_TRIVIA_RESULTS_DIALOG = "CLOSE_TRIVIA_RESULTS_DIALOG";
export const OPEN_SELECT_RANKING_PERIOD_DIALOG = "OPEN_SELECT_RANKING_PERIOD_DIALOG";
export const CLOSE_SELECT_RANKING_PERIOD_DIALOG = "CLOSE_SELECT_RANKING_PERIOD_DIALOG";
export const OPEN_SELECT_RANKING_DIALOG = "OPEN_SELECT_RANKING_DIALOG";
export const CLOSE_SELECT_RANKING_DIALOG = "CLOSE_SELECT_RANKING_DIALOG";
export const OPEN_SELECT_BU_DIALOG = "OPEN_SELECT_BU_DIALOG";
export const CLOSE_SELECT_BU_DIALOG = "CLOSE_SELECT_BU_DIALOG";
export const OPEN_SELECT_COUNTRY_DIALOG = "OPEN_SELECT_COUNTRY_DIALOG";
export const CLOSE_SELECT_COUNTRY_DIALOG = "CLOSE_SELECT_COUNTRY_DIALOG";
export const OPEN_CHANGE_PASSWORD_DIALOG = "OPEN_CHANGE_PASSWORD_DIALOG";
export const CLOSE_CHANGE_PASSWORD_DIALOG = "CLOSE_CHANGE_PASSWORD_DIALOG";
export const OPEN_DOWNLOAD_EXCEL_DIALOG = "OPEN_DOWNLOAD_EXCEL_DIALOG";
export const CLOSE_DOWNLOAD_EXCEL_DIALOG = "CLOSE_DOWNLOAD_EXCEL_DIALOG";
export const OPEN_ASSIGN_ROLES_DIALOG = "OPEN_ASSIGN_ROLES_DIALOG";
export const CLOSE_ASSIGN_ROLES_DIALOG = "CLOSE_ASSIGN_ROLES_DIALOG";
export const SELECT_RANKING_PERIOD_RECORD = "SELECT_RANKING_PERIOD_RECORD";
export const SHOW_SNACKBAR = "SHOW_SNACKBAR";
export const HIDE_SNACKBAR = "HIDE_SNACKBAR";

export const openDrawer = () => {
  return { type: OPEN_DRAWER };
};

export const closeDrawer = () => {
  return { type: CLOSE_DRAWER };
};

export const openVariableDialog = () => {
  return { type: OPEN_VARIABLE_DIALOG };
};

export const closeVariableDialog = () => {
  return { type: CLOSE_VARIABLE_DIALOG };
};

export const openPeriodDialog = () => {
  return { type: OPEN_PERIOD_DIALOG };
};

export const closePeriodDialog = () => {
  return { type: CLOSE_PERIOD_DIALOG };
};

export const openRecordDialog = () => {
  return { type: OPEN_RECORD_DIALOG };
};

export const closeRecordDialog = () => {
  return { type: CLOSE_RECORD_DIALOG };
};

export const openTriviaDialog = () => {
  return { type: OPEN_TRIVIA_DIALOG };
};

export const closeTriviaDialog = () => {
  return { type: CLOSE_TRIVIA_DIALOG };
};

export const openSurveyDialog = () => {
  return { type: OPEN_SURVEY_DIALOG };
};

export const closeSurveyDialog = () => {
  return { type: CLOSE_SURVEY_DIALOG };
};

export const openSurveyResultsDialog = () => {
  return { type: OPEN_TRIVIA_RESULTS_DIALOG };
};

export const closeSurveyResultsDialog = () => {
  return { type: CLOSE_TRIVIA_RESULTS_DIALOG };
};

export const openSelectRankingPeriodDialog = () => {
  return { type: OPEN_SELECT_RANKING_PERIOD_DIALOG };
};

export const closeSelectRankingPeriodDialog = () => {
  return { type: CLOSE_SELECT_RANKING_PERIOD_DIALOG };
};

export const openSelectRankingDialog = () => {
  return { type: OPEN_SELECT_RANKING_DIALOG };
};

export const closeSelectRankingDialog = () => {
  return { type: CLOSE_SELECT_RANKING_DIALOG };
};

export const openSelectBUDialog = () => {
  return { type: OPEN_SELECT_BU_DIALOG };
};

export const closeSelectBUDialog = () => {
  return { type: CLOSE_SELECT_BU_DIALOG };
};

export const openSelectCountryDialog = () => {
  return { type: OPEN_SELECT_COUNTRY_DIALOG };
};

export const closeSelectCountryDialog = () => {
  return { type: CLOSE_SELECT_COUNTRY_DIALOG };
};

export const selectRankingPeriodRecord = key => {
  return { type: SELECT_RANKING_PERIOD_RECORD, payload: key };
};

export const openChangePasswordDialog = () => {
  return { type: OPEN_CHANGE_PASSWORD_DIALOG };
};

export const closeChangePasswordDialog = () => {
  return { type: CLOSE_CHANGE_PASSWORD_DIALOG };
};

export const openDownloadExcelDialog = downloadExcelURL => {
  return { type: OPEN_DOWNLOAD_EXCEL_DIALOG, payload: downloadExcelURL };
};

export const closeDownloadExcelDialog = () => {
  return { type: CLOSE_DOWNLOAD_EXCEL_DIALOG };
};

export const openAssignRolesDialog = () => {
  return { type: OPEN_ASSIGN_ROLES_DIALOG };
};

export const closeAssignRolesDialog = () => {
  return { type: CLOSE_ASSIGN_ROLES_DIALOG };
};

export const showSnackbarMessage = message => {
  return dispatch => {
    dispatch({ type: SHOW_SNACKBAR, payload: message });
    setTimeout(() => {
      dispatch({ type: HIDE_SNACKBAR });
    }, 4000);
  };
};
