import api from '../api';

export const ADMIN_COUNTRIES_FETCH_START = 'ADMIN_COUNTRIES_FETCH_START'
export const ADMIN_COUNTRIES_FETCH_FAIL = 'ADMIN_COUNTRIES_FETCH_FAIL'
export const ADMIN_COUNTRIES_FETCH_SUCCESS = 'ADMIN_COUNTRIES_FETCH_SUCCESS'
export const ADMIN_COUNTRIES_CREATE_START = 'ADMIN_COUNTRIES_CREATE_START'
export const ADMIN_COUNTRIES_CREATE_FAIL = 'ADMIN_COUNTRIES_CREATE_FAIL'
export const ADMIN_COUNTRIES_CREATE_SUCCESS = 'ADMIN_COUNTRIES_CREATE_SUCCESS'
export const ADMIN_COUNTRIES_UPDATE_START = 'ADMIN_COUNTRIES_UPDATE_START'
export const ADMIN_COUNTRIES_UPDATE_FAIL = 'ADMIN_COUNTRIES_UPDATE_FAIL'
export const ADMIN_COUNTRIES_UPDATE_SUCCESS = 'ADMIN_COUNTRIES_UPDATE_SUCCESS'
export const ADMIN_COUNTRIES_UPLOAD_IMAGE_PROGRESS = 'ADMIN_COUNTRIES_UPLOAD_IMAGE_PROGRESS'
export const ADMIN_COUNTRIES_UPLOAD_IMAGE_SUCCESS = 'ADMIN_COUNTRIES_UPLOAD_IMAGE_SUCCESS'
export const ADMIN_COUNTRIES_SELECT_COUNTRY = 'ADMIN_COUNTRIES_SELECT_COUNTRY'

export const fetchCountries = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_COUNTRIES_FETCH_START })
    const access_token = window.localStorage.getItem('access_token')
    api.get('countries', { params: { access_token }})
      .then(res => {
        const countries = res.data;
        dispatch({ type: ADMIN_COUNTRIES_FETCH_SUCCESS, payload: countries })
      })
      .catch(err => {
        dispatch({ type: ADMIN_COUNTRIES_FETCH_FAIL, payload: err })
      })
  }
}

export const selectCountry = (id) => {
  return { type: ADMIN_COUNTRIES_SELECT_COUNTRY, payload: id }
}

export const createCountry = (country) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_COUNTRIES_CREATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.post('countries', country, { params: { access_token } })
      .then(res => {
        return api.get('countries', { params: { access_token }})
      })
      .then(res => {
        const countries = res.data;
        return dispatch({ type: ADMIN_COUNTRIES_FETCH_SUCCESS, payload: countries })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_COUNTRIES_CREATE_FAIL, payload: err })
      })
  }
}

export const updateCountry = (country) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_COUNTRIES_UPDATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.patch(`countries/${country.id}`, country, { params: { access_token } })
      .then(res => {
        return api.get('countries', { params: { access_token }})
      })
      .then(res => {
        const countries = res.data;
        return dispatch({ type: ADMIN_COUNTRIES_FETCH_SUCCESS, payload: countries })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_COUNTRIES_UPDATE_FAIL, payload: err })
      })
  }
}

export const uploadCountryImage = (id, file) => {
  return function(dispatch) {
    let extension = file.name.split('.');
    extension = extension[extension.length-1];
    const data = new FormData();
    data.append('file', file);
    const access_token = window.localStorage.getItem('access_token')
    api.post(`uploads/countries/upload`, data, {
      params: { id, access_token },
      onUploadProgress: function(progressEvent) {
        const percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
        console.log(percentCompleted + '% uploaded');
        dispatch({ type: ADMIN_COUNTRIES_UPLOAD_IMAGE_PROGRESS, payload: percentCompleted });
      }
    }).then(res => {
      console.log('100% complete');
      dispatch({ type: ADMIN_COUNTRIES_UPLOAD_IMAGE_SUCCESS });
      dispatch({ type: ADMIN_COUNTRIES_FETCH_START })
      return api.get('countries', { params: { access_token } })
    }).then(res => {
      return dispatch({ type: ADMIN_COUNTRIES_FETCH_SUCCESS, payload: res.data })
    }).catch(err => {
      dispatch({ type: ADMIN_COUNTRIES_UPDATE_FAIL, payload: err });
    });
  }
}