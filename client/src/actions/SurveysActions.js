import api from '../api';
import _ from 'lodash';
import moment from 'moment';

export const SURVEYS_FETCH_START = 'SURVEYS_FETCH_START'
export const SURVEYS_FETCH_FAIL = 'SURVEYS_FETCH_FAIL'
export const SURVEYS_FETCH_SUCCESS = 'SURVEYS_FETCH_SUCCESS'
export const SURVEYS_SELECT_SURVEY = 'SURVEYS_SELECT_SURVEY'
export const SURVEYS_SELECT_OPTION = 'SURVEYS_SELECT_OPTION'
export const SURVEYS_CONFIRM_ANSWER = 'SURVEYS_CONFIRM_ANSWER'

const defaultSurveysParams = {
  filter: {
    where: { active: true },
    order: 'endDate DESC',
    include: [
      { relation: 'options' },
      { 
        relation: 'answers', 
        scope: { include: ['option'] } 
      },
    ]
  }
}

export const fetchSurveys = () => {
  return (dispatch) => {
    dispatch({ type: SURVEYS_FETCH_START })
    const access_token = window.localStorage.getItem('access_token')
    api.get('surveys', { params: { ...defaultSurveysParams, access_token }})
      .then(res => {
        const surveys = res.data;
        dispatch({ type: SURVEYS_FETCH_SUCCESS, payload: surveys })
      })
      .catch(err => {
        dispatch({ type: SURVEYS_FETCH_FAIL, payload: err })
      })
  }
}

export const select = (entityType, id) => {
  switch (entityType) {
    case 'survey': {
      return { type: SURVEYS_SELECT_SURVEY, payload: id }
      break
    }
    case 'option': {
      return { type: SURVEYS_SELECT_OPTION, payload: id }
      break
    }
  }
}

export const confirmAnswer = (option, user) => {
  return (dispatch) => {
    dispatch({ type: SURVEYS_CONFIRM_ANSWER })
    const access_token = window.localStorage.getItem('access_token')
    const answer = { 
      surveyId: option.surveyId,
      surveyOptionId: option.id, 
      userId: user.id, 
      createdDate: new Date() 
    }
    api.post(`surveyanswers`, answer, { params: { access_token }})
      .then(res => {
        dispatch({ type: SURVEYS_FETCH_START })
        return api.get('surveys', { params: { ...defaultSurveysParams, access_token }})
      })
      .then(res => {
        const surveys = res.data;
        dispatch({ type: SURVEYS_FETCH_SUCCESS, payload: surveys })
      })
      .catch(err => {
        dispatch({ type: SURVEYS_FETCH_FAIL, payload: err })
      })
  }
}