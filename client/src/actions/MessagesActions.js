import api from '../api';
import * as uxActions from './UXActions';

export const MESSAGES_FETCH_START = 'MESSAGES_FETCH_START';
export const MESSAGES_SUCCESS = 'MESSAGES_SUCCESS';
export const MESSAGES_FAIL = 'MESSAGES_FAIL';
export const MESSAGES_HIDE_START = 'MESSAGES_HIDE_START';
export const MESSAGES_HIDE_SUCCESS = 'MESSAGES_HIDE_SUCCESS';

export const fetchMessages = (userId) => {
  return (dispatch) => {
    dispatch({ type: MESSAGES_FETCH_START });
    const access_token = window.localStorage.getItem('access_token');
    api.get('messages/userMessages', { params: { access_token, userId } })
      .then(res => {
        dispatch({ type: MESSAGES_SUCCESS, payload: res.data });
      })
      .catch(err => {
        dispatch({ type: MESSAGES_FAIL, payload: err });
      });
  }
}

export const hideMessage = (messageId) => {
  return (dispatch) => {
    dispatch({ type: MESSAGES_HIDE_START });
    const access_token = window.localStorage.getItem('access_token');
    api.post('messages/hideMessage', { messageId }, { params: { access_token } })
      .then(res => {
        dispatch({ type: MESSAGES_HIDE_SUCCESS, payload: messageId });
        dispatch(uxActions.showSnackbarMessage('Mensage ocultado.'));
      })
      .catch(err => {
        dispatch({ type: MESSAGES_FAIL, payload: err });
      });
  }
}