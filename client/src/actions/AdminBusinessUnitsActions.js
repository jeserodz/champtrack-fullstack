import api from '../api';

export const ADMIN_BU_FETCH_START = 'ADMIN_BU_FETCH_START'
export const ADMIN_BU_FETCH_FAIL = 'ADMIN_BU_FETCH_FAIL'
export const ADMIN_BU_FETCH_SUCCESS = 'ADMIN_BU_FETCH_SUCCESS'
export const ADMIN_BU_CREATE_START = 'ADMIN_BU_CREATE_START'
export const ADMIN_BU_CREATE_FAIL = 'ADMIN_BU_CREATE_FAIL'
export const ADMIN_BU_CREATE_SUCCESS = 'ADMIN_BU_CREATE_SUCCESS'
export const ADMIN_BU_DELETE_START = 'ADMIN_BU_DELETE_START'
export const ADMIN_BU_DELETE_FAIL = 'ADMIN_BU_DELETE_FAIL'
export const ADMIN_BU_DELETE_SUCCESS = 'ADMIN_BU_DELETE_SUCCESS'
export const ADMIN_BU_UPDATE_START = 'ADMIN_BU_UPDATE_START'
export const ADMIN_BU_UPDATE_FAIL = 'ADMIN_BU_UPDATE_FAIL'
export const ADMIN_BU_UPDATE_SUCCESS = 'ADMIN_BU_UPDATE_SUCCESS'
export const ADMIN_BU_UPLOAD_IMAGE_PROGRESS = 'ADMIN_BU_UPLOAD_IMAGE_PROGRESS'
export const ADMIN_BU_UPLOAD_IMAGE_SUCCESS = 'ADMIN_BU_UPLOAD_IMAGE_SUCCESS'
export const ADMIN_BU_SELECT_BU = 'ADMIN_BU_SELECT_BU'

export const fetchBusinessUnits = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_BU_FETCH_START })
    const access_token = window.localStorage.getItem('access_token')
    api.get('businessunits', { params: { access_token }})
      .then(res => {
        const businessUnits = res.data;
        dispatch({ type: ADMIN_BU_FETCH_SUCCESS, payload: businessUnits })
      })
      .catch(err => {
        dispatch({ type: ADMIN_BU_FETCH_FAIL, payload: err })
      })
  }
}

export const selectBusinessUnit = (id) => {
  return { type: ADMIN_BU_SELECT_BU, payload: id }
}

export const createBusinessUnit = (businessUnit) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_BU_CREATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.post('businessunits', businessUnit, { params: { access_token } })
      .then(res => {
        return api.get('businessunits', { params: { access_token }})
      })
      .then(res => {
        const businessUnits = res.data;
        return dispatch({ type: ADMIN_BU_FETCH_SUCCESS, payload: businessUnits })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_BU_CREATE_FAIL, payload: err })
      })
  }
}

export const updateBusinessUnit = (businessUnit) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_BU_UPDATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.patch(`businessUnits/${businessUnit.id}`, businessUnit, { params: { access_token } })
      .then(res => {
        return api.get('businessunits', { params: { access_token }})
      })
      .then(res => {
        const businessUnits = res.data;
        return dispatch({ type: ADMIN_BU_FETCH_SUCCESS, payload: businessUnits })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_BU_UPDATE_FAIL, payload: err })
      })
  }
}

export const deleteBusinessUnit = (id) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_BU_DELETE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.delete('businessunits', { params: { id, access_token } })
      .then(res => {
        return api.get('businessunits', { params: { access_token }})
      })
      .then(res => {
        const businessUnits = res.data;
        return dispatch({ type: ADMIN_BU_FETCH_SUCCESS, payload: businessUnits })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_BU_DELETE_FAIL, payload: err })
      })
  }
}