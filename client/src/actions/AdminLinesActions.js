import api from '../api';

export const ADMIN_LINE_FETCH_START = 'ADMIN_LINE_FETCH_START'
export const ADMIN_LINE_FETCH_FAIL = 'ADMIN_LINE_FETCH_FAIL'
export const ADMIN_LINE_FETCH_SUCCESS = 'ADMIN_LINE_FETCH_SUCCESS'
export const ADMIN_LINE_CREATE_START = 'ADMIN_LINE_CREATE_START'
export const ADMIN_LINE_CREATE_FAIL = 'ADMIN_LINE_CREATE_FAIL'
export const ADMIN_LINE_CREATE_SUCCESS = 'ADMIN_LINE_CREATE_SUCCESS'
export const ADMIN_LINE_UPDATE_START = 'ADMIN_LINE_UPDATE_START'
export const ADMIN_LINE_UPDATE_FAIL = 'ADMIN_LINE_UPDATE_FAIL'
export const ADMIN_LINE_UPDATE_SUCCESS = 'ADMIN_LINE_UPDATE_SUCCESS'
export const ADMIN_LINE_UPLOAD_IMAGE_PROGRESS = 'ADMIN_LINE_UPLOAD_IMAGE_PROGRESS'
export const ADMIN_LINE_UPLOAD_IMAGE_SUCCESS = 'ADMIN_LINE_UPLOAD_IMAGE_SUCCESS'
export const ADMIN_LINE_SELECT_BU = 'ADMIN_LINE_SELECT_BU'

export const fetchLines = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_LINE_FETCH_START })
    const access_token = window.localStorage.getItem('access_token')
    api.get('lines', { params: { access_token }})
      .then(res => {
        const lines = res.data;
        dispatch({ type: ADMIN_LINE_FETCH_SUCCESS, payload: lines })
      })
      .catch(err => {
        dispatch({ type: ADMIN_LINE_FETCH_FAIL, payload: err })
      })
  }
}

export const selectLine = (id) => {
  return { type: ADMIN_LINE_SELECT_BU, payload: id }
}

export const createLine = (line) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_LINE_CREATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.post('lines', line, { params: { access_token } })
      .then(res => {
        return api.get('lines', { params: { access_token }})
      })
      .then(res => {
        const lines = res.data;
        return dispatch({ type: ADMIN_LINE_FETCH_SUCCESS, payload: lines })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_LINE_CREATE_FAIL, payload: err })
      })
  }
}

export const updateLine = (line) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_LINE_UPDATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.patch(`lines/${line.id}`, line, { params: { access_token } })
      .then(res => {
        return api.get('lines', { params: { access_token }})
      })
      .then(res => {
        const lines = res.data;
        return dispatch({ type: ADMIN_LINE_FETCH_SUCCESS, payload: lines })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_LINE_UPDATE_FAIL, payload: err })
      })
  }
}