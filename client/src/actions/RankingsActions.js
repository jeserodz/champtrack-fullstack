import axios from 'axios';
import api from '../api';
import _ from 'lodash';
import fileSaver from 'file-saver';

export const RANKINGS_FETCH_START = 'RANKINGS_FETCH_START'
export const RANKINGS_FETCH_FAIL = 'RANKINGS_FETCH_FAIL'
export const RANKINGS_FETCH_SUCCESS = 'RANKINGS_FETCH_SUCCESS'
export const RANKINGS_SELECT_RANKING = 'RANKINGS_SELECT_RANKING'
export const RANKINGS_SELECT_PERIOD = 'RANKINGS_SELECT_PERIOD'
export const RANKINGS_SELECT_BU = 'RANKINGS_SELECT_BU'
export const RANKINGS_SELECT_COUNTRY = 'RANKINGS_SELECT_COUNTRY'
export const RANKINGS_UNSELECT_BU = 'RANKINGS_UNSELECT_BU'
export const RANKINGS_UNSELECT_COUNTRY = 'RANKINGS_UNSELECT_COUNTRY'
export const RANKINGS_FETCH_ACCUMULATED_RECORDS_START = 'RANKINGS_FETCH_ACCUMULATED_RECORDS_START'
export const RANKINGS_FETCH_ACCUMULATED_RECORDS_FAIL = 'RANKINGS_FETCH_ACCUMULATED_RECORDS_FAIL'
export const RANKINGS_FETCH_ACCUMULATED_RECORDS_SUCCESS = 'RANKINGS_FETCH_ACCUMULATED_RECORDS_SUCCESS'
export const RANKINGS_FETCH_RECORDS_START = 'RANKINGS_FETCH_RECORDS_START'
export const RANKINGS_FETCH_RECORDS_FAIL = 'RANKINGS_FETCH_RECORDS_FAIL'
export const RANKINGS_FETCH_RECORDS_SUCCESS = 'RANKINGS_FETCH_RECORDS_SUCCESS'
export const RANKINGS_LIKE_RECORD_START = 'RANKINGS_LIKE_RECORD_START'
export const RANKINGS_LIKE_RECORD_FAIL = 'RANKINGS_LIKE_RECORD_FAIL'
export const RANKINGS_LIKE_RECORD_SUCCESS = 'RANKINGS_LIKE_RECORD_SUCCESS'
export const RANKINGS_SELECT_RECORD = 'RANKINGS_SELECT_PERIOD'
export const RANKINGS_VIEW_TOP10 = 'RANKINGS_VIEW_TOP10'
export const RANKINGS_VIEW_REST = 'RANKINGS_VIEW_REST'
export const RANKINGS_RESET_SELECTIONS = 'RANKINGS_RESET_SELECTIONS'
export const RANKINGS_FETCH_ROLES = 'RANKINGS_FETCH_ROLES'

const defaultRankingParams = {
  filter: {
    include: [
      { relation: 'users'},
      { relation: 'periods', order: 'endDate ASC'}
    ]
  }
};

const defaultAllRankingRecordsParams = {
  filter: {
    include: [{
      relation: 'periods'
    }]
  }
}

const defaultRecordsParams = {
  filter: {
    where: { periodId: null },
    order: 'score DESC',
    include: [
      { relation: 'user', scope: {include: ['country', 'line']}},
      { relation: 'likes' },
      { relation: 'period' },
      { relation: 'data', scope: {include: ['variable']}},
    ]
  }
};

export const fetchRankings = () => {
  return (dispatch) => {
    dispatch({ type: RANKINGS_FETCH_START })
    const access_token = window.localStorage.getItem('access_token')
    api.get('rankings', { params: { ...defaultRankingParams, access_token }})
      .then(res => {
        const rankings = res.data;
        dispatch({ type: RANKINGS_FETCH_SUCCESS, payload: rankings })
      })
      .catch(err => {
        dispatch({ type: RANKINGS_FETCH_FAIL, payload: err })
      })
  }
}

export const fetchAllRankingRecords = (id) => {
  return (dispatch) => {
    dispatch({ type: RANKINGS_FETCH_ACCUMULATED_RECORDS_START })
    const access_token = window.localStorage.getItem('access_token')
    api.get(`rankings/${id}`, { params: { ...defaultAllRankingRecordsParams, access_token }})
      .then(res => {
        console.log(res.data);
        const rankings = res.data;
        dispatch({ type: RANKINGS_FETCH_ACCUMULATED_RECORDS_SUCCESS, payload: rankings })
      })
      .catch(err => {
        dispatch({ type: RANKINGS_FETCH_ACCUMULATED_RECORDS_FAIL, payload: err })
      })
  }
}

export const fetchRoles = () => {
  return async (dispatch) => {
    const response = await api.get('roles', { params: { filter: { include: ['rankings'] } } });
    const roles = response.data;
    dispatch({ type: RANKINGS_FETCH_ROLES, payload: roles });
  }
}

export const select = (entityType, id) => {
  return (dispatch) => {
    switch (entityType) {
      case 'ranking': {
        dispatch({ type: RANKINGS_SELECT_RANKING, payload: id })
        dispatch({ type: RANKINGS_FETCH_ACCUMULATED_RECORDS_START })
        const access_token = window.localStorage.getItem('access_token')
        axios.all([
          api.get(`rankings/${id}`, { params: { ...defaultAllRankingRecordsParams, access_token }}),
          api.get(`rankings/accumulatedRecords/?rankingId=${id}`, { params: { access_token }}),
          api.get('countries', { params: { access_token }}),
          api.get('businessunits', { params: { access_token }})
        ])
          .then(res => {
            const ranking = res[0].data;
            const accumulatedRecords = res[1].data;
            const countries = res[2].data;
            const businessUnits = res[3].data;
            dispatch({
              type: RANKINGS_FETCH_ACCUMULATED_RECORDS_SUCCESS,
              payload: { ranking, accumulatedRecords, countries, businessUnits }
            })
          })
          .catch(err => {
            dispatch({ type: RANKINGS_FETCH_ACCUMULATED_RECORDS_FAIL, payload: err })
          })
        break;
      }

      case 'period': {
        dispatch({ type: RANKINGS_SELECT_PERIOD, payload: id })
        dispatch({ type: RANKINGS_FETCH_RECORDS_START })
        const access_token = window.localStorage.getItem('access_token');
        defaultRecordsParams.filter.where.periodId = id;
        api.get('records', { params: { ...defaultRecordsParams, access_token }})
          .then(res => {
            let records = res.data;
            _.forEach(records, record => calculateRecordScore(record));
            records = _.sortBy(records, record => record.score).reverse();
            dispatch({ type: RANKINGS_FETCH_RECORDS_SUCCESS, payload: records })
          })
          .catch(err => {
            dispatch({ type: RANKINGS_FETCH_RECORDS_FAIL, payload: err })
          })
        break;
      }

      case 'record': {
        dispatch({ type: RANKINGS_SELECT_RECORD, payload: id })
        break;
      }

      case 'country': {
        dispatch({ type: RANKINGS_SELECT_COUNTRY, payload: id })
        break;
      }

      case 'businessUnit': {
        dispatch({ type: RANKINGS_SELECT_BU, payload: id })
        break;
      }
    }
  }
}

export const unselect = (entityType, id) => {
  return (dispatch) => {
    switch (entityType) {
      case 'country': {
        dispatch({ type: RANKINGS_UNSELECT_COUNTRY, payload: id })
        break;
      }

      case 'businessUnit': {
        dispatch({ type: RANKINGS_UNSELECT_BU, payload: id })
        break;
      }
    }
  }
}

export const fetchAccumulatedRecordsByYear = (rankingId, year) => {
  return async (dispatch) => {
    try {
      dispatch({ type: RANKINGS_FETCH_ACCUMULATED_RECORDS_START });
      const access_token = window.localStorage.getItem('access_token');
      const { data: accumulatedRecords } = await api.get(
        `rankings/accumulatedRecords/?rankingId=${rankingId}&year=${year}`,
        { params: { access_token }}
      );
      dispatch({
        type: RANKINGS_FETCH_ACCUMULATED_RECORDS_SUCCESS,
        payload: { accumulatedRecords, year },
      });
    } catch(e) {
      dispatch({
        type: RANKINGS_FETCH_ACCUMULATED_RECORDS_FAIL,
        payload: e,
      });
      throw e;
    }
  };
}

export const likeRecord = (record, user) => {
  return (dispatch) => {
    dispatch({ type: RANKINGS_LIKE_RECORD_START })
    const access_token = window.localStorage.getItem('access_token')
    const like = { recordId: record.id, userId: user.id, createdDate: new Date() }
    api.post(`recordlikes`, like, { params: { access_token }})
      .then(res => {
        defaultRecordsParams.filter.where.periodId = record.periodId;
        return api.get(`records/${record.id}`, { params: { ...defaultRecordsParams, access_token }})
      })
      .then(res => {
        const record = res.data;
        dispatch({ type: RANKINGS_LIKE_RECORD_SUCCESS, payload: record })
      })
      .catch(err => {
        dispatch({ type: RANKINGS_LIKE_RECORD_FAIL, payload: err })
      })
  }
}

export const unlikeRecord = (record, user) => {
  return (dispatch) => {
    dispatch({ type: RANKINGS_LIKE_RECORD_START })
    const access_token = window.localStorage.getItem('access_token')
    const like = _.find(record.likes, { userId: user.id })
    api.delete(`recordlikes/${like.id}`, { params: { access_token }})
      .then(res => {
        defaultRecordsParams.filter.where.periodId = record.periodId;
        return api.get(`records/${record.id}`, { params: { ...defaultRecordsParams, access_token }})
      })
      .then(res => {
        const record = res.data;
        dispatch({ type: RANKINGS_LIKE_RECORD_SUCCESS, payload: record })
      })
      .catch(err => {
        dispatch({ type: RANKINGS_LIKE_RECORD_FAIL, payload: err })
      });
  }
}

export const viewTop10 = () => {
  return ({ type: RANKINGS_VIEW_TOP10 });
}

export const viewRest = () => {
  return ({ type: RANKINGS_VIEW_REST });
}

function calculateRecordScore(record) {
  let score = 0;
  _.forEach(record.data, dataValue => {
    // const variable = _.find(record.variables, { id: dataValue.variableId });
    if (!dataValue.variable) return;
    score += (dataValue.value * dataValue.variable.weight / 100);
  });
  return record.score = score;
}
