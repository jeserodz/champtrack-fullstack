import api from '../api';
import axios from 'axios';
import * as uxActions from './UXActions';
export const ADMIN_MESSAGES_FETCH_START = 'ADMIN_MESSAGES_FETCH_START';
export const ADMIN_MESSAGES_FETCH_FAIL = 'ADMIN_MESSAGES_FETCH_FAIL';
export const ADMIN_MESSAGES_FETCH_SUCCESS = 'ADMIN_MESSAGES_FETCH_SUCCESS';
export const ADMIN_MESSAGES_CREATE_START = 'ADMIN_MESSAGES_CREATE_START';
export const ADMIN_MESSAGES_CREATE_FAIL = 'ADMIN_MESSAGES_CREATE_FAIL';
export const ADMIN_MESSAGES_CREATE_SUCCESS = 'ADMIN_MESSAGES_CREATE_SUCCESS';
export const ADMIN_MESSAGES_FETCH_USERS_START = 'ADMIN_MESSAGES_FETCH_USERS_START';
export const ADMIN_MESSAGES_FETCH_USERS_FAIL = 'ADMIN_MESSAGES_FETCH_USERS_FAIL';
export const ADMIN_MESSAGES_FETCH_USERS_SUCCESS = 'ADMIN_MESSAGES_FETCH_USERS_SUCCESS';
export const ADMIN_MESSAGES_SELECT = 'ADMIN_MESSAGES_SELECT';
export const ADMIN_MESSAGES_TOGGLE_SELECT_USER = 'ADMIN_MESSAGES_TOGGLE_SELECT_USER';
export const ADMIN_MESSAGES_SELECT_ENTITY = 'ADMIN_MESSAGES_SELECT_ENTITY';
export const ADMIN_MESSAGESS_SELECT_ALL = 'ADMIN_MESSAGESS_SELECT_ALL';
export const ADMIN_MESSAGESS_UNSELECT_ALL = 'ADMIN_MESSAGESS_DESELECT_ALL';
export const ADMIN_MESSAGESS_SEND_START = 'ADMIN_MESSAGESS_SEND_START';
export const ADMIN_MESSAGESS_SEND_FAIL = 'ADMIN_MESSAGESS_SEND_FAIL';
export const ADMIN_MESSAGESS_SEND_SUCCESS = 'ADMIN_MESSAGESS_SEND_SUCCESS';
export const ADMIN_MESSAGESS_TEXT_CHANGE = 'ADMIN_MESSAGESS_TEXT_CHANGE';

export const fetchMessages = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_MESSAGES_FETCH_START })
    const access_token = window.localStorage.getItem('access_token')
    api.get('messages', { params: { access_token }})
      .then(res => {
        const messages = res.data;
        dispatch({ type: ADMIN_MESSAGES_FETCH_SUCCESS, payload: messages })
      })
      .catch(err => {
        dispatch({ type: ADMIN_MESSAGES_FETCH_FAIL, payload: err })
      })
  }
}

export const selectMessage = (id) => {
  return { type: ADMIN_MESSAGES_SELECT, payload: id }
}

export const createMessage = (message) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_MESSAGES_CREATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.post('messages', message, { params: { access_token } })
      .then(res => {
        return api.get('messages', { params: { access_token }})
      })
      .then(res => {
        const messages = res.data;
        return dispatch({ type: ADMIN_MESSAGES_CREATE_SUCCESS, payload: messages })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_MESSAGES_CREATE_FAIL, payload: err })
      })
  }
}

export const fetchMessagesUsers = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_MESSAGES_FETCH_USERS_START })
    const access_token = window.localStorage.getItem('access_token')
    axios.all([
      api.get('users', { params: { access_token, filter: {
          include: ['country', 'businessUnit', 'ranking']
      }}}),
      api.get('businessUnits', { params: { access_token, filter: {
        include: ['lines']
      }}}),
      api.get('countries', { params: { access_token }}),
      api.get('rankings', { params: { access_token }}),
    ])
      .then(res => {
        const users = res[0].data;
        const businessUnits = res[1].data;
        const countries = res[2].data;
        const rankings = res[3].data;
        dispatch({ type: ADMIN_MESSAGES_FETCH_USERS_SUCCESS, payload: { users, businessUnits, countries, rankings } })
      })
      .catch(err => {
        dispatch({ type: ADMIN_MESSAGES_FETCH_USERS_FAIL, payload: err })
      })
  }
}

export const onTextChange = (text) => {
  return { type: ADMIN_MESSAGESS_TEXT_CHANGE, payload: text }
}

export const toggleUserSelection = (user) => {
  return { 
    type: ADMIN_MESSAGES_TOGGLE_SELECT_USER,
    payload: user
  }
}

export const selectEntity = (entityName, id) => {
  return {
    type: ADMIN_MESSAGES_SELECT_ENTITY,
    payload: { entityName, id }
  }
}

export const selectAll = (rankingId, countryId, businessUnitId) => {
  return {
    type: ADMIN_MESSAGESS_SELECT_ALL,
    payload: { rankingId, countryId, businessUnitId }
  }
}

export const unselectAll = (rankingId, countryId, businessUnitId) => {
  return {
    type: ADMIN_MESSAGESS_UNSELECT_ALL,
    payload: { rankingId, countryId, businessUnitId }
  }
}

export const sendMessage = (currentUser, selectedUsers, text) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_MESSAGESS_SEND_START });
    const fromId = currentUser.id;
    const userIds = selectedUsers.filter(user => user.selected === true).map(user => user.id);
    const access_token = window.localStorage.getItem('access_token');
    api.post('messages/sendMessage', 
      { fromId, userIds, text }, 
      { params: { access_token } }
    )
      .then(res => {
        dispatch({ type: ADMIN_MESSAGESS_SEND_SUCCESS });
        dispatch(uxActions.showSnackbarMessage('Mensage enviado!'));
      })
      .catch(err => {
        return dispatch({ type: ADMIN_MESSAGESS_SEND_FAIL, payload: err });
      });
  }
}