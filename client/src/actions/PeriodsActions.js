import _ from 'lodash';
import api from '../api';

export const PERIODS_FETCHED = "PERIODS_FETCHED";
export const PERIOD_ADDED = "PERIOD_ADDED";
export const PERIOD_CHANGED = "PERIOD_CHANGED";
export const PERIOD_REMOVED = "PERIOD_REMOVED";
export const PERIOD_SELECTED = "PERIOD_SELECTED";

export const setupPeriods = () => {
  return (dispatch) => {
    api.get('periods')
      .then(res => {
        dispatch({ type: PERIODS_FETCHED, payload: res.data })
      })
      .catch(err => {
        console.log(err);
      });
  };
}

export const selectPeriod = (id) => {
  return { type: PERIOD_SELECTED, payload: id };
}

export const addPeriod = (data) => {
  return function(dispatch) {
    api.post('periods', data)
      .then(res => {
        const period = res.data;
        dispatch({ type: PERIOD_ADDED, payload: period })
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export const changePeriod = (id, data) => {
  return function(dispatch) {
    api.patch(`periods/${id}`, data)
      .then(res => {
        const period = res.data;
        dispatch({ type: PERIOD_CHANGED, payload: period })
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export const removePeriod = (id) => {
  return function(dispatch) {
    api.delete(`periods/${id}`)
      .then(res => {
        dispatch({ type: PERIOD_REMOVED, payload: { key: id } })
      })
      .catch(err => {
        console.log(err);
      });
  }
}