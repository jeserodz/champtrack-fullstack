import api from '../api';

export const COUNTRIES_FETCHED = "COUNTRIES_FETCHED"
export const COUNTRY_ADDED = "COUNTRY_ADDED"
export const COUNTRY_CHANGED = "COUNTRY_CHANGED"
export const COUNTRY_REMOVED = "COUNTRY_REMOVED"
export const COUNTRY_SELECTED = "COUNTRY_SELECTED"
export const COUNTRY_IMAGE_UPLOAD_PROGRESS = "COUNTRY_IMAGE_UPLOAD_PROGRESS"
export const COUNTRY_IMAGE_UPLOAD_SUCCESS = "COUNTRY_IMAGE_UPLOAD_SUCCESS"

export const fetchCountries = () => {
  return (dispatch) => {
    api.get('countries')
      .then(res => {
        dispatch({ type: COUNTRIES_FETCHED, payload: res.data })
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export const selectCountry = (id) => {
  return { type: COUNTRY_SELECTED, payload: id };
}

export const addCountry = (data) => {
  return function(dispatch) {
    api.post('countries', data)
      .then(res => {
        dispatch({ type: COUNTRY_ADDED, payload: { key: res.data.id, val: res.data } })
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export const changeCountry = (id, data) => {
  return function(dispatch) {
    api.update(`countries/${id}`, data)
      .then(res => {
        dispatch({ type: COUNTRY_CHANGED, payload: { key: res.data.id, val: res.data } })
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export const removeCountry = (id) => {
  return function(dispatch) {
    api.delete(`countries/${id}`)
      .then(res => {
        dispatch({ type: COUNTRY_REMOVED, payload: { key: id } })
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export const uploadCountryImage = (key, file) => {
  return function(dispatch) {
    // let extension = file.name.split('.');
    // extension = extension[extension.length-1];

    // const ref = firebase.storage().ref('/images/countries/' + key + '.' + extension);
    // let uploadTask = ref.put(file);

    // uploadTask.on('state_changed', snapshot => {
    //   var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    //   console.log('Upload is ' + progress + '% done');
    //   dispatch({ type: COUNTRY_IMAGE_UPLOAD_PROGRESS, payload: progress })
    // });

    // uploadTask
    //   .then(res => ref.updateMetadata({ extension }))
    //   .then(res => ref.getDownloadURL())
    //   .then(photoURL => {
    //     console.log(photoURL);
    //     countriesRef.child(key).update({ photoURL });
    //     dispatch({ type: COUNTRY_IMAGE_UPLOAD_SUCCESS });
    //   });
  }
}