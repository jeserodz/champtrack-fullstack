import api from '../api'

export const ADMIN_RANKINGS_FETCH_START = 'ADMIN_RANKINGS_FETCH_START'
export const ADMIN_RANKINGS_FETCH_USERS_START = 'ADMIN_RANKINGS_FETCH_USERS_START'
export const ADMIN_RANKINGS_UPDATE_START = 'ADMIN_RANKINGS_UPDATE_START'
export const ADMIN_RANKINGS_DEACTIVATE_START = 'ADMIN_RANKINGS_DEACTIVATE_START'
export const ADMIN_RANKINGS_FETCH_SUCCESS = 'ADMIN_RANKINGS_FETCH_SUCCESS'
export const ADMIN_RANKINGS_FETCH_USERS_SUCCESS = 'ADMIN_RANKINGS_FETCH_USERS_SUCCESS'
export const ADMIN_RANKINGS_UPDATE_SUCCESS = 'ADMIN_RANKINGS_UPDATE_SUCCESS'
export const ADMIN_RANKINGS_DEACTIVATE_SUCCESS = 'ADMIN_RANKINGS_DEACTIVATE_SUCCESS'
export const ADMIN_RANKINGS_FETCH_FAIL = 'ADMIN_RANKINGS_FETCH_FAIL'
export const ADMIN_RANKINGS_FETCH_USERS_FAIL = 'ADMIN_RANKINGS_FETCH_USERS_FAIL'
export const ADMIN_RANKINGS_UPDATE_FAIL = 'ADMIN_RANKINGS_UPDATE_FAIL'
export const ADMIN_RANKINGS_DEACTIVATE_FAIL = 'ADMIN_RANKINGS_DEACTIVATE_FAIL'
export const ADMIN_RANKINGS_SELECT = 'ADMIN_RANKINGS_SELECT'
export const ADMIN_RANKING_IMAGE_UPLOAD_PROGRESS = 'ADMIN_RANKING_IMAGE_UPLOAD_PROGRES'
export const ADMIN_RANKING_IMAGE_UPLOAD_SUCCESS = 'ADMIN_RANKING_IMAGE_UPLOAD_SUCCESS'

const defaultRankingsParams = {
  filter: {
    include: ['users', 'periods', 'variables']
  }
}

const defaultUsersParams = {
  filter: {
    include: ['businessUnit', 'country', 'ranking']
  }
}

export const fetchRankings = (dispatch) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_RANKINGS_FETCH_START })
    const access_token = window.localStorage.getItem('access_token');
    api.get('rankings', { params: { ...defaultRankingsParams, access_token }})
      .then(res => {
        dispatch({ type: ADMIN_RANKINGS_FETCH_SUCCESS, payload: res.data })
        dispatch({ type: ADMIN_RANKINGS_FETCH_START })
        return api.get('users', { params: { ...defaultUsersParams, access_token } })
      })
      .then(res => {
        return dispatch({ type: ADMIN_RANKINGS_FETCH_USERS_SUCCESS, payload: res.data })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_RANKINGS_FETCH_FAIL, payload: err })
      })
  }
}

export const select = (entityType, id) => {
  return { 
    type: ADMIN_RANKINGS_SELECT, 
    payload: { entityType, id }
  }
}

export const create = (entityType, data) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_RANKINGS_UPDATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.post(`${entityType}s`, data, { params: { access_token }})
      .then(res => {
        dispatch({ type: ADMIN_RANKINGS_UPDATE_SUCCESS })
        dispatch({ type: ADMIN_RANKINGS_FETCH_START })
        return api.get('rankings', { params: { ...defaultRankingsParams, access_token } })
      })
      .then(res => {
        return dispatch({ type: ADMIN_RANKINGS_FETCH_SUCCESS, payload: res.data })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_RANKINGS_FETCH_FAIL, payload: err })
      })
  }
}

export const update = (entityType, data) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_RANKINGS_UPDATE_START })
    const access_token = window.localStorage.getItem('access_token')
    api.patch(`${entityType}s/${data.id}`, data, { params: { access_token }})
      .then(res => {
        dispatch({ type: ADMIN_RANKINGS_UPDATE_SUCCESS })
        dispatch({ type: ADMIN_RANKINGS_FETCH_START })
        return api.get('rankings', { params: { ...defaultRankingsParams, access_token} })
      })
      .then(res => {
        return dispatch({ type: ADMIN_RANKINGS_FETCH_SUCCESS, payload: res.data })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_RANKINGS_FETCH_FAIL, payload: err })
      })
  }
}

export const assignRankingToUser = (user, rankingId) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_RANKINGS_UPDATE_START })
    const access_token = window.localStorage.getItem('access_token');
    api.patch(`users/${user.id}`, user, { params: { access_token } })
      .then(res => {
        dispatch({ type: ADMIN_RANKINGS_UPDATE_SUCCESS })
        dispatch({ type: ADMIN_RANKINGS_FETCH_USERS_START })
        const access_token = window.localStorage.getItem('access_token');
        return api.get('users', { params: { ...defaultUsersParams, access_token } })
      })
      .then(res => {
        return dispatch({ type: ADMIN_RANKINGS_FETCH_USERS_SUCCESS, payload: res.data })
      })
      .catch(err => {
        return dispatch({ type: ADMIN_RANKINGS_FETCH_USERS_FAIL, payload: err })
      })
  }
}

export const uploadRankingImage = (id, file) => {
  return function(dispatch) {
    let extension = file.name.split('.');
    extension = extension[extension.length-1];
    const data = new FormData();
    data.append('file', file);
    const access_token = window.localStorage.getItem('access_token')
    api.post(`uploads/rankings/upload`, data, {
      params: { id, access_token },
      onUploadProgress: function(progressEvent) {
        const percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
        console.log(percentCompleted + '% uploaded');
        dispatch({ type: ADMIN_RANKING_IMAGE_UPLOAD_PROGRESS, payload: percentCompleted });
      }
    }).then(res => {
      console.log('100% complete');
      dispatch({ type: ADMIN_RANKING_IMAGE_UPLOAD_SUCCESS });
      dispatch({ type: ADMIN_RANKINGS_FETCH_START })
      return api.get('rankings', { params: { ...defaultRankingsParams, access_token } })
    }).then(res => {
      return dispatch({ type: ADMIN_RANKINGS_FETCH_SUCCESS, payload: res.data })
    }).catch(err => {
      dispatch({ type: ADMIN_RANKINGS_UPDATE_FAIL, payload: err });
    });
  }
}

// TODO
// export const deactivate = (entityType, entity) => {}