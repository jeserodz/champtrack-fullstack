import api from '../api';

export const ADMIN_POSTS_FETCH_START = 'ADMIN_POSTS_FETCH_START'
export const ADMIN_POSTS_FETCH_SUCCESS = 'ADMIN_POSTS_FETCH_SUCCESS'
export const ADMIN_POSTS_FETCH_FAIL = 'ADMIN_POSTS_FETCH_FAIL'
export const ADMIN_POSTS_CREATE_START = 'ADMIN_POSTS_CREATE_START'
export const ADMIN_POSTS_CREATE_SUCCESS = 'ADMIN_POSTS_CREATE_SUCCESS'
export const ADMIN_POSTS_CREATE_FAIL = 'ADMIN_POSTS_CREATE_FAIL'
export const ADMIN_POSTS_UPDATE_START = 'ADMIN_POSTS_UPDATE_START'
export const ADMIN_POSTS_UPDATE_SUCCESS = 'ADMIN_POSTS_UPDATE_SUCCESS'
export const ADMIN_POSTS_UPDATE_FAIL = 'ADMIN_POSTS_UPDATE_FAIL'
export const ADMIN_POSTS_SELECT_POST = 'ADMIN_POSTS_SELECT_POST'
export const ADMIN_POSTS_UPLOAD_IMAGE_PROGRESS = 'ADMIN_POSTS_UPLOAD_IMAGE_PROGRESS'
export const ADMIN_POSTS_UPLOAD_IMAGE_SUCCESS = 'ADMIN_POSTS_UPLOAD_IMAGE_SUCCESS'

export const fetchPosts = () => {
  return (dispatch) => {
    dispatch({ type: ADMIN_POSTS_FETCH_START });
    const access_token = window.localStorage.getItem('access_token')
    api.get('posts', { params: { access_token }})
      .then(res => dispatch({ type: ADMIN_POSTS_FETCH_SUCCESS, payload: res.data }))
      .catch(err => dispatch({ type: ADMIN_POSTS_FETCH_FAIL, payload: err }))
  }
}

export const selectPost = (id) => {
  return { type: ADMIN_POSTS_SELECT_POST, payload: id }
}

export const createPost = (post) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_POSTS_CREATE_START });
    const access_token = window.localStorage.getItem('access_token')
    api.post('posts', post, { params: { access_token }})
      .then(res => api.get('posts', { params: { access_token }}))
      .then(res => dispatch({ type: ADMIN_POSTS_CREATE_SUCCESS, payload: res.data }))
      .catch(err => dispatch({ type: ADMIN_POSTS_CREATE_FAIL, payload: err }))
  }
}

export const updatePost = (post) => {
  return (dispatch) => {
    dispatch({ type: ADMIN_POSTS_UPDATE_START });
    const access_token = window.localStorage.getItem('access_token')
    api.patch(`posts/${post.id}`, post, { params: { access_token }})
      .then(res => api.get('posts', { params: { access_token }}))
      .then(res => dispatch({ type: ADMIN_POSTS_UPDATE_SUCCESS, payload: res.data }))
      .catch(err => dispatch({ type: ADMIN_POSTS_UPDATE_FAIL, payload: err }))
  }
}

export const uploadPostImage = (id, file) => {
  return function(dispatch) {
    let extension = file.name.split('.');
    extension = extension[extension.length-1];
    const data = new FormData();
    data.append('file', file);
    const access_token = window.localStorage.getItem('access_token')
    api.post(`uploads/posts/upload`, data, {
      params: { id, access_token },
      onUploadProgress: function(progressEvent) {
        const percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
        console.log(percentCompleted + '% uploaded');
        dispatch({ type: ADMIN_POSTS_UPLOAD_IMAGE_PROGRESS, payload: percentCompleted });
      }
    }).then(res => {
      if (!Promise) return res;
      return new Promise((resolve, reject) => {
        window.setTimeout(() => { resolve(res) }, 2000);
      })
    }).then(res => {
      console.log('100% complete');
      dispatch({ type: ADMIN_POSTS_UPLOAD_IMAGE_SUCCESS });
      dispatch({ type: ADMIN_POSTS_FETCH_START });
      return api.get('posts', { params: { access_token }});
    }).then(res => {
      return dispatch({ type: ADMIN_POSTS_FETCH_SUCCESS, payload: res.data })
    }).catch(err => {
      dispatch({ type: ADMIN_POSTS_UPDATE_FAIL, payload: err });
    });
  }
}