import api from '../api';
import * as UXActions from './UXActions';
export const USER_FETCH_START = 'USER_FETCH_START';
export const USER_FETCH_FAIL = 'USER_FETCH_FAIL';
export const USER_FETCH_SUCCESS = 'USER_FETCH_SUCCESS';
export const USER_IMAGE_UPLOAD_PROGRESS = 'USER_IMAGE_UPLOAD_PROGRES';
export const USER_IMAGE_UPLOAD_SUCCESS = 'USER_IMAGE_UPLOAD_SUCCESS';
export const USER_CHANGE_PASSWORD_START = 'USER_CHANGE_PASSWORD_START';
export const USER_CHANGE_PASSWORD_FAIL = 'USER_CHANGE_PASSWORD_FAIL';
export const USER_CHANGE_PASSWORD_SUCCESS = 'USER_CHANGE_PASSWORD_SUCCESS';
export const USER_CLEAR_UNREAD_NOTIFICATIONS = 'USER_CLEAR_UNREAD_NOTIFICATIONS';

const defaultUserParams = {
  filter: {
    include: ['country', 'businessUnit', 'ranking' ]
  }
}

export const fetchUser = (id) => {
  return (dispatch) => {
    dispatch({ type: USER_FETCH_START });
    const access_token = window.localStorage.getItem('access_token');
    api.get(`users/${id}`, { params: { ...defaultUserParams, access_token }})
      .then(res => {
        const user = res.data;
        dispatch({ type: USER_FETCH_SUCCESS, payload: user });
      })
      .catch(err => {
        dispatch({ type: USER_FETCH_FAIL, payload: err });
      });
  };
}

export const uploadUserImage = (id, file) => {
  return function(dispatch) {
    let extension = file.name.split('.');
    extension = extension[extension.length-1];
    const data = new FormData();
    data.append('file', file);
    const access_token = window.localStorage.getItem('access_token')
    api.post(`uploads/profiles/upload`, data, {
      params: { id, access_token },
      onUploadProgress: function(progressEvent) {
        const percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
        console.log(percentCompleted + '% uploaded');
        dispatch({ type: USER_IMAGE_UPLOAD_PROGRESS, payload: percentCompleted });
      }
    }).then(res => {
      if (!Promise) return res;
      return new Promise((resolve, reject) => {
        window.setTimeout(() => { resolve(res) }, 2000);
      })
    }).then(res => {
      console.log('100% complete');
      dispatch({ type: USER_IMAGE_UPLOAD_SUCCESS });
      dispatch({ type: USER_FETCH_START })
      return api.get(`users/${id}`, { params: { ...defaultUserParams, access_token }})
    }).then(res => {
      return dispatch({ type: USER_FETCH_SUCCESS, payload: res.data })
    }).catch(err => {
      dispatch({ type: USER_FETCH_FAIL, payload: err });
    });
  }
}

export const changeUserPassword = (id, password) => {
  if (!id || !password) return { 
    type: USER_CHANGE_PASSWORD_FAIL, 
    payload: { id, password }
  };

  return (dispatch) => {
    dispatch({ type: USER_CHANGE_PASSWORD_START });
    const access_token = window.localStorage.getItem('access_token');
    api.patch(`users/${id}`, { password }, { params: { access_token }})
      .then(res => {
        const user = res.data;
        dispatch({ type: USER_CHANGE_PASSWORD_SUCCESS, payload: user });
        dispatch(UXActions.closeChangePasswordDialog());
        dispatch(UXActions.showSnackbarMessage('Password cambiado'));
      })
      .catch(err => {
        dispatch({ type: USER_CHANGE_PASSWORD_FAIL, payload: err });
      });
  };
}

export const clearUnreadNotifications = (userId, notificationType) => {
  return dispatch => {
    const access_token = window.localStorage.getItem('access_token');
    api.post(`users/${userId}/clearUnreadNotifications`, { notificationType }, { params: { access_token }})
      .then(response => dispatch({ type: USER_CLEAR_UNREAD_NOTIFICATIONS, payload: response.data }));
  }
}