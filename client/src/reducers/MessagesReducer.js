import _ from 'lodash';
import * as Actions from '../actions';

const INITIAL_STATE = {
  list: [],
  loading: false,
  error: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
      case Actions.MESSAGES_FETCH_START:
      case Actions.MESSAGES_HIDE_START: {
        return { ...state, loading: true }
      }
      
      case Actions.MESSAGES_FAIL: {
        return { ...state, loading: false, error: action.payload }
      }

      case Actions.MESSAGES_SUCCESS: {
        return { ...state, loading: false, error: null, list: action.payload }
      }

      case Actions.MESSAGES_HIDE_SUCCESS: {
        const newState = _.cloneDeep(state);
        newState.list = newState.list.filter(msg => msg.id !== action.payload);
        return { ...newState, loading: false, error: null }
      }

      default:
        return state;
  }
}