import {
  ADMIN_RANKINGS_FETCH_START,
  ADMIN_RANKINGS_FETCH_USERS_START,
  ADMIN_RANKINGS_UPDATE_START,
  ADMIN_RANKINGS_DEACTIVATE_START,
  ADMIN_RANKINGS_FETCH_SUCCESS,
  ADMIN_RANKINGS_FETCH_USERS_SUCCESS,
  ADMIN_RANKINGS_FETCH_FAIL,
  ADMIN_RANKINGS_FETCH_USERS_FAIL,
  ADMIN_RANKINGS_UPDATE_FAIL,
  ADMIN_RANKINGS_DEACTIVATE_FAIL,
  ADMIN_RANKINGS_SELECT,
  ADMIN_RANKING_IMAGE_UPLOAD_PROGRESS,
  ADMIN_RANKING_IMAGE_UPLOAD_SUCCESS
} from '../actions/AdminRankingsActions';

const INITIAL_STATE = {
  rankings: [],
  users: [],
  selectedRanking: null,
  selectedPeriod: null,
  selectedVariable: null,
  loading: false,
  error: null,
  uploadProgress: null
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case ADMIN_RANKINGS_FETCH_START:
    case ADMIN_RANKINGS_FETCH_USERS_START:
    case ADMIN_RANKINGS_UPDATE_START:
    case ADMIN_RANKINGS_DEACTIVATE_START:
      return { 
        ...state, 
        error: null, 
        loading: true 
      }

    case ADMIN_RANKINGS_FETCH_FAIL:
    case ADMIN_RANKINGS_FETCH_USERS_FAIL:
    case ADMIN_RANKINGS_UPDATE_FAIL:
    case ADMIN_RANKINGS_DEACTIVATE_FAIL:
      return { 
        ...state, 
        error: action.payload, 
        loading: false 
      }
      
    case ADMIN_RANKINGS_FETCH_SUCCESS:
      return {
        ...state,
        rankings: action.payload,
        error: null,
        loading: false
      }

    case ADMIN_RANKINGS_FETCH_USERS_SUCCESS: 
      return {
        ...state,
        users: action.payload,
        error: null,
        loading: false
      }

    case ADMIN_RANKINGS_SELECT: {
      switch (action.payload.entityType) {
        case 'ranking':
          return { ...state, selectedRanking: action.payload.id }
        case 'period':
          return { ...state, selectedPeriod: action.payload.id }
        case 'variable':
          return { ...state, selectedVariable: action.payload.id }
        default:
          return state
      }
    }

    case ADMIN_RANKING_IMAGE_UPLOAD_PROGRESS: {
      return { ...state, uploadProgress: action.payload }
    }

    case ADMIN_RANKING_IMAGE_UPLOAD_SUCCESS: {
      return { ...state, uploadProgress: null }
    }

    // update and deactivate actions are reloaded
    // with a complete fetch action

    default: 
      return state;
  }
}