import api from '../api';
import {
  ADMIN_LINE_FETCH_START,
  ADMIN_LINE_FETCH_FAIL,
  ADMIN_LINE_FETCH_SUCCESS,
  ADMIN_LINE_CREATE_START,
  ADMIN_LINE_CREATE_FAIL,
  ADMIN_LINE_CREATE_SUCCESS,
  ADMIN_LINE_UPDATE_START,
  ADMIN_LINE_UPDATE_FAIL,
  ADMIN_LINE_UPDATE_SUCCESS,
  ADMIN_LINE_UPLOAD_IMAGE_PROGRESS,
  ADMIN_LINE_UPLOAD_IMAGE_SUCCESS,
  ADMIN_LINE_SELECT_BU
} from '../actions/AdminLinesActions';

const INTIAL_STATE = {
  lines: [],
  selectedLine: null,
  loading: false,
  error: null
}

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case ADMIN_LINE_FETCH_START:
    case ADMIN_LINE_CREATE_START:
    case ADMIN_LINE_UPDATE_START: {
      return { ...state, loading: true, error: null }
    }
    case ADMIN_LINE_FETCH_FAIL:
    case ADMIN_LINE_CREATE_FAIL:
    case ADMIN_LINE_UPDATE_FAIL: {
      return { ...state, loading: false, error: action.payload }
    }
    case ADMIN_LINE_FETCH_SUCCESS: {
      return { ...state, loading: false, error: null, lines: action.payload }
    }
    case ADMIN_LINE_SELECT_BU: {
      return{ ...state, selectedLine: action.payload }
    }
    default: {
      return state;
    }
  }
}