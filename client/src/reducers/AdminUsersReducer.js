import _ from 'lodash';
import * as Actions from '../actions/AdminUsersActions';

const INTIAL_STATE = {
  users: [],
  countries: [],
  businessUnits: [],
  lines: [],
  rankings: [],
  roles: [],
  selectedUser: null,
  selectedCountry: null,
  selectedBusinessUnit: null,
  selectedLine: null,
  selectedRanking: null,
  uploadProgress: null,
  loading: false,
  error: null
}

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case Actions.ADMIN_USERS_INITIAL_FETCH_START:
    case Actions.ADMIN_USERS_FETCH_START:
    case Actions.ADMIN_USERS_CREATE_START:
    case Actions.ADMIN_USERS_UPDATE_START: 
    case Actions.ADMIN_USERS_ASSIGN_ROLE_START: {
      return { ...state, loading: true, error: null }
    }
    case Actions.ADMIN_USERS_INITIAL_FETCH_FAIL:
    case Actions.ADMIN_USERS_FETCH_FAIL:
    case Actions.ADMIN_USERS_CREATE_FAIL:
    case Actions.ADMIN_USERS_UPDATE_FAIL: 
    case Actions.ADMIN_USERS_ASSIGN_ROLE_FAIL: {
      return { ...state, loading: false, error: action.payload }
    }

    case Actions.ADMIN_USERS_INITIAL_FETCH_SUCCESS: {
      return { 
        ...state, 
        loading: false, 
        error: null, 
        users: action.payload.users,
        countries: action.payload.countries,
        businessUnits: action.payload.businessUnits,
        lines: action.payload.lines,
        rankings: action.payload.rankings,
        roles: action.payload.roles
      }
    }

    case Actions.ADMIN_USERS_FETCH_SUCCESS: {
      return { 
        ...state, 
        loading: false, 
        error: null, 
        users: action.payload
      }
    }

    case Actions.ADMIN_USERS_UPLOAD_IMAGE_PROGRESS: {
      return { ...state, loading: true, uploadProgress: action.payload }
    }

    case Actions.ADMIN_USERS_UPLOAD_IMAGE_SUCCESS: {
      return { ...state, loading: false, uploadProgress: null }
    }

    case Actions.ADMIN_USERS_SELECT_USER: {
      return { 
        ...state, 
        selectedUser: action.payload,
        selectedCountry: null,
        selectedBusinessUnit: null,
        selectedLine: null,
        selectedRanking: null
      }
    }

    case Actions.ADMIN_USERS_SELECT_COUNTRY: {
      return { ...state, selectedCountry: action.payload }
    }

    case Actions.ADMIN_USERS_SELECT_BU: {
      return { ...state, selectedBusinessUnit: action.payload }
    }

    case Actions.ADMIN_USERS_SELECT_LINE: {
      return { ...state, selectedLine: action.payload }
    }

    case Actions.ADMIN_USERS_SELECT_RANKING: {
      return { ...state, selectedRanking: action.payload }
    }

    case Actions.ADMIN_USERS_ASSIGN_ROLE_SUCCESS: {
      const users = [ ...state.users ];
      users.forEach(u => {
        if (u.id === action.payload.id) u.roles = action.payload.roles;
      });
      return { ...state, users };
    }
    default: {
      return state;
    }
  }
}