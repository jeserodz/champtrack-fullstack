import * as Actions from '../actions';

const INITIAL_STATE = { 
  list: [],
  user: null
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {

    case Actions.USER_LOGIN_SUCCESS:
      return { ...state, user: action.payload }

    case Actions.USER_LOGOUT:
      return { ...state, user: null }

    case Actions.USER_CLEAR_UNREAD_NOTIFICATIONS: 
      return { ...state, user: { ...state.user, ...action.payload }}
      
    default:
      return state;
  }
}