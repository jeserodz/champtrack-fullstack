import { RECORDS_FETCHED, RECORD_ADDED, RECORD_CHANGED, RECORD_REMOVED, RECORD_SELECTED } from '../actions';


const INITIAL_STATE = { list: {}, selected: null };


export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    
    case RECORDS_FETCHED: {
      return { ...state, list: action.payload };
    }

    case RECORD_ADDED: {
      const list = { ...state.list, [action.payload.key]: action.payload.val };
      return { ...state, list }
    }

    case RECORD_CHANGED: {
      const list = { ...state.list, [action.payload.key]: action.payload.val };
      return { ...state, list }
    }

    case RECORD_REMOVED: {
      const list = Object.create(state.list);
      delete list[action.payload.key];
      return { ...state, list }
    }

    case RECORD_SELECTED: {
      return { ...state, selected: action.payload }
    }

    default:
      return state;
  }
}