import { 
  PERIODS_FETCHED,
  PERIOD_ADDED,
  PERIOD_CHANGED,
  PERIOD_REMOVED,
  PERIOD_SELECTED
} from '../actions';


const INITIAL_STATE = { list: [], selected: null };


export default (state = INITIAL_STATE, action) => {
  switch(action.type) {

    case PERIODS_FETCHED: {
      return { ...state, list: action.payload }
    }

    case PERIOD_ADDED: {
      const list = state.list.concat([ action.payload ]);
      return { ...state, list }
    }

    case PERIOD_CHANGED: {
      const list = { ...state.list, [action.payload.key]: action.payload.val };
      return { ...state, list }
    }

    case PERIOD_REMOVED: {
      const list = Object.create(state.list);
      delete list[action.payload.key];
      return { ...state, list }
    }
    
    case PERIOD_SELECTED: {
      return { ...state, selected: action.payload }
    }

    default:
      return state;
  }
}