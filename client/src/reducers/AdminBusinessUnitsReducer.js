import api from '../api';
import {
  ADMIN_BU_FETCH_START,
  ADMIN_BU_FETCH_FAIL,
  ADMIN_BU_FETCH_SUCCESS,
  ADMIN_BU_CREATE_START,
  ADMIN_BU_CREATE_FAIL,
  ADMIN_BU_CREATE_SUCCESS,
  ADMIN_BU_DELETE_START,
  ADMIN_BU_DELETE_FAIL,
  ADMIN_BU_DELETE_SUCCESS,
  ADMIN_BU_UPDATE_START,
  ADMIN_BU_UPDATE_FAIL,
  ADMIN_BU_UPDATE_SUCCESS,
  ADMIN_BU_UPLOAD_IMAGE_PROGRESS,
  ADMIN_BU_UPLOAD_IMAGE_SUCCESS,
  ADMIN_BU_SELECT_BU
} from '../actions/AdminBusinessUnitsActions';

const INTIAL_STATE = {
  businessUnits: [],
  selectedBusinessUnit: null,
  loading: false,
  error: null
}

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case ADMIN_BU_FETCH_START:
    case ADMIN_BU_CREATE_START:
    case ADMIN_BU_DELETE_START:
    case ADMIN_BU_UPDATE_START: {
      return { ...state, loading: true, error: null }
    }
    case ADMIN_BU_FETCH_FAIL:
    case ADMIN_BU_CREATE_FAIL:
    case ADMIN_BU_DELETE_FAIL:
    case ADMIN_BU_UPDATE_FAIL: {
      return { ...state, loading: false, error: action.payload }
    }
    case ADMIN_BU_FETCH_SUCCESS: {
      return { ...state, loading: false, error: null, businessUnits: action.payload }
    }
    case ADMIN_BU_SELECT_BU: {
      return{ ...state, selectedBusinessUnit: action.payload }
    }
    default: {
      return state;
    }
  }
}