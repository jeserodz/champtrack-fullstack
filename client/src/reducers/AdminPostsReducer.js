import * as Actions from '../actions/AdminPostsActions';

const INITIAL_STATE = {
  posts: [],
  selectedPost: null,
  uploadProgress: null,
  loading: false,
  error: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.ADMIN_POSTS_FETCH_START:
    case Actions.ADMIN_POSTS_CREATE_START:
    case Actions.ADMIN_POSTS_UPDATE_START:
      return { 
        ...state, 
        loading: true 
      }

    case Actions.ADMIN_POSTS_FETCH_FAIL:
    case Actions.ADMIN_POSTS_CREATE_FAIL:
    case Actions.ADMIN_POSTS_UPDATE_FAIL:
      return { 
        ...state, 
        loading: false, 
        error: action.payload 
      }

    case Actions.ADMIN_POSTS_FETCH_SUCCESS: 
    case Actions.ADMIN_POSTS_CREATE_SUCCESS: 
    case Actions.ADMIN_POSTS_UPDATE_SUCCESS: 
      return { 
        ...state, 
        loading: false, 
        error: null, 
        posts: action.payload
      }

    case Actions.ADMIN_POSTS_SELECT_POST: {
      return {
        ...state,
        selectedPost: action.payload
      }
    }

    case Actions.ADMIN_POSTS_UPLOAD_IMAGE_PROGRESS: {
      return { 
        ...state, 
        loading: true, 
        uploadProgress: action.payload 
      }
    }

    case Actions.ADMIN_POSTS_UPLOAD_IMAGE_SUCCESS: {
      return {
        ...state,
        loading: false,
        uploadProgress: null
      }
    }

    default:
      return state;
  }
}