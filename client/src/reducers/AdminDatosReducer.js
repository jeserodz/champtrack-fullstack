import {
  ADMIN_DATOS_RANKINGS_START,
  ADMIN_DATOS_RANKINGS_SUCCESS,
  ADMIN_DATOS_RANKINGS_FAIL,
  ADMIN_DATOS_SELECT_RANKING,
  ADMIN_DATOS_SELECT_PERIOD,
  ADMIN_DATOS_SELECT_RECORD,
  ADMIN_DATOS_START,
  ADMIN_DATOS_SUCCESS,
  ADMIN_DATOS_FAIL,
  ADMIN_DATOS_SELECT_CSV,
  ADMIN_DATOS_UPLOAD_CSV_START,
  ADMIN_DATOS_UPLOAD_CSV_SUCCESS,
  ADMIN_DATOS_UPLOAD_CSV_FAIL,
} from '../actions/AdminDatosActions';

const INITIAL_STATE = {
  rankings: [],
  records: [],
  selectedRanking: null,
  selectedUser: null,
  selectedPeriod: null,
  selectedRecord: null,
  selectedFile: null,
  csvButtonLabel: 'Cargar Datos CSV',
  loading: false,
  error: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADMIN_DATOS_RANKINGS_START: {
      return { ...state, loading: true, error: null }
    }
    case ADMIN_DATOS_RANKINGS_SUCCESS: {
      return { ...state, rankings: action.payload, loading: false, error: null }
    }
    case ADMIN_DATOS_RANKINGS_FAIL:  {
      return { ...state, loading: false, error: action.payload }
    }
    case ADMIN_DATOS_SELECT_RANKING: {
      return { ...state, selectedRanking: action.payload, records: [], selectedPeriod: null, selectedRecord: null }
    }
    case ADMIN_DATOS_SELECT_PERIOD: {
      return { ...state, selectedPeriod: action.payload, selectedRecord: null }
    }
    case ADMIN_DATOS_SELECT_RECORD: {
      return { ...state, selectedRecord: action.payload }
    }
    case ADMIN_DATOS_START: {
      return { ...state, loading: true, error: null }
    }
    case ADMIN_DATOS_SUCCESS: {
      return { ...state, records: action.payload, loading: false, error: null }
    }
    case ADMIN_DATOS_FAIL: {
      return { ...state, loading: false, error: action.payload }
    }
    case ADMIN_DATOS_SELECT_CSV: {
      return { ...state, selectedFile: action.payload, csvButtonLabel: action.payload.name }
    }
    case ADMIN_DATOS_UPLOAD_CSV_START: {
      return { ...state, loading: true }
    }
    case ADMIN_DATOS_UPLOAD_CSV_FAIL:
    case ADMIN_DATOS_UPLOAD_CSV_SUCCESS: {
      return { ...state, loading: false, selectedFile: null, csvButtonLabel: INITIAL_STATE.csvButtonLabel }
    }
    default: {
      return state;
    }
  }
}