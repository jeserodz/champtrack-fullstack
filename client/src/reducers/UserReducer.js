import { 
  USER_FETCH_START,
  USER_FETCH_FAIL,
  USER_FETCH_SUCCESS,
  USER_IMAGE_UPLOAD_PROGRESS,
  USER_IMAGE_UPLOAD_SUCCESS,
} from '../actions/UserActions';

const INITIAL_STATE = { 
  profile: null,
  photoUploadProgress: null,
  loading: false,
  error: null
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {

    case USER_FETCH_START:
      return { ...state, loading: true, error: null }

    case USER_FETCH_FAIL:
      return { ...state, loading: false, error: action.payload }

    case USER_FETCH_SUCCESS: {
      return { ...state, loading: false, error: null, profile: action.payload }
    }

    case USER_IMAGE_UPLOAD_PROGRESS:
      return { ...state, photoUploadProgress: action.payload }

    case USER_IMAGE_UPLOAD_SUCCESS:
      return { ...state, photoUploadProgress: null }

    default:
      return state;
  }
}