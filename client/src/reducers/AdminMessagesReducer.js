import * as Actions from '../actions/AdminMessagesActions';

const INITIAL_STATE = {
  text: '',
  users: [],
  businessUnits: [],
  countries: [],
  rankings: [],
  businessUnitSelected: false,
  countrySelected: false,
  rankingSelected: false,
  loading: false,
  error: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    
    case Actions.ADMIN_MESSAGES_FETCH_START:
    case Actions.ADMIN_MESSAGES_CREATE_START: 
    case Actions.ADMIN_MESSAGESS_SEND_START: 
    case Actions.ADMIN_MESSAGES_FETCH_USERS_START: {
      return {
        ...state,
        loading: true
      }
    }

    case Actions.ADMIN_MESSAGES_FETCH_FAIL:
    case Actions.ADMIN_MESSAGES_CREATE_FAIL: 
    case Actions.ADMIN_MESSAGESS_SEND_FAIL: 
    case Actions.ADMIN_MESSAGES_FETCH_USERS_FAIL: {
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    }

    case Actions.ADMIN_MESSAGES_FETCH_SUCCESS:
    case Actions.ADMIN_MESSAGES_CREATE_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: null,
        list: action.payload
      }
    }

    case Actions.ADMIN_MESSAGES_FETCH_USERS_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: null,
        users: action.payload.users,
        businessUnits: action.payload.businessUnits,
        countries: action.payload.countries,
        rankings: action.payload.rankings,
      }
    }

    case Actions.ADMIN_MESSAGESS_TEXT_CHANGE: {
      return {
        ...state,
        text: action.payload
      }
    }

    case Actions.ADMIN_MESSAGES_TOGGLE_SELECT_USER: {
      const newState = { ...state };
      newState.users.forEach(user => {
        if (user.id === action.payload.id) {
          user.selected = user.selected ? false : true;
        }
      });
      return { ...newState }
    }

    case Actions.ADMIN_MESSAGES_SELECT_ENTITY: {
      return {
        ...state,
        [action.payload.entityName + 'Selected']: action.payload.id
      }
    }

    case Actions.ADMIN_MESSAGESS_SELECT_ALL: {
      const { rankingId, countryId, businessUnitId } = action.payload;
      const newState = { ...state };
      newState.users.forEach(user => {
        let include = true;
        if (rankingId && rankingId !== user.rankingId) {
          include = false;
        }
        if (countryId && countryId !== user.countryId) {
          include = false;
        }
        if (businessUnitId && businessUnitId !== user.businessUnitId) {
          include = false;
        }
        if (include) {
          user.selected = true;
        }
      });
      return { ...newState }
    }

    case Actions.ADMIN_MESSAGESS_UNSELECT_ALL: {
      const { rankingId, countryId, businessUnitId } = action.payload;
      const newState = { ...state };
      newState.users.forEach(user => {
        let include = true;
        if (rankingId && rankingId !== user.rankingId) {
          include = false;
        }
        if (countryId && countryId !== user.countryId) {
          include = false;
        }
        if (businessUnitId && businessUnitId !== user.businessUnitId) {
          include = false;
        }
        if (include) {
          user.selected = false;
        }
      });
      return { ...newState }
    }

    case Actions.ADMIN_MESSAGESS_SEND_SUCCESS: {
      const newState = { ...state };
      newState.users.forEach(user => user.selected = false);
      return {
        ...newState,
        text: '',
        businessUnitSelected: false,
        countrySelected: false,
        rankingSelected: false,
        loading: false,
        error: null
      }
    }

    default:
      return state;
  }
}