import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import UserReducer from './UserReducer';
import RankingsReducer from './RankingsReducer';
import PeriodsReducer from './PeriodsReducer';
import RecordsReducer from './RecordsReducer';
import VariablesReducer from './VariablesReducer';
import UXReducer from './UXReducer';
import MessagesReducer from './MessagesReducer';
import SurveysReducer from './SurveysReducer';
import CountriesReducer from './CountriesReducer';
import PostsReducer from './PostsReducer';

import AdminRankingsReducer from './AdminRankingsReducer';
import AdminDatosReducer from './AdminDatosReducer';
import AdminSurveysReducer from './AdminSurveysReducer';
import AdminCountriesReducer from './AdminCountriesReducer';
import AdminRolesReducer from './AdminRolesReducer';
import AdminBusinessUnitsReducer from './AdminBusinessUnitsReducer';
import AdminLinesReducer from './AdminLinesReducer';
import AdminUsersReducer from './AdminUsersReducer';
import AdminPostsReducer from './AdminPostsReducer';
import AdminMessagesReducer from './AdminMessagesReducer';

export default combineReducers({
  auth: AuthReducer,
  user: UserReducer,
  rankings: RankingsReducer,
  periods: PeriodsReducer,
  records: RecordsReducer,
  variables: VariablesReducer,
  ux: UXReducer,
  messages: MessagesReducer,
  surveys: SurveysReducer,
  countries: CountriesReducer,
  posts: PostsReducer,
  adminRankings: AdminRankingsReducer,
  adminDatos: AdminDatosReducer,
  adminSurveys: AdminSurveysReducer,
  adminCountries: AdminCountriesReducer,
  adminRoles: AdminRolesReducer,
  adminBusinessUnits: AdminBusinessUnitsReducer,
  adminLines: AdminLinesReducer,
  adminUsers: AdminUsersReducer,
  adminPosts: AdminPostsReducer,
  adminMessages: AdminMessagesReducer
});