import api from '../api';
import * as Actions from '../actions/AdminRolesActions';

const INTIAL_STATE = {
  roles: [],
  rankings: [],
  selectedRole: null,
  loading: false,
  error: null
}

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case Actions.ADMIN_ROLE_FETCH_START:
    case Actions.ADMIN_ROLE_CREATE_START:
    case Actions.ADMIN_ROLE_UPDATE_START: {
      return { ...state, loading: true, error: null }
    }
    case Actions.ADMIN_ROLE_FETCH_FAIL:
    case Actions.ADMIN_ROLE_CREATE_FAIL:
    case Actions.ADMIN_ROLE_UPDATE_FAIL: {
      return { ...state, loading: false, error: action.payload }
    }
    case Actions.ADMIN_ROLE_FETCH_SUCCESS: {
      return { ...state, loading: false, error: null, roles: action.payload }
    }
    case Actions.ADMIN_ROLE_SELECT_BU: {
      return{ ...state, selectedRole: action.payload }
    }
    case Actions.ADMIN_ROLE_FETCH_RANKINGS: {
      return { ...state, rankings: action.payload };
    }
    default: {
      return state;
    }
  }
}