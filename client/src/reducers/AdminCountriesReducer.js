import {
  ADMIN_COUNTRIES_FETCH_START,
  ADMIN_COUNTRIES_FETCH_FAIL,
  ADMIN_COUNTRIES_FETCH_SUCCESS,
  ADMIN_COUNTRIES_CREATE_START,
  ADMIN_COUNTRIES_CREATE_FAIL,
  ADMIN_COUNTRIES_UPDATE_START,
  ADMIN_COUNTRIES_UPDATE_FAIL,
  ADMIN_COUNTRIES_UPLOAD_IMAGE_PROGRESS,
  ADMIN_COUNTRIES_UPLOAD_IMAGE_SUCCESS,
  ADMIN_COUNTRIES_SELECT_COUNTRY
} from '../actions/AdminCountriesActions';

const INTIAL_STATE = {
  countries: [],
  selectedCountry: null,
  uploadProgress: null,
  loading: false,
  error: null
}

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {
    case ADMIN_COUNTRIES_FETCH_START:
    case ADMIN_COUNTRIES_CREATE_START:
    case ADMIN_COUNTRIES_UPDATE_START: {
      return { ...state, loading: true, error: null }
    }
    case ADMIN_COUNTRIES_FETCH_FAIL:
    case ADMIN_COUNTRIES_CREATE_FAIL:
    case ADMIN_COUNTRIES_UPDATE_FAIL: {
      return { ...state, loading: false, error: action.payload }
    }
    case ADMIN_COUNTRIES_FETCH_SUCCESS: {
      return { ...state, loading: false, error: null, countries: action.payload }
    }
    case ADMIN_COUNTRIES_UPLOAD_IMAGE_PROGRESS: {
      return { ...state, loading: true, uploadProgress: action.payload }
    }
    case ADMIN_COUNTRIES_UPLOAD_IMAGE_SUCCESS: {
      return { ...state, loading: false, uploadProgress: null }
    }
    case ADMIN_COUNTRIES_SELECT_COUNTRY: {
      return{ ...state, selectedCountry: action.payload }
    }
    default: {
      return state;
    }
  }
}