import { ROUTE_CHANGE } from '../actions';

const INITIAL_STATE = {
  currentRoute: 'Login'
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ROUTE_CHANGE: {
      return { ...state, currentRoute: action.payload }
    }
    default:
      return state;
  }
}