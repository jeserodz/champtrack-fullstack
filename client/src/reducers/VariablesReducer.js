import { VARIABLES_FETCHED, VARIABLE_ADDED, VARIABLE_CHANGED, VARIABLE_REMOVED, VARIABLE_SELECTED } from '../actions';

const INITAL_STATE = { list: {}, selected: null }

export default (state = INITAL_STATE, action) => {
  switch(action.type) {

    case VARIABLES_FETCHED:  {
      return { ...state, list: action.payload }  
    }

    case VARIABLE_ADDED: {
      const list = { ...state.list, [action.payload.key]: action.payload.val };
      return { ...state, list }
    }

    case VARIABLE_CHANGED: {
      const list = { ...state.list, [action.payload.key]: action.payload.val };
      return { ...state, list }
    }

    case VARIABLE_REMOVED: {
      const list = Object.create(state.list);
      delete list[action.payload.key];
      return { ...state, list }
    }
    
    case VARIABLE_SELECTED: {
      return { ...state, selected: action.payload }
    }

    default: 
      return state;
  }
}