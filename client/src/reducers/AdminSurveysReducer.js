import _ from 'lodash';
import * as Actions from '../actions/AdminSurveysActions';

const INITIAL_STATE = {
  surveys: [],
  selectedSurvey: null,
  loading: false,
  error: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.ADMIN_SURVEYS_FETCH_START:
    case Actions.ADMIN_SURVEYS_CREATE_START:
    case Actions.ADMIN_SURVEYS_UPDATE_START: {
      return { ...state, loading: true, error: false }
    }
    case Actions.ADMIN_SURVEYS_FETCH_FAIL:
    case Actions.ADMIN_SURVEYS_CREATE_FAIL:
    case Actions.ADMIN_SURVEYS_HIDE_FAIL:
    case Actions.ADMIN_SURVEYS_UPDATE_FAIL: {
      return { ...state, loading: false, error: action.payload }
    }
    case Actions.ADMIN_SURVEYS_FETCH_SUCCESS: {
      return { ...state, loading: false, error: null, surveys: action.payload }
    }
    case Actions.ADMIN_SURVEYS_CREATE_SUCCESS: {
      return { ...state, loading: false, error: null }
    }
    case Actions.ADMIN_SURVEYS_UPDATE_SUCCESS: {
      return { ...state, loading: false, error: null }
    }
    case Actions.ADMIN_SURVEYS_SELECT_SURVEY: {
      return { ...state, selectedSurvey: action.payload }
    }
    case Actions.ADMIN_SURVEYS_HIDE_SUCCESS: {
      const newState = _.cloneDeep(state);
      newState.surveys = newState.surveys.filter(s => s.id !== action.payload);
      return { ...newState, loading: false, error: null }
    }
    default: {
      return state;
    }
  }
}