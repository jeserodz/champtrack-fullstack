import _ from 'lodash';
import * as Types from '../actions/RankingsActions';

const INITIAL_STATE = {
  rankings: [],
  records: [],
  accumulatedRecords: [],
  accumulatedRecordsYear: null,
  roles: [],
  countries: [],
  businessUnits: [],
  selectedRanking: null,
  selectedPeriod: null,
  selectedRecord: null,
  selectedBUs: [],
  selectedCountries: [],
  viewTop10: true,
  loading: false,
  accumulatedLoading: false,
  error: null
};


export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case Types.RANKINGS_FETCH_START:
    case Types.RANKINGS_FETCH_RECORDS_START:
    case Types.RANKINGS_LIKE_RECORD_START: {
      return { ...state, loading: true, error: null }
    }
    case Types.RANKINGS_FETCH_ACCUMULATED_RECORDS_START: {
      return { ...state, loading: true, accumulatedLoading: true, error: null }
    }
    case Types.RANKINGS_FETCH_FAIL:
    case Types.RANKINGS_FETCH_ACCUMULATED_RECORDS_FAIL:
    case Types.RANKINGS_FETCH_RECORDS_FAIL:
    case Types.RANKINGS_LIKE_RECORD_FAIL: {
      return { ...state, loading: false, error: action.payload }
    }
    case Types.RANKINGS_FETCH_SUCCESS: {
      return { ...state, loading: false, error: null, rankings: action.payload }
    }
    case Types.RANKINGS_FETCH_ROLES: {
      return { ...state, roles: action.payload };
    }
    case Types.RANKINGS_SELECT_RANKING: {
      return { ...state, selectedRanking: action.payload, selectedPeriod: null, records: [] }
    }
    case Types.RANKINGS_SELECT_PERIOD: {
      return { ...state, selectedPeriod: action.payload, records: [] }
    }
    case Types.RANKINGS_FETCH_ACCUMULATED_RECORDS_SUCCESS: {
      return {
        ...state,
        loading: false,
        accumulatedLoading: false,
        error: null,
        accumulatedRecords: action.payload.accumulatedRecords,
        accumulatedRecordsYear: action.payload.year || null,
        countries: action.payload.countries || state.countries,
        businessUnits: action.payload.businessUnits || state.businessUnits,
      }
    }
    case Types.RANKINGS_FETCH_RECORDS_SUCCESS: {
      return { ...state, loading: false, error: null, records: action.payload }
    }
    case Types.RANKINGS_LIKE_RECORD_SUCCESS: {
      const newState = _.clone(state);
      newState.records.forEach(record => {
        if (record.id === action.payload.id) {
          record.likes = action.payload.likes;
        }
      });
      newState.accumulatedRecords.forEach(record => {
        if (record.id === action.payload.id) {
          record.likes = action.payload.likes;
        }
      });
      return { ...newState, loading: false, error: null }
    }
    case Types.RANKINGS_SELECT_RECORD: {
      return { ...state, selectedRecord: action.payload }
    }
    case Types.RANKINGS_SELECT_BU: {
      const selectedBUs = _.union(state.selectedBUs, [ action.payload ]);
      return { ...state, selectedBUs }
    }
    case Types.RANKINGS_SELECT_COUNTRY: {
      const selectedCountries = _.union(state.selectedCountries, [ action.payload ]);
      return { ...state, selectedCountries }
    }
    case Types.RANKINGS_UNSELECT_BU: {
      const selectedBUs = _.filter(state.selectedBUs, buId => buId !== action.payload);
      return { ...state, selectedBUs }
    }
    case Types.RANKINGS_UNSELECT_COUNTRY: {
      const selectedCountries = _.filter(state.selectedCountries, countryId => countryId !== action.payload);
      return { ...state, selectedCountries }
    }
    case Types.RANKINGS_VIEW_TOP10: {
      return { ...state, viewTop10: true }
    }
    case Types.RANKINGS_VIEW_REST: {
      return { ...state, viewTop10: false }
    }
    case Types.RANKINGS_RESET_SELECTIONS: {
      return {
        ...state,
        selectedPeriod: null,
        selectedRecord: null,
        selectedBUs: [],
        selectedCountries: [],
        viewTop10: true
      }
    }
    default:
      return state;
  }
}
