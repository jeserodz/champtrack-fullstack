import { 
  SURVEYS_FETCH_START, 
  SURVEYS_FETCH_FAIL, 
  SURVEYS_FETCH_SUCCESS, 
  SURVEYS_SELECT_SURVEY,
  SURVEYS_SELECT_OPTION,
  SURVEYS_CONFIRM_ANSWER
} from '../actions/SurveysActions';

const INITIAL_STATE = {
  suverys: [],
  selectedSurvey: null,
  selectedOption: null,
  loading: false,
  error: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SURVEYS_FETCH_START: 
    case SURVEYS_CONFIRM_ANSWER: {
      return { ...state, loading: true, error: null, selectedSurvey: null, selectedOption: null }
    }
    case SURVEYS_FETCH_FAIL: {
      return { ...state, loading: false, error: action.payload }
    }
    case SURVEYS_FETCH_SUCCESS: {
      return { ...state, loading: false, error: null, surveys: action.payload }
    }
    case SURVEYS_SELECT_SURVEY: {
      return { ...state, selectedSurvey: action.payload, selectedOption: null }
    }
    case SURVEYS_SELECT_OPTION: {
      return { ...state, selectedOption: action.payload }
    }
    default: {
      return state;
    }
  }
}