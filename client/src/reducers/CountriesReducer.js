import { COUNTRIES_FETCHED, COUNTRY_ADDED, COUNTRY_CHANGED, COUNTRY_REMOVED, COUNTRY_SELECTED, COUNTRY_IMAGE_UPLOAD_PROGRESS, COUNTRY_IMAGE_UPLOAD_SUCCESS } from '../actions';

const INITAL_STATE = { list: {}, selected: null, photoUploadProgress: null }

export default (state = INITAL_STATE, action) => {
  switch(action.type) {

    case COUNTRIES_FETCHED:  {
      return { ...state, list: action.payload }  
    }

    case COUNTRY_ADDED: {
      const list = { ...state.list, [action.payload.key]: action.payload.val };
      return { ...state, list }
    }

    case COUNTRY_CHANGED: {
      const list = { ...state.list, [action.payload.key]: action.payload.val };
      return { ...state, list }
    }

    case COUNTRY_REMOVED: {
      const list = Object.create(state.list);
      delete list[action.payload.key];
      return { ...state, list }
    }
    
    case COUNTRY_SELECTED: {
      return { ...state, selected: action.payload }
    }

    case COUNTRY_IMAGE_UPLOAD_PROGRESS:
      return { ...state, photoUploadProgress: action.payload }

    case COUNTRY_IMAGE_UPLOAD_SUCCESS:
      return { ...state, photoUploadProgress: null }

    default: 
      return state;
  }
}