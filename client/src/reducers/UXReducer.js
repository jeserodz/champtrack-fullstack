import { 
  OPEN_DRAWER,
  CLOSE_DRAWER,
  OPEN_VARIABLE_DIALOG,
  CLOSE_VARIABLE_DIALOG,
  OPEN_PERIOD_DIALOG,
  CLOSE_PERIOD_DIALOG,
  OPEN_RECORD_DIALOG,
  CLOSE_RECORD_DIALOG,
  OPEN_TRIVIA_DIALOG,
  CLOSE_TRIVIA_DIALOG,
  OPEN_SURVEY_DIALOG,
  CLOSE_SURVEY_DIALOG,
  OPEN_TRIVIA_RESULTS_DIALOG,
  CLOSE_TRIVIA_RESULTS_DIALOG,
  OPEN_SELECT_RANKING_DIALOG,
  CLOSE_SELECT_RANKING_DIALOG,
  OPEN_SELECT_BU_DIALOG,
  CLOSE_SELECT_BU_DIALOG,
  OPEN_SELECT_COUNTRY_DIALOG,
  CLOSE_SELECT_COUNTRY_DIALOG,
  OPEN_SELECT_RANKING_PERIOD_DIALOG,
  CLOSE_SELECT_RANKING_PERIOD_DIALOG,
  SELECT_RANKING_PERIOD_RECORD,
  OPEN_CHANGE_PASSWORD_DIALOG,
  CLOSE_CHANGE_PASSWORD_DIALOG,
  OPEN_DOWNLOAD_EXCEL_DIALOG,
  CLOSE_DOWNLOAD_EXCEL_DIALOG,
  OPEN_ASSIGN_ROLES_DIALOG,
  CLOSE_ASSIGN_ROLES_DIALOG,
  SHOW_SNACKBAR,
  HIDE_SNACKBAR,
} from '../actions';

const INITAL_STATE = { 
  drawer: false,
  variableDialog: false,
  periodDialog: false,
  recordDialog: false,
  triviaDialog: false,
  surveyDialog: false,
  surveyResultsDialog: false,
  selectRankingDialog: false,
  selectBUDialog: false,
  selectCountryDialog: false,
  selectRankingPeriodDialog: false,
  selectedRankingPeriodRecord: null,
  changePasswordDialog: false,
  downloadExcelDialog: false,
  downloadExcelURL: null,
  assignRolesDialog: false,
  snackbarOpen: false,
  snackbarMessage: '',
};

export default (state = INITAL_STATE, action) => {
  switch (action.type) {
    case OPEN_DRAWER:
      return { ...state, drawer: true }
    case CLOSE_DRAWER:
      return { ...state, drawer: false }
    case OPEN_VARIABLE_DIALOG:
      return { ...state, variableDialog: true }
    case CLOSE_VARIABLE_DIALOG:
      return { ...state, variableDialog: false }
    case OPEN_PERIOD_DIALOG:
      return { ...state, periodDialog: true }
    case CLOSE_PERIOD_DIALOG:
      return { ...state, periodDialog: false }
    case OPEN_RECORD_DIALOG:
      return { ...state, recordDialog: true }
    case CLOSE_RECORD_DIALOG:
      return { ...state, recordDialog: false }
    case OPEN_TRIVIA_DIALOG:
      return { ...state, triviaDialog: true }
    case CLOSE_TRIVIA_DIALOG:
      return { ...state, triviaDialog: false }
    case OPEN_SURVEY_DIALOG:
      return { ...state, surveyDialog: true }
    case CLOSE_SURVEY_DIALOG:
      return { ...state, surveyDialog: false }
    case OPEN_TRIVIA_RESULTS_DIALOG:
      return { ...state, surveyResultsDialog: true }
    case CLOSE_TRIVIA_RESULTS_DIALOG:
      return { ...state, surveyResultsDialog: false }
    case OPEN_SELECT_RANKING_DIALOG:
      return { ...state, selectRankingDialog: true }
    case CLOSE_SELECT_RANKING_DIALOG:
      return { ...state, selectRankingDialog: false }
    case OPEN_SELECT_BU_DIALOG:
      return { ...state, selectBUDialog: true }
    case CLOSE_SELECT_BU_DIALOG:
      return { ...state, selectBUDialog: false }
    case OPEN_SELECT_COUNTRY_DIALOG:
      return { ...state, selectCountryDialog: true }
    case CLOSE_SELECT_COUNTRY_DIALOG:
      return { ...state, selectCountryDialog: false }
    case OPEN_SELECT_RANKING_PERIOD_DIALOG:
      return { ...state, selectRankingPeriodDialog: true }
    case CLOSE_SELECT_RANKING_PERIOD_DIALOG:
      return { ...state, selectRankingPeriodDialog: false }
    case OPEN_CHANGE_PASSWORD_DIALOG:
      return { ...state, changePasswordDialog: true }
    case CLOSE_CHANGE_PASSWORD_DIALOG:
      return { ...state, changePasswordDialog: false }
    case OPEN_DOWNLOAD_EXCEL_DIALOG:
      return { ...state, downloadExcelDialog: true, downloadExcelURL: action.payload }
    case CLOSE_DOWNLOAD_EXCEL_DIALOG:
      return { ...state, downloadExcelDialog: false, downloadExcelURL: null }
    case OPEN_ASSIGN_ROLES_DIALOG:
      return { ...state, assignRolesDialog: true }
    case CLOSE_ASSIGN_ROLES_DIALOG: 
      return { ...state, assignRolesDialog: false }
    case SELECT_RANKING_PERIOD_RECORD:
      return { ...state, selectedRankingPeriodRecord: action.payload }
    case SHOW_SNACKBAR:
      return { ...state, snackbarOpen: true, snackbarMessage: action.payload }
    case HIDE_SNACKBAR:
      return { ...state, snackbarOpen: false, snackbarMessage: '' }
    default:
      return state;
  }
}