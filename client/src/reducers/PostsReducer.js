import * as Actions from '../actions/PostsActions';
import _ from 'lodash';

const INITIAL_STATE = {
  list: [],
  selectedPost: null,
  loading: false,
  error: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Actions.POSTS_FETCH_START: 
    case Actions.POSTS_LIKE_START: {
      return { 
        ...state,
        loading: true,
      }
    }

    case Actions.POSTS_FETCH_FAIL: 
    case Actions.POSTS_LIKE_FAIL: {
      return { 
        ...state,
        loading: false,
        error: action.payload
      }
    }

    case Actions.POSTS_FETCH_SUCCESS: {
      return { 
        ...state,
        list: action.payload,
        loading: false,
        error: null,
      }
    }

    case Actions.POSTS_SELECT_POST: {
      return { 
        ...state,
        selectedPost: action.payload,
      }
    }

    case Actions.POSTS_LIKE_SUCCESS: {
      const newState = _.clone(state);
      newState.list.forEach(post => {
        if (post.id === action.payload.id) {
          post.likes = action.payload.likes;
        }
      });
      return {
        ...newState,
        loading: false,
        error: null
      }
    }

    default:
      return state;
  }
}