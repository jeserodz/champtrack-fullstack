import { colors } from 'material-ui/styles';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: colors.red900,
    accent1Color: colors.orange500
  }
});

export default muiTheme;