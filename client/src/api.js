import axios from 'axios';

export const baseURL = '/api';

const api = axios.create({ baseURL });

api.interceptors.request.use(function (config) {
  config.headers.access_token = localStorage.getItem('access_token');
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

export default api;