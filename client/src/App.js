import React, { Component } from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import reducer from './reducers';
import { MuiThemeProvider } from 'material-ui';
import muiTheme from './muiTheme';
import Connector from './Connector';
import './App.css';
import './animate.css';

injectTapEventPlugin();

class App extends Component {

  render() {
    return (
      <Provider id="reduxProvider" store={createStore(
        reducer, 
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), 
        applyMiddleware(ReduxThunk)
      )}>
        <MuiThemeProvider id="themeProvider" muiTheme={muiTheme}>
          <Connector />
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
