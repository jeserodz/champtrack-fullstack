import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

// Safari, in Private Browsing Mode, looks like it supports localStorage but all calls to setItem
// throw QuotaExceededError. We're going to detect this and show an alert.
if (typeof localStorage === 'object') {
  try {
      localStorage.setItem('localStorage', 1);
      localStorage.removeItem('localStorage');
  } catch (e) {
      alert('Su navegador no soporta guardar datos localmente. En dispositivos Apple usando Safari, la causa más común es que está usando el navegador en "Modo Privado". Esto impedirá que pueda acceder a la applicación.');
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
