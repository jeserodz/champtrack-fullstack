import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { select, create, update } from '../actions/AdminRankingsActions';
import { openVariableDialog } from '../actions/UXActions';
import { Toolbar, ToolbarGroup, FlatButton, List, ListItem, Avatar } from 'material-ui';
import { ContentAdd, ImageBlurOn } from 'material-ui/svg-icons';
import { colors } from 'material-ui/styles';
import EditVariableDialog from './EditVariableDialog';
import { StyleSheet, css } from 'aphrodite';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);

class RankingVariables extends Component {

  onSelectVariable(id) {
    this.props.select('variable', id);
    this.props.openVariableDialog();
  }

  renderVariables() {
    const { rankings, selectedRanking } = this.props.adminRankings;
    if (!selectedRanking) return null;
    const ranking = _.find(rankings, { id: selectedRanking });
    return _.map(ranking.variables, (variable, index) => {
      return (
        <ListItem key={index}
          primaryText={variable.name}
          secondaryText={`Peso: ${variable.weight}%`}
          leftAvatar={<Avatar icon={<ImageBlurOn />} backgroundColor={colors.green500} />}
          onTouchTap={() => this.onSelectVariable(variable.id)}
        />
      );
    });
  }

  render() {
    const { selectedRanking } = this.props.adminRankings;
    return (
      <div>
        <Toolbar>
          <ToolbarGroup className={css(styles.c1)}>
            <FlatButton 
              label="Agregar Variables" 
              primary={true} 
              icon={<ContentAdd />} 
              disabled={ selectedRanking ? false : true }
              onTouchTap={() => this.props.openVariableDialog()}
            />
          </ToolbarGroup>
        </Toolbar>
        <List>
          { this.renderVariables() }
        </List>
        <EditVariableDialog />
      </div>
    );
  }
}

const styles = StyleSheet.create({
  c1: { display: 'flex', alignItems: 'flex-end', flex: 1 }
});

function mapStateToProps({ adminRankings }) {
  return { adminRankings };
}
 
export default connect(mapStateToProps, { select, create, update, openVariableDialog })(RankingVariables);