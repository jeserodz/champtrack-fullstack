import _ from "lodash";
import moment from 'moment';
import React, { Component } from "react";
import { connect } from "react-redux";
import { select } from "../actions/AdminRankingsActions";
import { generateCsvTemplate } from "../actions/AdminDatosActions";
import { openPeriodDialog } from "../actions/UXActions";
import { Toolbar, ToolbarGroup, FlatButton, List, ListItem, Avatar, IconMenu, MenuItem, IconButton } from "material-ui";
import { ContentAdd, ActionDateRange, NavigationMoreVert } from "material-ui/svg-icons";
import { colors } from "material-ui/styles";
import EditPeriodDialog from "./EditPeriodDialog";
import { StyleSheet, css } from "aphrodite";
import postCss from "postcss-js";
const prefixer = postCss.sync([require("autoprefixer")]);

class RankingPeriods extends Component {
  onSelectPeriod(id) {
    this.props.select("period", id);
    this.props.openPeriodDialog();
  }

  renderIconButtonElement() {
    return (
      <IconButton touch={true} tooltip="Descargar o subir datos..." tooltipPosition="bottom-left">
        <NavigationMoreVert color={colors.grey400} />
      </IconButton>
    );
  }

  renderRightIconMenu(id) {
    const { selectedRanking } = this.props.adminRankings;
    return (
      <IconMenu iconButtonElement={this.renderIconButtonElement()}>
        <MenuItem onTouchTap={() => this.props.generateCsvTemplate(selectedRanking, id)}>
          Descargar plantilla Excel
        </MenuItem>
        {/* <MenuItem onTouchTap={() => null}>Subir datos desde plantilla Excel</MenuItem> */}
      </IconMenu>
    );
  }

  renderPeriods() {
    const { rankings, selectedRanking } = this.props.adminRankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return null;

    const years = _.groupBy(ranking.periods, period => moment(period.endDate).year());

    return _.map(years, (yearPeriods, year) => (
      <ListItem
        key={year}
        primaryText={year}
        onTouchTap={() => this.onPeriodSelect(year)}
        nestedItems={yearPeriods.map((period, index) => (
          <ListItem
            key={index}
            primaryText={period.name}
            secondaryText={`
              Desde ${new Date(period.startDate).toLocaleDateString()} 
              hasta ${new Date(period.endDate).toLocaleDateString()}`}
            leftAvatar={
              <Avatar
                icon={<ActionDateRange />}
                backgroundColor={colors.indigo600}
                onTouchTap={() => this.onSelectPeriod(period.id)}
              />
            }
            rightIconButton={this.renderRightIconMenu(period.id)}
          />
        ))}
      />
    ))
  }

  render() {
    const { selectedRanking } = this.props.adminRankings;
    return (
      <div>
        <Toolbar>
          <ToolbarGroup className={css(styles.c1)}>
            <FlatButton
              label="Agregar Período"
              primary={true}
              icon={<ContentAdd />}
              disabled={selectedRanking ? false : true}
              onTouchTap={this.props.openPeriodDialog}
            />
          </ToolbarGroup>
        </Toolbar>
        <List>{this.renderPeriods()}</List>
        <EditPeriodDialog />
      </div>
    );
  }
}

const styles = StyleSheet.create({
  c1: { display: "flex", alignItems: "flex-end", flex: 1 }
});

function mapStateToProps({ adminRankings }) {
  return { adminRankings };
}

export default connect(mapStateToProps, { select, generateCsvTemplate, openPeriodDialog })(RankingPeriods);
