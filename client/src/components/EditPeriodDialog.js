import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { select, create, update } from '../actions/AdminRankingsActions';
import { closePeriodDialog } from '../actions/UXActions';
import { Dialog, TextField, DatePicker, FlatButton } from 'material-ui';
 
class EditPeriodDialog extends Component {

  INITIAL_STATE = { 
    id: null,
    rankingId: null, 
    name: '', 
    startDate: new Date(), 
    endDate: new Date() 
  }

  state = { ...this.INITIAL_STATE }

  componentWillReceiveProps(nextProps) {
    const { rankings, selectedRanking, selectedPeriod } = nextProps.adminRankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    const period = ranking ? _.find(ranking.periods, { id : selectedPeriod }) : null;
    this.setState(period ? { ...period } : { 
      rankingId: selectedRanking,
      name: '',
      startDate: new Date(),
      endDate: new Date()
    });
  }

  onClose() {
    this.props.select('period', null);
    this.props.closePeriodDialog();
    this.setState(this.INITIAL_STATE);
  }

  onConfirm() {
    const { 
      adminRankings: { selectedPeriod }, 
      create, 
      update, 
      select, 
      closePeriodDialog 
    } = this.props;

    selectedPeriod ? update('period', this.state) : create('period', this.state);
    select('period', null);
    closePeriodDialog();
    this.setState(this.INITIAL_STATE);
  }

  onInputChange(field, value) {
    this.setState({ ...this.state, [field]: value });
  }
 
  actions = [
    <FlatButton 
      label="Cerrar" 
      onTouchTap={this.onClose.bind(this)}
    />,
    <FlatButton 
      label="Confirmar"
      primary
      onTouchTap={this.onConfirm.bind(this)}
    />
  ]

  renderDialogTitle() {
    const { selectedPeriod } = this.props.adminRankings;
    return selectedPeriod ? "Editar Período" : "Crear Período";
  }
 
  render() {
    return (
      <Dialog
        title={this.renderDialogTitle()}
        open={this.props.ux.periodDialog}
        actions={this.actions}
      >
        <TextField
          floatingLabelText="Nombre"
          value={this.state.name}
          onChange={(e) => this.onInputChange('name', e.target.value)}
        />
        <DatePicker
          floatingLabelText="Fecha Inicio"
          defaultDate={new Date(this.state.startDate)}
          onChange={(e, date) => this.onInputChange('startDate', date)}
        />
        <DatePicker
          floatingLabelText="Fecha Fin"
          defaultDate={new Date(this.state.endDate)}
          onChange={(e, date) => this.onInputChange('endDate', date)}
        />
      </Dialog>
    );
  }
}

function mapStateToProps({ adminRankings, ux }) {
  return { adminRankings, ux }
}
 
export default connect(mapStateToProps, { select, create, update, closePeriodDialog })(EditPeriodDialog);