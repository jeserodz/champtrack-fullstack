import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { select } from '../actions/RankingsActions';
import { closeSelectRankingDialog } from '../actions/UXActions';
import { Dialog, List, ListItem, Divider, FlatButton } from 'material-ui';
import * as Utils from '../utils';
 
class SelectRankingDialog extends Component {

  onRankingSelect(id) {
    this.props.select('ranking', id);
    this.props.closeSelectRankingDialog();
  }

  canSeeRanking(user, roles, ranking) {
    if (Utils.isAdmin(user)) return true;
    let rankingFound = false;
    user.roles.forEach(userRole => {
      const roleData = _.find(roles, { id: userRole.id });
      if (roleData && _.find(roleData.rankings, { id: ranking.id })) {
        rankingFound = true;
      }
    });
    return rankingFound;
  }

  renderRankings() {
    const { rankings, auth } = this.props;
    return _.map(rankings.rankings, (ranking, index) => {
      if (!this.canSeeRanking(auth.user, rankings.roles, ranking)) return null;
      return (
        <span key={index}>
          <ListItem 
            primaryText={ranking.name} 
            onTouchTap={() => this.onRankingSelect(ranking.id)} 
          />
        </span>
      )
    })
  }

  render() {
    return (
      <Dialog 
        open={this.props.ux.selectRankingDialog}
        title="Seleccione un ranking"
        titleStyle={{ padding: '24px 16px' }}
        bodyStyle={{ padding: 0 }}
        autoScrollBodyContent={true}
        actions={[ 
          <FlatButton 
            primary 
            label="Cerrar" 
            onTouchTap={this.props.closeSelectRankingDialog} 
          /> 
        ]}
      >
        <List style={{ padding: 0 }}>
          { this.renderRankings() }
        </List>
      </Dialog>
    );
  }
}

const mapStateToProps = ({ ux, rankings, auth }) => ({ ux, rankings, auth });

export default connect(mapStateToProps, { select, closeSelectRankingDialog })(SelectRankingDialog);