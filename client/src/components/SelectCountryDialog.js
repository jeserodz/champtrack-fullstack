import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { select, unselect } from '../actions/RankingsActions';
import { closeSelectCountryDialog } from '../actions/UXActions';
import { Dialog, List, ListItem, Divider, FlatButton, Checkbox } from 'material-ui';
 
class SelectCountryDialog extends Component {

  onBUSelect(id, isChecked) {
    const { select, unselect } = this.props;
    isChecked ? select('country', id) : unselect('country', id);
  }

  renderCountries() {
    const { countries, selectedCountries } =  this.props.rankings;
    return _.map(countries, (country, index) => {
      return (
        <span key={index}>
          <ListItem 
            primaryText={country.name} 
            leftCheckbox={<Checkbox 
              checked={_.includes(selectedCountries, country.id)}
              onCheck={(e,isChecked) => this.onBUSelect(country.id, isChecked)} 
            />}
          />
        </span>
      )
    })
  }

  render() {
    return (
      <Dialog 
        open={this.props.ux.selectCountryDialog}
        title="Filtre por Países"
        titleStyle={{ padding: '24px 16px' }}
        bodyStyle={{ padding: 0 }}
        autoScrollBodyContent={true}
        actions={[ 
          <FlatButton 
            primary 
            label="Cerrar" 
            onTouchTap={this.props.closeSelectCountryDialog} 
          /> 
        ]}
      >
        <List style={{ padding: 0 }}>
          { this.renderCountries() }
        </List>
      </Dialog>
    );
  }
}

const mapStateToProps = ({ ux, rankings }) => ({ ux, rankings });

export default connect(mapStateToProps, { select, unselect, closeSelectCountryDialog })(SelectCountryDialog);