import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { update, uploadRankingImage } from '../actions/AdminRankingsActions';
import { List, ListItem, Divider, Toggle } from 'material-ui';
import { EditorModeEdit, ActionDelete, ActionLabel, FileFileUpload } from 'material-ui/svg-icons';
import { colors } from 'material-ui/styles';
import EditRankingDialog from './EditRankingDialog';

import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);
 
class RankingSettings extends Component {

  state = { editRankingDialogOpen: false, selectedFile: null }

  componentWillUpdate(nextProps) {
    const { uploadProgress } = nextProps.adminRankings;
    if (uploadProgress === null && this.props.adminRankings.uploadProgress > 0) {
      this.setState({ ...this.state, selectedFile: null });
      let fileInput = document.getElementById('fileInput');
      fileInput.value = null;
    }
  }

  onFileSelected(e) {
    const file = document.getElementById('fileInput').files[0];
    this.setState({ ...this.state, selectedFile: file });
  }

  fileIsValid() {
    return String('image/jpeg image/gif image/png').indexOf(this.state.selectedFile.type) > -1
  }

  renderFilePrimaryLabel() {
    if (!this.state.selectedFile) return 'Seleccionar Imagen de Portada';
    if (!this.fileIsValid()) return 'Seleccionar Imagen de Portada Válida';
    return 'Click Para Subir Imagen De Portada';
  }

  renderFileSecondaryLabel() {
    if (!this.state.selectedFile) return 'Cambie la imagen de portada del ranking.';
    if (!this.fileIsValid()) return 'Tipo de archivo inválido.'
    return this.state.selectedFile.name;
  }

  onUploadPress() {
    if (!this.state.selectedFile || !this.fileIsValid()) document.getElementById('fileInput').click();
    else this.props.uploadRankingImage(this.props.adminRankings.selectedRanking, this.state.selectedFile);
  }

  onShowTitleToggle(newValue) {
    const { rankings, selectedRanking } = this.props.adminRankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return null;
    ranking.showTitle = newValue;
    this.props.update('ranking', ranking);
  }

  getShowTitleValue() {
    const { rankings, selectedRanking } = this.props.adminRankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return false;
    return ranking.showTitle;
  }

  render() {
    const { rankings, selectedRanking } = this.props.adminRankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    return (
      <div>
        <List>
          <ListItem 
              primaryText={this.renderFilePrimaryLabel()}
              secondaryText={this.renderFileSecondaryLabel()}
              leftIcon={<FileFileUpload color={ selectedRanking ? colors.green500 : colors.grey500 } />}
              onTouchTap={() => this.onUploadPress()}
            >
              <input 
                style={prefixer({ display: 'none'})} 
                type="file" 
                id="fileInput" 
                disabled={ selectedRanking ? false : true }
                onChange={e => this.onFileSelected(e)} 
              />
          </ListItem>
          <Divider />
          <ListItem 
            primaryText="Cambiar Nombre"
            secondaryText="Cambie el nombre del ranking. Los datos del ranking seguirán intactos."
            leftIcon={<EditorModeEdit color={ selectedRanking ? colors.blue500 : colors.grey500 } />}
            onTouchTap={() => selectedRanking ? this.setState({ ...this.state, editRankingDialogOpen: true }) : null } 
          />
          <Divider />
          <ListItem 
            primaryText="Mostrar Nombre en Portada"
            secondaryText="Mostrar u ocultar el nombre del ranking sobre la imagen de portada."
            leftIcon={<ActionLabel color={ selectedRanking ? colors.orange500 : colors.grey500 } />}
            rightToggle={<Toggle 
              toggled={this.getShowTitleValue()} 
              onToggle={(e, value) => this.onShowTitleToggle(value)} 
              disabled={ selectedRanking ? false : true }
            />}/>
          <Divider />
          <ListItem 
            primaryText="Eliminar" 
            secondaryText="Elimine ranking. Los datos del ranking también serán eliminados. Tenga precaución."
            leftIcon={<ActionDelete color={ selectedRanking ? colors.red500 : colors.grey500 } />}
          />
          <Divider />
        </List>

        <EditRankingDialog 
          open={this.state.editRankingDialogOpen}
          title="Editar Ranking"
          ranking={ranking}
          onConfirm={(rankingName) => this.props.update('ranking', { id: selectedRanking, name: rankingName })}
          onClose={() => this.setState({ ...this.state, editRankingDialogOpen: false })}
        />
      </div>
    );
  }
}

function mapStateToProps({ adminRankings }) {
  return { adminRankings }
}
 
export default connect(mapStateToProps, { update, uploadRankingImage })(RankingSettings);