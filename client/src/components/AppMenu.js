import _ from "lodash";
import { baseURL } from "../api";
import React, { Component } from "react";
import { connect } from "react-redux";
import * as Utils from "../utils";
import { Redirect, NavLink } from "react-router-dom";
import { openDrawer, closeDrawer } from "../actions";
import { Drawer, AppBar, MenuItem, Avatar, Badge } from "material-ui";
import { colors } from "material-ui/styles";
import {
  ContentSort,
  ContentContentPaste,
  ActionSupervisorAccount,
  ActionExitToApp,
  ActionDashboard,
  CommunicationMessage,
  CommunicationLiveHelp,
  ActionSettings,
  HardwareKeyboardArrowRight,
  MapsMap,
  ActionGroupWork,
  ActionAnnouncement,
  NavigationMenu
} from "material-ui/svg-icons";
import AppMenuIcon from "./AppMenuIcon";
import rankingImg from "../images/ranking.png";
import messageImg from "../images/message.png";
import triviaImg from "../images/trivia.png";
import surveyImg from "../images/survey.png";
import newsImg from "../images/newspaper.png";
import fallbackUserImg from "../images/user.png";

import postCss from "postcss-js";
const prefixer = postCss.sync([require("autoprefixer")]);

class AppMenu extends Component {
  toggleDrawer() {
    const { ux, openDrawer, closeDrawer } = this.props;
    ux.drawer ? closeDrawer() : openDrawer();
  }

  getProfilePicture() {
    const { user } = this.props.auth;
    return user.photoURL ? baseURL + user.photoURL : fallbackUserImg;
  }

  navigateTo(pathname) {
    this.props.history.push(pathname);
    this.props.closeDrawer();
  }

  renderAdminOption() {
    const { closeDrawer } = this.props;
    const { user } = this.props.auth;
    if (!Utils.isAdmin(user)) return null;
    return (
      <MenuItem
        leftIcon={<ActionSettings />}
        rightIcon={<HardwareKeyboardArrowRight />}
        primaryText="Administración"
        menuItems={[
          <MenuItem
            leftIcon={<ActionGroupWork />}
            onTouchTap={() => this.navigateTo("/admin/roles")}
            primaryText="Roles"
          />,
          <MenuItem
            leftIcon={<ActionGroupWork />}
            onTouchTap={() => this.navigateTo("/admin/businessunits")}
            primaryText="Business Units"
          />,
          <MenuItem
            leftIcon={<ActionGroupWork />}
            onTouchTap={() => this.navigateTo("/admin/lines")}
            primaryText="Líneas"
          />,
          <MenuItem leftIcon={<MapsMap />} onTouchTap={() => this.navigateTo("/admin/paises")} primaryText="Países" />,
          <MenuItem
            leftIcon={<ContentSort />}
            onTouchTap={() => this.navigateTo("/admin/rankings")}
            primaryText="Rankings"
          />,
          <MenuItem
            leftIcon={<ActionDashboard />}
            onTouchTap={() => this.navigateTo("/admin/datos")}
            primaryText="Datos"
          />,
          <MenuItem
            leftIcon={<CommunicationLiveHelp />}
            onTouchTap={() => this.navigateTo("/admin/surveys")}
            primaryText="Encuestas y Trivias"
          />,
          <MenuItem
            leftIcon={<ActionSupervisorAccount />}
            onTouchTap={() => this.navigateTo("/admin/usuarios")}
            primaryText="Usuarios"
          />,
          <MenuItem
            leftIcon={<ActionAnnouncement />}
            onTouchTap={() => this.navigateTo("/admin/mktnews")}
            primaryText="MKT News"
          />,
          <MenuItem
            leftIcon={<CommunicationMessage />}
            onTouchTap={() => this.navigateTo("/admin/mensajes")}
            primaryText="Mensajes"
          />
        ]}
      />
    );
  }

  render() {
    const { ux, closeDrawer, auth } = this.props;
    const { unreadPostCount, unreadMessageCount, unreadSurveyCount } = auth.user;
    return (
      <Drawer open={ux.drawer}>
        <AppBar iconElementLeft={<AppMenuIcon />} onLeftIconButtonTouchTap={this.toggleDrawer.bind(this)} />
        <MenuItem
          leftIcon={<Avatar style={styles.menuIconStyle} src={rankingImg} />}
          onTouchTap={() => this.navigateTo("/")}
          primaryText="Rankings"
        />
        <MenuItem
          leftIcon={<Avatar style={styles.menuIconStyle} src={messageImg} />}
          onTouchTap={() => this.navigateTo("/mensajes")}
          primaryText="Mensajes"
          rightIcon={
            <Badge
              badgeContent={unreadMessageCount}
              badgeStyle={{
                color: "#FFF",
                backgroundColor: colors.red700,
                opacity: unreadMessageCount > 0 ? 1 : 0
              }}
            />
          }
        />
        <MenuItem
          leftIcon={<Avatar style={styles.menuIconStyle} src={surveyImg} />}
          onTouchTap={() => this.navigateTo("/surveys")}
          primaryText="Encuestas Y Trivias"
          rightIcon={
            <Badge
              badgeContent={unreadSurveyCount}
              badgeStyle={{
                color: "#FFF",
                backgroundColor: colors.red700,
                opacity: unreadSurveyCount > 0 ? 1 : 0
              }}
            />
          }
        />
        <MenuItem
          leftIcon={<Avatar style={styles.menuIconStyle} src={newsImg} />}
          onTouchTap={() => this.navigateTo("/mktnews")}
          primaryText="MKT News"
          rightIcon={
            <Badge
              badgeContent={unreadPostCount}
              badgeStyle={{
                color: "#FFF",
                backgroundColor: colors.red700,
                opacity: unreadPostCount > 0 ? 1 : 0
              }}
            />
          }
        />
        <MenuItem
          leftIcon={<Avatar src={this.getProfilePicture()} />}
          onTouchTap={() => this.navigateTo("/profile")}
          primaryText="Mi Perfil"
        />
        {this.renderAdminOption()}
        <MenuItem leftIcon={<ActionExitToApp />} onTouchTap={this.props.onLogoutPress} primaryText="Salir" />
      </Drawer>
    );
  }
}

const styles = {
  menuIconStyle: prefixer({
    borderRadius: 0,
    backgroundColor: "transparent"
  })
};

function mapStateToProps({ auth, ux, router, users }) {
  return { auth, ux, router, users };
}

export default connect(mapStateToProps, { openDrawer, closeDrawer })(AppMenu);
