import React, { Component } from 'react';
import {Dialog, FlatButton, TextField} from 'material-ui';
import { connect } from 'react-redux';
import * as Actions from '../actions';
 
class ChangePasswordDialog extends Component {
  INITIAL_STATE = {
    password: '',
    confirmPassword: ''
  }
  
  state = { ...this.INITIAL_STATE }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.ux.changePasswordDialog === false) {
      this.setState({ ...this.INITIAL_STATE });
    }
  }

  onInputChange = (field, value) => {
    this.setState({ [field]: value });
  }

  onCancel = () => {
    this.setState({ password: '', confirmPassword: '' });
    this.props.closeChangePasswordDialog();
  }

  onConfirm = () => {
    const { user } = this.props.auth;
    this.props.changeUserPassword(user.id, this.state.password);
  }

  isValid() {
    if (
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/.test(this.state.password)
      && this.state.password === this.state.confirmPassword
    ) {
      console.log('valid')
      return true;
    } else {
      console.log('invalid')
      return false;
    };
  }
  
  getActions = () => ([
    <FlatButton
      label="Cancelar"
      primary={true}
      onTouchTap={() => this.onCancel()}
    />,
    <FlatButton
      label="Confirmar"
      primary={true}
      disabled={!this.isValid()}
      onTouchTap={() => this.onConfirm()}
    />
  ])
  

  render() {
    const { user } = this.props.auth;

    return (
      <Dialog
        title="Cambiar Password"
        actions={this.getActions()}
        modal={false}
        open={this.props.ux.changePasswordDialog}
        contentStyle={{ maxWidth: 'none', width: '90%' }}
      >
        Introduzca un password con mínimo de 8 caracteres, usando mayúsculas y números.
        <br />
        <TextField
          hintText="Nuevo password..."
          onChange={(e) => this.onInputChange('password', e.target.value)}
          value={this.state.password}
          type="password"
        /><br />
        <TextField
          hintText="Confirmar password..."
          onChange={(e) => this.onInputChange('confirmPassword', e.target.value)}
          value={this.state.confirmPassword}
          type="password"
        /><br />
      </Dialog>
    );
  }
}

function mapStateToProps({ auth, user, ux }) {
  return { auth, user, ux };
}
 
export default connect(mapStateToProps, Actions)(ChangePasswordDialog);