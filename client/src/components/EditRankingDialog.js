import _ from 'lodash';
import React, { Component } from 'react';
import { Dialog, FlatButton, TextField } from 'material-ui';
 
class EditRankingDialog extends Component {

  state = { rankingName: '', showTitle: true }

  componentWillReceiveProps(newProps) {
    if (!newProps.ranking) return;
    this.setState({ rankingName: newProps.ranking.name })
  }

  handleClose(result) {
    if (result === 'confirm') {
      this.props.onConfirm(this.state.rankingName);
      this.props.onClose();
    } else {
      this.props.onClose();
    }
  }

  onInputChange(field, value) {
    this.setState({ ...this.state, [field]: value });
  }
  
  actions = [
    <FlatButton
      label="Cancelar"
      primary={true}
      onTouchTap={() => this.handleClose('cancel')}
    />,
    <FlatButton
      label="Confirmar"
      primary={true}
      keyboardFocused={true}
      onTouchTap={() => this.handleClose('confirm')}
    />
  ]

  render() {
    return (
      <Dialog
        title={this.props.title}
        actions={this.actions}
        modal={false}
        open={this.props.open}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={true}
      >
        <TextField
          floatingLabelText="Nombre del Ranking"
          value={this.state.rankingName}
          onChange={(e) => this.onInputChange('rankingName', e.target.value)}
        />
      </Dialog>
    );
  }
}
 
export default EditRankingDialog;