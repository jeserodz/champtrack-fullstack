import _ from 'lodash';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { oneOf } from 'prop-types';
import { connect } from 'react-redux';
import { closeSurveyResultsDialog } from '../actions/UXActions';
import { Dialog, FlatButton, List, ListItem, Avatar } from 'material-ui';
import fallbackProfile from '../images/avatar-placeholder.png';
 
class SurveyResultsDialog extends Component {

  static propTypes = {
    mode: oneOf(['trivia', 'survey']).isRequired
  }

  handleClose = () => {
    this.props.closeSurveyResultsDialog();
  };

  getCorrectAnswer(data) {
    const item = _.get(data.list, data.selected, {});
    const answer = _.get(item.options, item.correctAnswer);
    return answer;
  }

  renderAnswersList(survey) {
    if (_.isEmpty(survey.answers)) {
      return (
        <ListItem 
          primaryText={'No hay participaciones en esta ' + (survey.hasCorrectOption ? 'trivia.' : 'encuesta.') }
        />
      )
    }

    return _.map(survey.answers, (answer, index) => {
      return (
          <ListItem 
            key={index}
            leftAvatar={<Avatar src={answer.user.photoURL ? baseURL + answer.user.photoURL : fallbackProfile } />}
            primaryText={answer.user.displayName}
            secondaryTextLines={2}
            secondaryText={
              <p>
                <strong>Selección: </strong>{answer.option.title} <br/>
                <strong>Fecha: </strong>{(new Date(answer.createdDate)).toLocaleString()}
              </p>
            }
          />
      )
    });
  }

  render() {
    const { mode, ux, adminSurveys } = this.props;
    const survey = _.find(adminSurveys.surveys, { id: adminSurveys.selectedSurvey });
    if (!survey) return null;

    const correctOptions = _.filter(survey.options, { isCorrect: true });
    let correctOptionsTitles = '';
    _.forEach(correctOptions, (option, index, array) => {
      correctOptionsTitles += option.title;
      option === _.last(correctOptions) ? null : correctOptionsTitles += ', '
    });

    const actions = [
      <FlatButton
        label="Cerrar"
        primary={true}
        onTouchTap={this.handleClose}
      />
    ];

    return (
      <Dialog
        title={"Resultados de " + (survey.hasCorrectOption ? 'Trivia' : 'Encuesta')}
        actions={actions}
        modal={false}
        open={ux.surveyResultsDialog}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={true}
      >
        <p>{ 'Pregunta: ' + survey.question }</p>
        <p>{ survey.hasCorrectOption ? 'Respuesta Correcta: ' + correctOptionsTitles : null }</p>
        <List>
          { this.renderAnswersList(survey) }
        </List>
      </Dialog>
    );
  }
}

function mapStateToProps({ ux, adminSurveys }) {
  return { ux, adminSurveys }
}
 
export default connect(mapStateToProps, { closeSurveyResultsDialog })(SurveyResultsDialog);