import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { assignRolesToUser } from '../actions/AdminUsersActions';
import { closeAssignRolesDialog } from '../actions/UXActions';
import { Dialog, List, ListItem, Divider, FlatButton, Checkbox } from 'material-ui';
 
class AssignRolesDialog extends Component {

  state = {
    currentUser: null
  }

  componentWillUpdate(nextProps) {
    const { selectedUser } = nextProps.adminUsers;
    if (selectedUser && selectedUser !== _.get(this.state.currentUser, 'id')) {
      const user = _.find(nextProps.adminUsers.users, { id: selectedUser });
      this.setState({ currentUser: _.cloneDeep(user) })
    }
  }

  onRoleSelect(selectedRole, isChecked) {
    let roles = [ ...this.state.currentUser.roles ];
    if (isChecked) roles.push(selectedRole);
    else roles = roles.filter(role => role.id !== selectedRole.id);
    const currentUser = { ...this.state.currentUser, roles };
    this.setState({ currentUser });
  }

  renderRoles() {
    const { roles } =  this.props.adminUsers;
    const { currentUser } = this.state;
    return _.map(roles, (role, index) => {
      return (
        <span key={index}>
          <ListItem 
            primaryText={role.name} 
            leftCheckbox={<Checkbox 
              checked={(currentUser && _.find(currentUser.roles, { id: role.id })) ? true : false}
              onCheck={(e,isChecked) => this.onRoleSelect(role, isChecked)} 
            />}
          />
        </span>
      )
    })
  }

  render() {
    return (
      <Dialog 
        open={this.props.ux.assignRolesDialog}
        title="Asignar Roles"
        titleStyle={{ padding: '24px 16px' }}
        bodyStyle={{ padding: 0 }}
        autoScrollBodyContent={true}
        actions={[ 
          <FlatButton 
            primary 
            label="Cancelar" 
            onTouchTap={this.props.closeAssignRolesDialog} 
          />,
          <FlatButton 
            primary 
            label="Guardar Cambios" 
            onTouchTap={() => this.props.assignRolesToUser(
              this.state.currentUser, 
              this.state.currentUser.roles
            )} 
          /> 
        ]}
      >
        <List style={{ padding: 0 }}>
          { this.renderRoles() }
        </List>
      </Dialog>
    );
  }
}

const mapStateToProps = ({ ux, adminUsers }) => ({ ux, adminUsers });

export default connect(mapStateToProps, { assignRolesToUser, closeAssignRolesDialog })(AssignRolesDialog);