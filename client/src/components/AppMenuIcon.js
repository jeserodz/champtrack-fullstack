import React, { Component } from "react";
import { connect } from "react-redux";
import * as Actions from "../actions";
import { Badge, IconButton } from "material-ui";
import { NavigationMenu } from "material-ui/svg-icons";

class AppMenuIcon extends Component {
  toggleDrawer() {
    const { ux, openDrawer, closeDrawer } = this.props;
    ux.drawer ? closeDrawer() : openDrawer();
  }

  getTotalUnread = () => {
    const { user } = this.props.auth;
    const totalUnread = user.unreadPostCount + user.unreadMessageCount + user.unreadSurveyCount;
    return totalUnread;
  }

  render() {
    const totalUnread = this.getTotalUnread();
    return (
      <Badge badgeContent={totalUnread} badgeStyle={{ opacity: (totalUnread > 0) ? 1 : 0 }}>
        <IconButton style={{ marginTop: -24 }} onTouchTap={this.toggleDrawer.bind(this)}>
          <NavigationMenu color="#FFFFFF" />
        </IconButton>
      </Badge>
    );
  }
}

function mapStateToProps(state) {
  return { ...state };
}

export default connect(mapStateToProps, Actions)(AppMenuIcon);
