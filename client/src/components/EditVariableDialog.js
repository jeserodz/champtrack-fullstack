import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { select, create, update } from '../actions/AdminRankingsActions';
import { closeVariableDialog } from '../actions/UXActions';
import { Dialog, TextField, FlatButton } from 'material-ui';

class EditDialogVariable extends Component {

  INITIAL_STATE = { 
    id: null,
    rankingId: null, 
    name: '', 
    weight: 0 
  }

  state = { ...this.INITIAL_STATE }

  componentWillReceiveProps(nextProps) {
    const { rankings, selectedRanking, selectedVariable } = nextProps.adminRankings;
    const ranking = _.find(rankings, { id: selectedRanking });
    const variable = ranking ? _.find(ranking.variables, { id : selectedVariable }) : null;
    this.setState(variable ? { ...variable } : { 
      rankingId: selectedRanking,
      name: '',
      weight: 0
    });
  }

  renderDialogTitle() {
    const { selectedVariable } = this.props.adminRankings;
    return selectedVariable ? "Editar Variable" : "Crear Variable";
  }

  onClose() {
    this.props.select('variable', null);
    this.props.closeVariableDialog();
    this.setState(this.INITIAL_STATE);
  }

  onConfirm() {
    const { 
      adminRankings: { selectedVariable }, 
      create, 
      update, 
      select, 
      closeVariableDialog 
    } = this.props;

    selectedVariable ? update('variable', this.state) : create('variable', this.state);
    select('period', null);
    closeVariableDialog();
    this.setState(this.INITIAL_STATE);
  }

  onTextChange(field, value) {
    this.setState({ ...this.state, [field]: value });
  }

  actions = [
    <FlatButton label="Cerrar" onTouchTap={this.onClose.bind(this)} />,
    <FlatButton label="Confirmar" primary onTouchTap={this.onConfirm.bind(this)} />
  ]

  render() {
    return (
      <Dialog
        title={this.renderDialogTitle()}
        actions={this.actions}
        open={this.props.ux.variableDialog}
      >
        <TextField 
          floatingLabelText="Nombre" 
          type="text" 
          value={this.state.name} 
          onChange={(e) => this.onTextChange('name', e.target.value)} 
        /><br />
        <TextField 
          floatingLabelText="Peso" 
          type="number" 
          value={this.state.weight} 
          onChange={(e) => this.onTextChange('weight', e.target.value)} 
        />
      </Dialog>
    );
  }
}

function mapStateToProps({ adminRankings, ux }) {
  return { adminRankings, ux };
}
 
export default connect(mapStateToProps, { select, create, update, closeVariableDialog })(EditDialogVariable);