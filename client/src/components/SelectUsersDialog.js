import _ from 'lodash';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { assignRankingToUser } from '../actions/AdminRankingsActions';
import { Dialog, FlatButton, List, ListItem, Checkbox, Avatar, Divider } from 'material-ui';
import avatarPlaceholder from '../images/avatar-placeholder.png'; 
 
class SelectUsersDialog extends Component {

  handleClose(result) {
    this.props.onClose();
  }

  toggleParticipant(user, included) {
    const { selectedRanking } = this.props.adminRankings;
    user.rankingId = included ? selectedRanking : null;
    this.props.assignRankingToUser(user, user.rankingId);
  }
  
  actions = [
    <FlatButton
      label="Cerrar"
      primary={true}
      onTouchTap={() => this.handleClose('cancel')}
    />
  ]

  renderListItems() {
    const { selectedRanking, users } = this.props.adminRankings;
    const firstItem = _.find(users, {});
    return _.map(users, (user, index) => {
      return (
        <span key={index}>
          { user === firstItem ? null : <Divider /> }
          <ListItem
            leftCheckbox={
              <Checkbox 
                checked={(user.rankingId === selectedRanking) ? true : false }
                onCheck={(e, isChecked) => { this.toggleParticipant(user, isChecked) }}
              />
            }
            rightAvatar={<Avatar src={user.photoURL ? baseURL + user.photoURL : avatarPlaceholder} />}
            primaryText={user.displayName}
            secondaryText={user.email}
          />
        </span>
      );
    });
  }

  render() {
    return (
      <Dialog
        title="Seleccionar Usuarios"
        actions={this.actions}
        modal={false}
        open={this.props.open}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={true}
      >
        <List>
          { this.renderListItems() }
        </List>
      </Dialog>
    );
  }
}

function mapStateToProps({ adminRankings }) {
  return { adminRankings }
}

export default connect(mapStateToProps, { assignRankingToUser })(SelectUsersDialog);