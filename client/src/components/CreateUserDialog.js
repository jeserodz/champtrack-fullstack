import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dialog, TextField, FlatButton, SelectField, MenuItem } from 'material-ui';
 
class CreateUserDialog extends Component {

  state = {
    email: '',
    password: '',
    displayName: '',
    country: ''
  }

  handleClose(result) {
    if (result === 'confirm') {
      this.props.onConfirm(this.state);
      this.props.onClose();
    } else {
      this.props.onClose();
    }
  }

  onInputChange(field, value) {
    this.setState({ ...this.state, [field]: value });
  }

  renderCountryOptions() {
    const { countries } = this.props;
    return _.map(countries.list, (c, key) => {
      return <MenuItem 
        key={key}
        value={key}
        primaryText={c.name}
      />
    });
  }
  
  actions = [
    <FlatButton
      label="Cerrar"
      primary={true}
      onTouchTap={() => this.handleClose('cancel')}
    />,
    <FlatButton
      label="Confirmar"
      primary={true}
      keyboardFocused={true}
      onTouchTap={() => this.handleClose('confirm')}
    />
  ]

  render() {
    return (
      <Dialog
        title="Crear Usuario"
        actions={this.actions}
        modal={false}
        open={this.props.open}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={true}
      >
        <TextField
          floatingLabelText="Email"
          value={this.state.email}
          onChange={(e) => this.onInputChange('email', e.target.value)}
        /><br />
        <TextField
          floatingLabelText="Contraseña"
          type="password"
          value={this.state.password}
          onChange={(e) => this.onInputChange('password', e.target.value)}
        /><br />
        <TextField
          floatingLabelText="Nombre del usuario"
          value={this.state.displayName}
          onChange={(e) => this.onInputChange('displayName', e.target.value)}
        /><br />
        <SelectField
          floatingLabelText="País"
          value={this.state.country}
          onChange={(event, index, value) => this.onInputChange('country', value)}
        >
          { this.renderCountryOptions() }
        </SelectField><br />
      </Dialog>
    );
  }
}

function mapStateToProps({ countries }) {
  return { countries }
}
 
export default connect(mapStateToProps)(CreateUserDialog);