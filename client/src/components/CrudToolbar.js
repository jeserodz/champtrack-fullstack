import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import * as Icons from 'material-ui/svg-icons';
import { 
  Toolbar, 
  ToolbarGroup,
  FlatButton,
  RaisedButton
} from 'material-ui';

class CrudToolbar extends Component {
  static propTypes = {
    saveButtonLabel: PropTypes.string,
    cancelButtonLabel: PropTypes.string,
    onSavePress: PropTypes.func,
    onCancelPress: PropTypes.func,
    saveButtonDisabled: PropTypes.bool
  }

  static defaultProps = {
    saveButtonDisabled: false
  }

  render() {
    const {
      saveButtonLabel,
      cancelButtonLabel,
      onSavePress,
      onCancelPress
    } = this.props;

    return (
      <Toolbar>
        <ToolbarGroup>
          <RaisedButton 
            onTouchTap={(e) => onSavePress ? onSavePress(e) : null }
            label={ saveButtonLabel || "Salvar" }
            icon={<Icons.ContentSave />}
            disabled={this.props.saveButtonDisabled}
          />
          <RaisedButton 
            onTouchTap={(e) => onCancelPress ? onCancelPress(e) : null }
            label={ this.props.cancelButtonLabel || "Cancelar" }
            icon={<Icons.NavigationCancel />}
          />
        </ToolbarGroup>
      </Toolbar>
    );
  }
}

export default CrudToolbar;