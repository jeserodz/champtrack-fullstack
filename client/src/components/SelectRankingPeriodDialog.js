import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { select, fetchAccumulatedRecordsByYear } from '../actions/RankingsActions';
import { closeSelectRankingPeriodDialog } from '../actions/UXActions';
import { Dialog, List, ListItem, Divider, FlatButton } from 'material-ui';
import moment from 'moment';

class SelectRankingPeriod extends Component {

  onPeriodSelect(id) {
    this.props.select('period', id);
    this.props.closeSelectRankingPeriodDialog();
  }

  onYearSelect(year) {
    const { selectedRanking } = this.props.rankings;
    this.props.fetchAccumulatedRecordsByYear(selectedRanking, year);
    this.props.closeSelectRankingPeriodDialog();
  }

  renderRankingPeriods() {
    const { rankings, selectedRanking } =  this.props.rankings;
    const ranking = _.find(rankings, { id: selectedRanking })

    if (!ranking || !ranking.periods.length) return (
      <ListItem primaryText="Este ranking aún no contiene períodos de datos." />
    )

    const years = _.groupBy(ranking.periods, period => moment(period.endDate).year());

    return _.map(years, (yearPeriods, year) => (
      <ListItem
        key={year}
        primaryText={year}
        onTouchTap={() => this.onYearSelect(year)}
        nestedItems={yearPeriods.map(period => (
          <ListItem
            key={period.id}
            primaryText={period.name}
            onTouchTap={() => this.onPeriodSelect(period.id)}
          />
        ))}
      />
    ))
  }

  render() {
    return (
      <Dialog
        open={this.props.ux.selectRankingPeriodDialog}
        title="Seleccione un período"
        contentStyle={{
          backgroundColor: 'red',
          position: 'absolute',
          top: 0,
          left: '1em',
          right: '1em'
        }}
        titleStyle={{ padding: '24px 16px' }}
        bodyStyle={{ padding: 0 }}
        autoScrollBodyContent={true}
        actions={[
          <FlatButton
            primary
            label="Cerrar"
            onTouchTap={this.props.closeSelectRankingPeriodDialog}
          />
        ]}
      >
        <List style={{ padding: 0 }}>
          <ListItem
            primaryText={'Acumulado'}
            onTouchTap={() => this.onPeriodSelect(null)}
          />
          {this.renderRankingPeriods()}
        </List>
      </Dialog>
    );
  }
}

const mapStateToProps = ({ ux, rankings }) => ({ ux, rankings });

export default connect(mapStateToProps, { select, closeSelectRankingPeriodDialog, fetchAccumulatedRecordsByYear })(SelectRankingPeriod);
