import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectUser, changeUser, uploadUserImage } from '../actions'; 
import { spacing } from 'material-ui/styles';
import { 
  Dialog, 
  TextField, 
  FlatButton, 
  RaisedButton, 
  SelectField, 
  MenuItem, 
  LinearProgress,
  Avatar
} from 'material-ui';
import { StyleSheet, css } from 'aphrodite';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);
 
class EditUserDialog extends Component {

  INITIAL_STATE = { 
    id: null,
    displayName: '', 
    country: '', 
    selectedFile: null 
  }

  state = { ...this.INITIAL_STATE }

  componentWillReceiveProps(nextProps) {
    const { list, selected } = nextProps.users;
    if (!selected) 
      return this.setState(this.INITIAL_STATE);
    else
      this.setState({
        ...this.state,
        displayName: list[selected].displayName,
        country: list[selected].country
      });
  }


  handleClose(result) {
    if (result === 'confirm') {
      const { displayName, country } = this.state;
      this.props.changeUser(this.props.users.selected, { displayName, country });
      this.props.selectUser(null);
    } else {
      this.props.selectUser(null);
    }
  }

  onInputChange(field, value) {
    this.setState({ ...this.state, [field]: value });
  }

  onFileSelected() {
    const file = document.getElementById('fileInput').files[0];
    console.log(file)
    this.setState({ ...this.state, selectedFile: file });
  }

  onUploadPress() {
    const { users, uploadUserImage } = this.props;
    uploadUserImage(users.selected, this.state.selectedFile);
  }

  renderUploadImageButton() {
    const { selectedFile } = this.state;
    if (!selectedFile) return null;
    const validFormat = String('image/jpeg image/gif image/png').indexOf(selectedFile.type) > -1;
    return (
      <RaisedButton
        primary
        label={ !validFormat ? 'Tipo de archivo inválido' : 'Subir Imagen' }
        disabled={!validFormat}
        onTouchTap={() => this.onUploadPress()}
      />
    )
  }
  
  renderUploadProgressBar() {
    const { photoUploadProgress } = this.props.users;
    if (photoUploadProgress === null) return null;
    return (
      <div style={prefixer({ margin: spacing.desktopGutterMini })}>
        <LinearProgress value={photoUploadProgress}/>
      </div>
    );
  }

  renderCountryOptions() {
    const { countries } = this.props;
    return _.map(countries.list, (c, key) => {
      return <MenuItem 
        key={key}
        value={key}
        primaryText={c.name}
      />
    });
  }
  
  actions = [
    <FlatButton
      label="Cerrar"
      primary={true}
      onTouchTap={() => this.handleClose('cancel')}
    />,
    <FlatButton
      label="Confirmar"
      primary={true}
      keyboardFocused={true}
      onTouchTap={() => this.handleClose('confirm')}
    />
  ]

  render() {
    const { users } = this.props;
    console.log(users, _.get(users.list, users.selected + '.photoURL', ''))
    return (
      <Dialog
        title="Editar Usuario"
        actions={this.actions}
        modal={false}
        open={this.props.open}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={true}
      >
        <div className={css(styles.c1)}>
          <div className={css(styles.c2)}>
            <TextField
              floatingLabelText="Nombre del usuario"
              value={this.state.displayName}
              onChange={(e) => this.onInputChange('displayName', e.target.value)}
            />
            <br />
            <SelectField
              floatingLabelText="País"
              value={this.state.country}
              onChange={(event, index, value) => this.onInputChange('country', value)}
            >
              { this.renderCountryOptions() }
            </SelectField><br />
            <RaisedButton containerElement='label' label='Seleccionar Foto'>
                <input 
                  id="fileInput" 
                  type="file" 
                  style={prefixer({ display: 'none' })} 
                  onChange={(e) => this.onFileSelected()} 
                />
            </RaisedButton>
            <p>{ _.get(this.state.selectedFile, 'name', '') }</p>
            {this.renderUploadImageButton()}
            {this.renderUploadProgressBar()}
          </div>
          <div className={css(styles.c3)}>
            <Avatar
              style={prefixer({ margin: spacing.desktopGutterLess })} 
              size={128} 
              src={_.get(users.list, users.selected + '.photoURL', '')}
            />
          </div>
        </div>
      </Dialog>
    );
  }
}

const styles = StyleSheet.create({
  c1: { display: 'flex', flexDirection: 'row' },
  c2: { flex: 2, display: 'flex', flexDirection: 'column' },
  c3: { flex: 1, display: 'flex', justifyContent: 'center' }
})

function mapStateToProps({ users, countries }) {
  return { users, countries }
}
 
export default connect(mapStateToProps, { selectUser, changeUser, uploadUserImage })(EditUserDialog);