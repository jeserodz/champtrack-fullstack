import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dialog, FlatButton, TextField } from 'material-ui';
 
class ConfirmDialog extends Component {

  static propTypes = {
    open: PropTypes.bool.isRequired,
    title: PropTypes.string,
    message: PropTypes.string,
    onConfirm: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
  }

  onInputChange(field, value) {
    this.setState({ ...this.state, [field]: value });
  }
  
  actions = [
    <FlatButton
      label="Cancelar"
      primary={true}
      onTouchTap={() => this.props.onCancel()}
    />,
    <FlatButton
      label="Confirmar"
      primary={true}
      keyboardFocused={true}
      onTouchTap={() => this.props.onConfirm()}
    />
  ]

  render() {
    return (
      <Dialog
        title={this.props.title || 'Confirmar'}
        actions={this.actions}
        modal={false}
        open={this.props.open}
        onRequestClose={this.onCancel}
        autoScrollBodyContent={true}
      >
        { this.props.message || '¿Realmente desea realizar esta acción?' }
      </Dialog>
    );
  }
}
 
export default ConfirmDialog;