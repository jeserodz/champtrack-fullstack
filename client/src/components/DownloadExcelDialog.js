import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import { Dialog, FlatButton, TextField } from 'material-ui';
 
class ConfirmDialog extends Component {
  
  getButtons() { 
    return ([
      <FlatButton
        label="Cerrar"
        primary={true}
        onTouchTap={() => this.props.closeDownloadExcelDialog()}
      />,
      <FlatButton
        label="Descargar"
        href={this.props.ux.downloadExcelURL}
        download={true}
        target="_blank"
        onTouchTap={() => setTimeout(() => {this.props.closeDownloadExcelDialog()}, 300)}
      />
    ]);
  }

  render() {
    return (
      <Dialog
        title={'Exportar'}
        actions={this.getButtons()}
        modal={false}
        open={this.props.ux.downloadExcelDialog}
        onRequestClose={this.onCancel}
        autoScrollBodyContent={true}
      >
        <p>Presione "Descargar" para exportar la lista en Excel</p>
      </Dialog>
    );
  }
}

function mapStateToProps({ ux }) {
  return { ux };
}
 
export default connect(mapStateToProps, Actions)(ConfirmDialog);