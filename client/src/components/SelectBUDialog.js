import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { select, unselect } from '../actions/RankingsActions';
import { closeSelectBUDialog } from '../actions/UXActions';
import { Dialog, List, ListItem, Divider, FlatButton, Checkbox } from 'material-ui';
 
class SelectBUDialog extends Component {

  onBUSelect(id, isChecked) {
    const { select, unselect } = this.props;
    isChecked ? select('businessUnit', id) : unselect('businessUnit', id);
  }

  renderBUs() {
    const { businessUnits, selectedBUs } =  this.props.rankings;
    return _.map(businessUnits, (businessUnit, index) => {
      return (
        <span key={index}>
          <ListItem 
            primaryText={businessUnit.name} 
            leftCheckbox={<Checkbox 
              checked={_.includes(selectedBUs, businessUnit.id)}
              onCheck={(e,isChecked) => this.onBUSelect(businessUnit.id, isChecked)} 
            />}
          />
        </span>
      )
    })
  }

  render() {
    return (
      <Dialog 
        open={this.props.ux.selectBUDialog}
        title="Filtre por Business Units"
        titleStyle={{ padding: '24px 16px' }}
        bodyStyle={{ padding: 0 }}
        autoScrollBodyContent={true}
        actions={[ 
          <FlatButton 
            primary 
            label="Cerrar" 
            onTouchTap={this.props.closeSelectBUDialog} 
          /> 
        ]}
      >
        <List style={{ padding: 0 }}>
          { this.renderBUs() }
        </List>
      </Dialog>
    );
  }
}

const mapStateToProps = ({ ux, rankings }) => ({ ux, rankings });

export default connect(mapStateToProps, { select, unselect, closeSelectBUDialog })(SelectBUDialog);