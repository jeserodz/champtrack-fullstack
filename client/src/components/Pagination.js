import _ from 'lodash';
import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { StyleSheet, css } from 'aphrodite';
import { IconButton, Chip, TextField } from 'material-ui';
import { NavigationChevronLeft, NavigationChevronRight } from 'material-ui/svg-icons';

class Pagination extends Component {

  static propTypes = {
    collection: PropTypes.array.isRequired,
    pageSize: PropTypes.number.isRequired,
    render: PropTypes.func.isRequired,
    filterHintText: PropTypes.string,
    filter: PropTypes.func,
  }
  
  state = { 
    currentPage: 0,
    filterText: ''
  }

  previousPage() {
    if (this.state.currentPage > 0) {
      this.setState({
        currentPage: this.state.currentPage - 1
      });
    }
  }

  nextPage() {
    if (this.state.currentPage < this.getLastPage()) {
      this.setState({
        currentPage: this.state.currentPage + 1
      });
    }
  }

  getLastPage() {
    const { pageSize } = this.props;
    const filteredCollection = this.getFilteredCollection();
    return Math.floor(filteredCollection.length / pageSize);
  }
  
  getFilteredCollection() {
    const { collection, filter } = this.props;
    const { filterText } = this.state;
    const filteredCollection = (filterText && filter) ? 
      _.filter(collection, item => filter(item, filterText)) : 
      collection;
    return filteredCollection;
  }

  onFilterChange(value) {
    this.setState({
      filterText: value,
      currentPage: 0
    });
  }

  renderIndicator() {
    const { currentPage } = this.state;
    return `${currentPage + 1}/${this.getLastPage() + 1}`;
  }

  renderPage() {
    const { pageSize, render } = this.props;
    const { currentPage } = this.state;
    const filteredCollection = this.getFilteredCollection();
    const page = filteredCollection.slice(
      pageSize * currentPage, 
      pageSize * (currentPage+1)
    );
    return page.map((item, index) => render(item, index))
  }

  render() {
    const { filterHintText } = this.props;
    const { filterText } = this.state;

    return (
      <div className={css(styles.container)}>
        <div className={css(styles.header)}>
          <IconButton onTouchTap={() => this.previousPage()}>
            <NavigationChevronLeft/> 
          </IconButton>
          <Chip className={css(styles.chip)}>
            { this.renderIndicator() }
          </Chip>
          <IconButton onTouchTap={() => this.nextPage()}>
            <NavigationChevronRight/>
          </IconButton>
        </div>
        <div className={css(styles.filter)}>
          <TextField
            style={{ width: '100%'}}
            hintText={ filterHintText || 'Filtrar...' }
            onChange={(e) => this.onFilterChange(e.target.value)}
            value={filterText}
          />
        </div>
        <div className={css(styles.body)}>
          { this.renderPage() }
        </div>
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 48
  },
  chip: {
    margin: 8,
    display: 'flex',
    alignItems: 'center'
  },
  filter: {
    margin: '0px 16px'
  },
  body: {
    display: 'flex',
    flexDirection: 'column'
  }
});

export default Pagination;