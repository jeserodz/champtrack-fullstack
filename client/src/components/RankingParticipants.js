import _ from 'lodash';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Toolbar, ToolbarGroup, FlatButton, List, ListItem, Divider, Avatar } from 'material-ui';
import { ContentAdd } from 'material-ui/svg-icons';
import SelectUsersDialog from '../components/SelectUsersDialog';
import avatarPlaceholder from '../images/avatar-placeholder.png';
import { StyleSheet, css } from 'aphrodite';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);

class RankingParticipants extends Component {

  state = { openSelectorDialog: false }

  updateParticipants(participantKey, included) {
    const { addParticipant, removeParticipant, rankings } = this.props;
    included ? 
      addParticipant(rankings.selected, participantKey) : 
      removeParticipant(rankings.selected, participantKey);
  }

  renderListItems() {
    let { selectedRanking, users } = this.props.adminRankings;
    const firstItem = _.find(users, {});
    return _.map(users, (user, index) => {
      if (user.rankingId !== selectedRanking) return null;
      return (
        <span key={index}>
          { user !== firstItem ? <Divider /> : null }
          <ListItem 
            primaryText={user.displayName}
            secondaryText={user.email}
            leftAvatar={<Avatar src={user.photoURL ? baseURL + user.photoURL : avatarPlaceholder} />}
          />
        </span>
      );
    });
  }

  render() {
    const { selectedRanking } = this.props.adminRankings;
    return (
      <div className={css(styles.c1)}>
        <Toolbar>
          <ToolbarGroup className={css(styles.c2)}>
            <FlatButton 
              label="Gestionar Participantes" 
              primary={true} 
              icon={<ContentAdd />} 
              disabled={ selectedRanking ? false : true }
              onTouchTap={() => this.setState({ ...this.state, openSelectorDialog: true })}
            />
          </ToolbarGroup>
        </Toolbar>
        <List>
          { this.renderListItems() }
        </List>
        <SelectUsersDialog 
          open={this.state.openSelectorDialog} 
          onClose={() => this.setState({ ...this.state, openSelectorDialog: false })}
        />
      </div>
    );
  }
}

const styles = StyleSheet.create({
  c1: { flex: 1, display: 'flex', flexDirection: 'column' },
  c2: { display: 'flex', alignItems: 'flex-end', flex: 1 }
})

function mapStateToProps({ adminRankings }) {
  return { adminRankings };
}

export default connect(mapStateToProps)(RankingParticipants);