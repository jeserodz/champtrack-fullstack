import _ from 'lodash';
import React, { Component } from 'react';
import { baseURL } from '../api';
import { 
  Card, 
  CardHeader, 
  CardMedia, 
  CardTitle, 
  CardText, 
  CardActions, 
  FlatButton, 
} from 'material-ui'; 
import * as PropTypes from 'prop-types';
import spacing from 'material-ui/styles/spacing';
import { colors } from 'material-ui/styles';
import { StyleSheet, css } from 'aphrodite';
import placeholderImg from '../images/placeholder.png';
import { ActionThumbUp } from 'material-ui/svg-icons';

class PostCard extends Component {
  static propTypes = {
    post: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    toggleLike: PropTypes.func.isRequired
  }

  onLikePress() {
    const { post, user, toggleLike } = this.props;
    toggleLike(post.id, user.id);
  }

  getLikeButtonColor() {
    const { post, user } = this.props;
    let myLike = _.find(post.likes, { userId: user.id });
    return myLike ? colors.red400 : '#757575';
  }

  render() {
    const { post } = this.props;
    return (
      <Card className={css(styles.container)}>
        <CardMedia>
          <img src={post.photoURL ? baseURL + post.photoURL : placeholderImg} />
        </CardMedia>
        <CardActions>
          <FlatButton 
            style={{ textAlign: 'left', color: '#757575' }} 
            icon={<ActionThumbUp color={this.getLikeButtonColor()}/>} 
            label={post.likes.length.toString()}
            onTouchTap={this.onLikePress.bind(this)}
          />
        </CardActions>
        <CardTitle 
          style={{ paddingTop: 0, paddingBottom: 0 }}
          title={post.title} subtitle={(new Date(post.createdDate)).toDateString()} 
        />
        <CardText>
          {post.description}
        </CardText>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: spacing.desktopGutterLess
  }
});

export default PostCard;