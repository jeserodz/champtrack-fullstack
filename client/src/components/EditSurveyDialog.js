import _ from 'lodash';
import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
  Dialog, 
  FlatButton, 
  TextField, 
  TimePicker, 
  List, 
  ListItem,
  Subheader, 
  Checkbox,
  RadioButtonGroup,
  RadioButton
} from 'material-ui';
import { ContentAddCircle } from 'material-ui/svg-icons';
import { createSurvey, updateSurvey, select } from '../actions/AdminSurveysActions';
import { closeSurveyDialog } from '../actions/UXActions';
import { StyleSheet, css } from 'aphrodite';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);
 
class EditSurveyDialog extends Component {

  INITIAL_STATE = {
    id: null,
    question : "",
    options : [],
    answers : [],
    hasCorrectOption: false,
    startDate: moment().startOf('hour').toDate(),
    endDate: moment().startOf('hour').add(1, 'hour').toDate()
  }

  state = { ...this.INITIAL_STATE }

  componentWillReceiveProps(nextProps) {
    const { surveys, selectedSurvey } = nextProps.adminSurveys;
    const survey = _.find(surveys, { id: selectedSurvey })
    if (survey) {
      this.setState({ ...survey });
    } else {
      this.setState({ id: null, ...this.INITIAL_STATE });
    }
  }

  onInputChange(key, value) {
    this.setState({ ...this.state, [key]: value });
  }

  onClose() {
    this.props.closeSurveyDialog();
    this.props.select('survey', null);
    this.setState(this.INITIAL_STATE);
  }

  onConfirm() {
    const { surveys, selectedSurvey } = this.props.adminSurveys;
    const survey = _.find(surveys, { id: selectedSurvey })
    survey ? this.props.updateSurvey({ ...this.state }) : this.props.createSurvey({ ...this.state });
    this.props.select('survey', null);
    this.props.closeSurveyDialog();
    this.setState(this.INITIAL_STATE);
  }
  
  onAddOptionPress() {
    const { selectedSurvey } = this.props.adminSurveys;
    const newOption = {
      surveyId: selectedSurvey,
      title: '',
      isCorrect: false,
    }
    this.setState({ 
      ...this.state,
      options: [ ...this.state.options, newOption ]
    });
  }

  onOptionTextChange(index, value) {
    const options = [ ...this.state.options ];
    options[index].title = value;
    this.setState({ 
      ...this.state,
      options: [ ...options ]
    });
  }

  onOptionMarkCorrect(index, value) {
    const options = [ ...this.state.options ];
    options[index].isCorrect = value;
    this.setState({ 
      ...this.state,
      options: [ ...options ]
    });
  }

  validateFields() {
    let isValid = true;
    if (!this.state.question) isValid = false;
    if (!this.state.startDate) isValid = false;
    if (!this.state.endDate) isValid = false;
    if (!this.state.options.length) isValid = false;
    this.state.options.forEach(option => {
      if (!option.title) isValid = false;
    })
    return isValid;
  }

  actions = () => [
    <FlatButton 
      label="Cerrar" 
      onTouchTap={this.onClose.bind(this)} 
    />,
    <FlatButton 
      label="Confirmar" 
      primary
      disabled={!this.validateFields()}
      onTouchTap={this.onConfirm.bind(this)} 
    />
  ]

  renderDialogTitle() {
    const { selectedSurvey } = this.props.adminSurveys;
    const action = selectedSurvey ? 'Editar ' : 'Crear ';
    const type = this.state.hasCorrectOption ? 'Trivia' : 'Encuesta';
    return action + type;
  }

  renderOptionsList() {
    return _.map(this.state.options, (option, index) => {
      return (
        <ListItem 
          key={index} 
          style={{ paddingTop: 0, paddingBottom: 0 }}
          leftCheckbox={ this.state.hasCorrectOption ?
            <Checkbox
              checked={option.isCorrect} 
              onCheck={(e, value) => this.onOptionMarkCorrect(index, value)
            }/> : null 
          }
        >
          <TextField 
            id={'_'+index}
            value={option.title}
            onChange={(e, value) => this.onOptionTextChange(index, value)}
          />
        </ListItem>
      )
    })
  }

  render() {
    const { ux } = this.props;
    return (
      <Dialog
        title={this.renderDialogTitle()}
        open={ux.surveyDialog}
        actions={this.actions()}
        autoScrollBodyContent={true}
      >
        <RadioButtonGroup 
          name="hasCorrectOption" 
          defaultSelected={this.state.hasCorrectOption} 
          onChange={(e, value) => this.setState({ ...this.state, hasCorrectOption: value })}
        >
          <RadioButton
            value={false}
            label="Encuesta"
          />
          <RadioButton
            value={true}
            label="Trivia"
          />
        </RadioButtonGroup>

        <TextField
          fullWidth={true}
          floatingLabelText="Pregunta"
          value={this.state.question}
          onChange={(e) => this.onInputChange('question', e.target.value)}
        />

        <div className={css(styles.surveyDialogContent)}>
          <div style={prefixer({ flex: 1 })}>
            <List>
              <Subheader>Opciones</Subheader>
              { this.renderOptionsList() }
              <ListItem 
                leftIcon={<ContentAddCircle/>} 
                primaryText="Agregar Opción" 
                onTouchTap={this.onAddOptionPress.bind(this)}
              />
            </List>
          </div>
          <div style={prefixer({ flex: 1 })}>
            <TimePicker
              floatingLabelText="Tiempo Inicio"
              value={moment(this.state.startDate).toDate()}
              onChange={(e, time) => this.onInputChange('startDate', time)}
            />

            <TimePicker
              floatingLabelText="Tiempo Final"
              value={moment(this.state.endDate).toDate()}
              onChange={(e, time) => this.onInputChange('endDate', time)}
            />
          </div>
        </div>

      </Dialog>
    );
  }
}

const styles = StyleSheet.create({
  surveyDialogContent: { display: 'flex' }
})

function mapStateToProps({ adminSurveys, ux }) {
  return { adminSurveys, ux };
}

export default connect(mapStateToProps, { createSurvey, updateSurvey, select, closeSurveyDialog })(EditSurveyDialog);