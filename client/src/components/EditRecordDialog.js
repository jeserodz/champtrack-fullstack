import _ from 'lodash';
import validator from 'validator';
import { baseURL } from '../api';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { select, createRecord, updateRecord } from '../actions/AdminDatosActions';
import { closeRecordDialog } from '../actions/UXActions';
import Pagination from './Pagination';
import { Dialog, FlatButton, Chip, Avatar, TextField, List, ListItem, Subheader, Checkbox } from 'material-ui';
import { ContentSort, ActionDateRange, ActionDonutSmall } from 'material-ui/svg-icons';
import { spacing } from 'material-ui/styles';
import { StyleSheet, css } from 'aphrodite';
import profileFallback from '../images/avatar-placeholder.png';
import postCss from 'postcss-js';
const prefixer = postCss.sync([require('autoprefixer')]);
 
const styles = StyleSheet.create({
  chipStyle: {
    marginRight: 5
  },
  chipsContainer: { 
    display: 'flex', 
    width: '100%', 
    marginTop: spacing.desktopGutterLess 
  },
  recordContent: { display: 'flex' }
});

class EditRecordDialog extends Component {

  INITIAL_STATE = {
    id: null,
    periodId: null,
    userId: null,
    score: 0,
    data: [
      // { recordId: 0, variableId: 0, value: 0 }
    ],
  }

  state = { ...this.INITIAL_STATE }

  componentWillReceiveProps(nextProps) {
    const { 
      rankings, 
      records, 
      selectedRanking, 
      selectedPeriod, 
      selectedRecord 
    } = nextProps.adminDatos;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return null; // need record for variables info
    const record = _.find(records, { id: selectedRecord });
    if (record) {
      this.setState({ ...record });
    } else {
      const data = _.map(ranking.variables, variable => {
        return { variableId: variable.id, value: 0 }
      })
      this.setState({
        periodId: selectedPeriod,
        userId: null,
        data: data,
        score: this.calculateScore(data)
      });
    }
  }

  onClose() {
    const { closeRecordDialog, select } = this.props;
    closeRecordDialog()
    select('record', null);
    this.setState({ ...this.INITIAL_STATE });
  }

  onConfirm() {
    const { selectedRecord } = this.props.adminDatos;
    selectedRecord ? this.props.updateRecord({...this.state}) : this.props.createRecord({...this.state});
    this.props.closeRecordDialog();
    this.props.select('record', null);
    this.setState({ ...this.INITIAL_STATE });
  }

  onVariableValueChange(id, value) {
    const data = { ...this.state.data };
    const changedVariable = _.find(data, { variableId: id });
    changedVariable.value = value;

    this.setState({
      ...this.state,
      data: data,
      score: this.calculateScore(data)
    });
  }

  onUserSelect(id) {
    this.setState({ ...this.state, userId: id });
  }

  calculateScore(data) {
    const { rankings, selectedRanking } = this.props.adminDatos;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return 0;
    const score = _.reduce(data, (sum, dataValue) => {
      const variable = _.find(ranking.variables, { id: dataValue.variableId })
      if (!variable) return 0;
      return sum + (dataValue.value * variable.weight / 100);
    }, 0);
    return parseFloat(score).toFixed(2);
  }

  validateFields() {
    let isValid = true;

    if (!this.state.userId) {
      isValid = false;
    }

    _.forEach(this.state.data, (dataValue, index) => {
      if (!dataValue.value || dataValue.value <= 0)
        isValid = false;
    });

    return isValid;
  }

  actions = () => [
    <FlatButton
      label="Cerrar"
      onTouchTap={this.onClose.bind(this)}
    />,
    <FlatButton
      label="Confirmar"
      primary
      onTouchTap={this.onConfirm.bind(this)}
      disabled={!this.validateFields()}
    />
  ]

  renderDialogTitle() {
    const { selectedRecord } = this.props.adminDatos;
    return selectedRecord ? "Editar Dato" : "Crear Dato";
  }

  renderRecordHeaders() {
    const { rankings, selectedRanking, selectedPeriod, selectedUser } = this.props.adminDatos;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return null;
    const period = _.find(ranking.periods, { id: selectedPeriod });
    const user = _.find(ranking.users, { id: this.state.userId });
    return (
      <div className={css(styles.chipsContainer)}>
        <Chip
          className={css(styles.chipStyle)} 
          children={[ 
            <Avatar icon={<ContentSort />} />,
            ranking ? ranking.name : 'No Ranking'
          ]}
        />
        <Chip
          style={styles.chipStyle} 
          children={[ 
            <Avatar icon={<ActionDateRange />} />,
            period ? period.name : 'No Período' 
          ]}
        />
        <Chip
          style={styles.chipStyle} 
          children={[
            <Avatar src={(user && user.photoURL) ? baseURL + user.photoURL : profileFallback} />,
            user ? user.displayName : 'No Usuario'
          ]}
        />
        <Chip
          style={styles.chipStyle} 
          children={[ 
            <Avatar icon={<ActionDonutSmall />} />,
            this.state.score ? this.state.score : '0' 
          ]}
        />
      </div>
    )
  }

  renderVariableValuesFields() {
    const { rankings, selectedRanking } = this.props.adminDatos;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return null;
    return _.map(this.state.data, (data, key) => {
      const variable = _.find(ranking.variables, { id: data.variableId });
      if (!variable) return null;
      return (
        <TextField
          type="number"
          min={0}
          key={key}
          style={{ display: 'block' }}
          floatingLabelText={`${variable.name} (Peso: ${variable.weight}%)`}
          value={data.value || '' }
          onChange={(e, value) => this.onVariableValueChange(variable.id, value)}
        />
      );
    });
  }
  
  userFilter(user, filterText) {
    let match = false;
    if (!filterText) return true;
    if (validator.contains(user.displayName.toLowerCase(), filterText)) match = true;
    if (validator.contains(user.email.toLowerCase(), filterText)) match = true;
    if (validator.contains(_.get(user, 'country.name', '').toLowerCase(), filterText)) match = true;
    if (validator.contains(_.get(user, 'businessUnit.name', '').toLowerCase(), filterText)) match = true;
    if (validator.contains(_.get(user, 'ranking.name', '').toLowerCase(), filterText)) match = true;
    return match;
  }

  renderUsersList() {
    const { rankings, selectedRanking } = this.props.adminDatos;
    const ranking = _.find(rankings, { id: selectedRanking });
    if (!ranking) return null;
    return (
      <Pagination 
        collection={ranking.users}
        pageSize={4}
        render={this.renderUser.bind(this)}
        filterHintText={'Filtrar usuarios...'}
        filter={this.userFilter}
      />
    );
  }

  renderUser(user) {
    return (
      <ListItem
        key={user.id}
        primaryText={user.displayName}
        secondaryTextLines={2}
        secondaryText={
          <p>
            <span>
              <strong>{'BU: '}</strong> {_.get(user, 'businessUnit.name', 'Sin BU')}
              <strong>{' | País: '}</strong> {_.get(user, 'country.name', 'Sin País')}
              <strong>{' | Ranking: '}</strong> {_.get(user, 'ranking.name', 'Sin Ranking')}
            </span><br/>
            <span>
              <strong>{'Email: '}</strong> {user.email}
            </span>
          </p>
        }
        leftCheckbox={
          <Checkbox 
            checked={this.state.userId === user.id } 
            onCheck={(e) => this.onUserSelect(user.id)} 
          />
        }
        rightIcon={<Avatar src={user.photoURL ? baseURL + user.photoURL : profileFallback} />}
      />
    );
  }

  render() {
    return (
      <Dialog
        title={this.renderDialogTitle()}
        open={this.props.ux.recordDialog}
        actions={this.actions()}
        autoScrollBodyContent={true}
        contentStyle={{ maxWidth: 1200 }}
      >
        { this.renderRecordHeaders() }
        <div className={css(styles.recordContent)}>
          <div style={prefixer({ flex: 1 })}>
            { this.renderVariableValuesFields() }
          </div>
          <div style={prefixer({ flex: 1, marginLeft: spacing.desktopGutter })}>
            <List>
              <Subheader children={"Seleccione Usuario"} />
              { this.renderUsersList() }
            </List>
          </div>
        </div>
      </Dialog>
    );
  }
}

function mapStateToProps({ adminDatos, ux }) {
  return { adminDatos, ux }
}

export default connect(mapStateToProps, { select, createRecord, updateRecord, closeRecordDialog })(EditRecordDialog);