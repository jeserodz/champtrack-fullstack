import React, { Component } from 'react';
import { Snackbar } from 'material-ui';
import { connect } from 'react-redux';
import { HashRouter, Route } from 'react-router-dom';
import { StyleSheet, css } from 'aphrodite';
import Login from './containers/Login';
import Dashboard from './containers/Dashboard';

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    display: 'flex', 
    flexDirection: 'column',
  }
});

class Connector extends Component {
  render() {
    return (
      <div id="connector" className={css(styles.container)}>
        <HashRouter>
          <Route path="/" component={ this.props.auth.user ? Dashboard : Login }/>
        </HashRouter>
        <Snackbar
          open={this.props.ux.snackbarOpen}
          message={this.props.ux.snackbarMessage}
        />
      </div>
    );
  }
}

function mapStateToProps({ auth, ux }) {
  return { auth, ux }
}

export default connect(mapStateToProps)(Connector);